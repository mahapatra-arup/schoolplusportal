using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using SP.Data.Models;

namespace SP.Data.Banks.Tools
{
    public class BankTools
    {
        private SP_DataEntities db = new SP_DataEntities();
        public BankTools()
        {
            db = new SP_DataEntities();
        }


        public async Task<List<Bank>> GetBanksAsync()
        {
           return  await db.Banks.OrderBy(s=>s.BankName).ToListAsync();
        }
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                