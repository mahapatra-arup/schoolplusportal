﻿

using System.ComponentModel.DataAnnotations;

namespace SP.Data.Account.Models
{
    public class TransectionMode
    {
        public enum _TransectionMode
        {
            [Display(Name = "CASH")]
            CASH,
            [Display(Name = "BANK")]
            BANK
        }
    }
}
