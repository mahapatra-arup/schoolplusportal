﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP.Data.Account.Models
{
    public class LedgerCategories
    {
        public static string FEES
        {
            get => "FEES"; }
        public static string FUND
        {
            get => "FUND"; }
        public static string BANK
        {
            get => "BANK"; }
        public static string CASH
        {
            get => "CASH"; }
        }
    }
