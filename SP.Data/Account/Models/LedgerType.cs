﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP.Data.Account.Models
{
   public class LedgerType
    {
        public static string INCOME { get => "INCOME"; }
        public static string EXPENSE { get => "EXPENSE"; }
    }
}
