﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using SP.Data.Account.Models;

namespace SP.Data.Account.Tools
{
    public class LedgerTools
    {
        private SP_DataEntities db = new SP_DataEntities();
        public LedgerTools()
        {
            db = new SP_DataEntities();
        }



        /// <summary>
        ///  Get Fees Parent Ledger
        /// _Category Like : LedgerCategories.FUND,LedgerCategories.Fees
        /// </summary>
        /// <param name="_Category">use Loike LedgerCategories.FUND,LedgerCategories.Fees</param>
        /// <returns></returns>
        public async Task<IEnumerable<Ledger>> GetLedgerByCategoryAsync(string _Category)
        {
            var lstLegder =  db.Ledgers.Include(l => l.Ledger2).Include(b => b.LedgerBankDetails);
            List<Ledger> LedgerData = new List<Ledger>();
            if (lstLegder.IsValidIEnumerable())
            {
                LedgerData = await lstLegder.Where(s => s.Category == _Category).ToListAsync();
            }
            return  LedgerData;
        }

        /// <summary>
        /// Is Fees Lab Ledger check
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public async Task<bool> IsLabLedgerAsync(Guid ledgerId)
        {
            var lstLegder = db.Ledgers.Include(l => l.Ledger2).Include(b => b.LedgerBankDetails);
            List<Ledger> LedgerData = new List<Ledger>();
            if (lstLegder.IsValidIEnumerable())
            {
                LedgerData = await lstLegder.Where(s => s.LedgerId == ledgerId).Where(f=>f.IsFees==true).Where(l=>l.IsLab==true).ToListAsync();
            }
            return LedgerData.IsValidList();
        }
       
    }
}
