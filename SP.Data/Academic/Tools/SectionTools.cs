﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace SP.Data.Academic.Tools
{
 public    class SectionTools
    {
        private  SP_DataEntities db = new SP_DataEntities();

        public async Task<IEnumerable<Section>> GetSectionsAsync(long? ClassID)
        {
            var data = db.Sections.Include(s => s.Class);
            IEnumerable<Section> lstseCtion =await data.Where(s=>s.ClassID== ClassID).ToListAsync();
         return   lstseCtion;
        }

        public async Task<Section> GetSectionAsync(long? id)
        {
            var data = db.Sections.Include(s => s.Class);
            Section lstseCtion =await data.Where(s => s.ID == id).FirstOrDefaultAsync();
            return lstseCtion;
        }

        #region ======Bool===
        public async Task<bool> IsValidSectionAsync(long ClassID, string sectionName)
        {
            var data = db.Sections.Include(s => s.Class);

           Section _secDetails = new Section();
            if (data.ISValidObject())
            {
                var sec = from s in data
                          where s.ClassID==ClassID && s.SectionName == sectionName
                          select s;
                _secDetails = sec.ISValidObject() ?await sec.FirstOrDefaultAsync() : new Section();
            }
            return _secDetails.ISValidObject() ? _secDetails.SectionName.ISValidObject() : false;
        }

        public async Task<bool> IsValidSectionAsync(long ClassID,string sectionName, long NotInSetonId)
        {
           var  data = db.Sections.Include(s => s.Class);

            Section _secDetails = new Section();
            if (data.ISValidObject())
            {
                var sec = from s in data
                          where s.ClassID == ClassID && s.SectionName == sectionName && s.ID!= NotInSetonId
                          select s;
                _secDetails = sec.ISValidObject() ?await sec.FirstOrDefaultAsync() : new Section();
            }
            return _secDetails.ISValidObject() ? _secDetails.SectionName.ISValidObject() : false;
        }
        #endregion
    }
}
