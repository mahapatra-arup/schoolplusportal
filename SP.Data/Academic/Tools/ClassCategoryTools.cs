﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace SP.Data.Academic.Tools
{
    public class ClassCategoryTools
    {
        private SP_DataEntities db = new SP_DataEntities();
        public async Task<Class_Category> GetClassCategoryAsync(long id)
        {
            var lstClsCat = db.Class_Category.Include(s => s.Classes);
            Class_Category classCatData = new Class_Category();
            if (lstClsCat.IsValidIEnumerable())
            {
                classCatData = await lstClsCat.Where(s => s.ID == id).FirstOrDefaultAsync();
            }
            return classCatData;
        }
    }
}
