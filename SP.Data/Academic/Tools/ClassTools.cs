﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using SP.Data.Models.others;
using SP.Data.Academic.Models;

namespace SP.Data.Academic
{
    public class ClassTools
    {
        private SP_DataEntities db = new SP_DataEntities();

        public ClassTools()
        {
            db = new SP_DataEntities();
        }



        /// <summary>
        /// Get Class Use mp Or Hs Session (Because some time Two Session Are different)
        /// </summary>
        /// <param name="MpSessionId"></param>
        /// <param name="HsSessionId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Class>> GetClassAsync(int MpSessionId, int HsSessionId)
        {
            var lstcls = db.Classes.Include(s => s.Class_Category).Include(b => b.Session);
            IEnumerable<Class> classData = null;
            if (lstcls.IsValidIEnumerable())
            {
                classData = await lstcls.Where(s => s.SessionId == MpSessionId).Where(q => q.Class_Category.CategoryName.ToLower() != ClassCategories.Higher_Secondary)//Mp
                     .Union(lstcls.Where(s => s.SessionId == HsSessionId).Where(q => q.Class_Category.CategoryName.ToLower() == ClassCategories.Higher_Secondary)).ToListAsync();//HS
            }
            return classData;
        }

        public async Task<IEnumerable<Class>> GetMpClassAsync(int MpSessionId)
        {
            var lstcls = db.Classes.Include(s => s.Class_Category).Include(b => b.Session);
            IEnumerable<Class> classData = null;
            if (lstcls.IsValidIEnumerable())
            {
                classData = await lstcls.Where(s => s.SessionId == MpSessionId).Where(q => q.Class_Category.CategoryName.ToLower() != ClassCategories.Higher_Secondary).ToListAsync();//Mp
            }
            return classData;
        }

        public async Task<IEnumerable<Class>> GetHsClassAsync(int HsSessionId)
        {
            var lstcls = db.Classes.Include(s => s.Class_Category).Include(b => b.Session);
            IEnumerable<Class> classData = null;
            if (lstcls.IsValidIEnumerable())
            {
                classData = await lstcls.Where(s => s.SessionId == HsSessionId).Where(q => q.Class_Category.CategoryName.ToLower() == ClassCategories.Higher_Secondary).ToListAsync();//Mp
            }
            return classData;
        }



        public async Task<Class> GetClassAsync(long? classID)
        {
            var s = db.Classes;
            Class _ClassDetails = new Class();
            if (s.ISValidObject())
            {
                var cls = await s.Where(c => c.Id == classID).ToListAsync();
                _ClassDetails = cls.ISValidObject() ?  cls.FirstOrDefault() : new Class();
            }
            return _ClassDetails;
        }

       

        #region ======Bool===
        public async Task<bool> IsValidClassAsync(int sessionId, string cLassName)
        {
            var s = db.Classes;

            Class _ClassDetails = new Class();
            if (s.ISValidObject())
            {
                var cls =await (from classs in s
                          where classs.ClassName == cLassName
                          select classs).ToListAsync();
                _ClassDetails = cls.ISValidObject() ? cls.FirstOrDefault() : new Class();
            }
            return _ClassDetails.ISValidObject() ? _ClassDetails.ClassName.ISValidObject() : false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cLassName"></param>
        /// <param name="ClassId"></param>
        /// <returns></returns>
        public async Task<bool> IsValidClassAsync(int sessionId, string cLassName, long NotInClassId)
        {
            var s = db.Classes;

            Class _ClassDetails = new Class();
            if (s.ISValidObject())
            {
                var cls =await (from classs in s
                          where classs.SessionId == sessionId && classs.ClassName == cLassName && classs.Id != NotInClassId
                          select classs).ToListAsync();
                _ClassDetails = cls.ISValidObject() ? cls.FirstOrDefault() : new Class();
            }
           
            return _ClassDetails.ISValidObject() ? _ClassDetails.ClassName.ISValidObject() : false;
          
        }

        /// <summary>
        /// Category_Type like  {ClassCategories.Higher_Secondary} 
        /// </summary>
        /// <param name="_ClassId">long</param>
        /// <param name="Category_Type">like ClassCategories.Higher_Secondary</param>
        /// <returns></returns>
        public async Task<bool> IsClassCategory(long _ClassId,string Category_Type)
        {
            var s = db.Classes;
            
            Class _ClassDetails = new Class();
            if (s.ISValidObject())
            {
                var cls = await (from cl in s
                                 join cls_cat in db.Class_Category on  cl.CategoryID equals cls_cat.ID
                                 where cl.Id== _ClassId &&cls_cat.CategoryName== Category_Type
                                 select cl).ToListAsync();
                _ClassDetails = cls.ISValidObject() ? cls.FirstOrDefault() : new Class();
            }
            return _ClassDetails.ISValidObject() ? _ClassDetails.ClassName.ISValidObject() : false;
        }
        #endregion

    }
}
