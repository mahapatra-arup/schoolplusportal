﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP.Data.Academic.Models
{
    /// <summary>
    /// set Type of Cass Readonly Field;
    /// </summary>
    public class ClassCategories
    {
        public static string Higher_Secondary { get => "Higher Secondary"; }
        public static string Secondary { get => "Secondary"; }
        public static string Upper_Primary { get => "Upper Primary"; }
        public static string Primary { get => "Primary"; }


    }
}
