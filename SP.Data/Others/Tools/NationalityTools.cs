﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using SP.Data.Models.others;

namespace SP.Data.Others
{
    public class NationalityTools
    {
        private SP_DataEntities db = new SP_DataEntities();
        public NationalityTools()
        {
            db = new SP_DataEntities();
        }

        public async Task<List<Nationality>> GetNationalitiesAsync()
        {
            var lstNationalities = db.Nationalities;
            List<Nationality> nationalities = new List<Nationality>();
            if (lstNationalities.IsValidIEnumerable())
            {
                nationalities = await lstNationalities.ToListAsync();
            }
            return nationalities;
        }
        
    }
}
