﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using SP.Data.Models.others;

namespace SP.Data.Others
{
    public class SupportingDocsTools
    {
        private SP_DataEntities db = new SP_DataEntities();
        public SupportingDocsTools()
        {
            db = new SP_DataEntities();
        }

        public async Task<List<SupportingDoc>> GetSupportingDocsAsync()
        {
            var lstSupportingDocs = db.SupportingDocs;
            List<SupportingDoc> supportingDocs = new List<SupportingDoc>();
            if (lstSupportingDocs.IsValidIEnumerable())
            {
                supportingDocs = await lstSupportingDocs.ToListAsync();
            }
            return supportingDocs;
        }
        
    }
}
