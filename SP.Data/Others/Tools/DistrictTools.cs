﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using SP.Data.Models.others;

namespace SP.Data.Others
{
    public class DistrictTools
    {
        private SP_DataEntities db;
        public DistrictTools()
        {
            db = new SP_DataEntities();
        }

        public async Task<List<District>> GetDistrictAsync()
        {
            return await db.Districts.ToListAsync();
        }

        public async Task<District> GetDistrictAsync(int DistId)
        {
           return (await db.Districts.Where(s=>s.Id== DistId).FirstOrDefaultAsync());
        }
    }
}
