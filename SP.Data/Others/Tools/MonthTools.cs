﻿using SP.Data.Academic;
using SP.Data.Academic.Models;
using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP.Data.Others.Tools
{
    public class MonthTools
    {
        private SP_DataEntities db ;
        ClassTools classTools = new ClassTools();

        public MonthTools()
        {
            db = new SP_DataEntities();
        }

        public async Task<List<Month>> GetMonthsAsync(long classId)
        {

           var isHigherSecondary=await classTools.IsClassCategory(classId, ClassCategories.Higher_Secondary);
            if (isHigherSecondary)
            {
              return await db.Months.OrderBy(o => o.HSSlNo).ToListAsync();//HS
            }
            return await db.Months.OrderBy(o => o.MPSlNo).ToListAsync();//Mp
        }

        public async Task<List<Month>> GetMonthsAsync(Guid _BIllId, long classId)
        {
            var isHigherSecondary = await classTools.IsClassCategory(classId, ClassCategories.Higher_Secondary);
            var fessDetailsMonth =await db.FeesBillDetails.Where(p=>p.BillId==_BIllId).Select(m=>m.Month).ToListAsync();

            if (isHigherSecondary)
            {
                //MP
               var mpm= await db.Months.Where(s => fessDetailsMonth.Contains(s.MonthName))
                    .OrderBy(o => o.HSSlNo).ToListAsync();//HS
                return mpm;
            }
            //HS
            var hsm=  await db.Months.Where(s => fessDetailsMonth.Contains(s.MonthName))
                .OrderBy(o => o.MPSlNo).ToListAsync();//Mp
            return hsm;
        }
    }
}
