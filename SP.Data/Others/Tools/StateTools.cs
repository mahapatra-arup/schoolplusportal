﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using SP.Data.Models.others;

namespace SP.Data.Others
{
    public class StateTools
    {
        private SP_DataEntities db;
        public StateTools()
        {
            db = new SP_DataEntities();
        }


        public async Task<State> GetStateAsync(int StateId)
        {
           return (await db.States.Where(s=>s.ID== StateId).FirstOrDefaultAsync());
        }
    }
}
