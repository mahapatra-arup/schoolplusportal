﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using SP.Data.Models.others;

namespace SP.Data.Others
{
    public class SubCasteTools
    {
        private SP_DataEntities db = new SP_DataEntities();
        public SubCasteTools()
        {
            db = new SP_DataEntities();
        }

        public async Task<List<SubCaste>> GetSubCasteAsync()
        {
            var lstSubCaste = db.SubCastes;
            List<SubCaste> subcasteData = new List<SubCaste>();
            if (lstSubCaste.IsValidIEnumerable())
            {
                subcasteData = await lstSubCaste.ToListAsync();
            }
            return subcasteData;
        }
        
    }
}
