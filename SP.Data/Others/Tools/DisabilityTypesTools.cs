﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using SP.Data.Models.others;

namespace SP.Data.Others
{
    public class DisabilityTypesTools
    {
        private SP_DataEntities db ;
        public DisabilityTypesTools()
        {
            db = new SP_DataEntities();
        }

        public async Task<List<DisabilityType>> GetDisabilityTypesAsync()
        {
            var lstDisabilityTypes = db.DisabilityTypes;
            List<DisabilityType> disabilityTypes = new List<DisabilityType>();
            if (lstDisabilityTypes.IsValidIEnumerable())
            {
                disabilityTypes = await lstDisabilityTypes.ToListAsync();
            }
            return disabilityTypes;
        }
        
    }
}
