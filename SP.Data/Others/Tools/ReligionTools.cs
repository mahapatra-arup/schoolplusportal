﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using SP.Data.Models.others;

namespace SP.Data.Others
{
    public class ReligionTools
    {
        private SP_DataEntities db = new SP_DataEntities();
        public ReligionTools()
        {
            db = new SP_DataEntities();
        }

        public async Task<List<Religion>> GetReligionsAsync()
        {
            var lstReligions = db.Religions;
            List<Religion> religions = new List<Religion>();
            if (lstReligions.IsValidIEnumerable())
            {
                religions = await lstReligions.ToListAsync();
            }
            return religions;
        }
        
    }
}
