﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using SP.Data.Models.others;

namespace SP.Data.Others
{
    public class EconomicalStatusTools
    {
        private SP_DataEntities db ;
        public EconomicalStatusTools()
        {
            db = new SP_DataEntities();
        }

        public async Task<List<EconomicalStatu>> GetEconomicalStatusAsync()
        {
            return await db.EconomicalStatus.ToListAsync();
        }
        
    }
}
