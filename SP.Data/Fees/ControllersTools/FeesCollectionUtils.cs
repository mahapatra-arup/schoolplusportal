﻿using SP.Data.Fees.Models;
using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using static SP.Data.Enums.StudentsFeesEnum;

namespace SP.Data.Fees.ControllersTools
{
    public class FeesCollectionUtils
    {

        #region -=Obj=-
        SP_DataEntities db = new SP_DataEntities();
        FeesConfigUtils feesConfigUtils = new FeesConfigUtils();
        FeesBiilGenerateUtils feesBiilGenerateUtils = new FeesBiilGenerateUtils();
        #endregion

        

        #region --==Collection==--
        //*****************************************************************************************************
        //Name : Payment
        //Description : Collect Students Due Fess 
        //*****************************************************************************************************
        public async Task<int> FeesCollection(FeesPaymentDetail feesPaymentDetail, List<FeesPaymentDetailsSub> lstfeesPaymentDetailsSub, List<FeesBillDetail> lstfeesBillDetails, List<Month> lstMonth)
        {
            int _OutPut = 0;

            var isMonthly = (await feesConfigUtils.GetFeesConfig()).IsMonthly;

            //Save
            using (db = new SP_DataEntities())
            {
                db.Database.Log = Console.Write;
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Fees Payment Details
                        db.FeesPaymentDetails.Add(feesPaymentDetail);
                        _OutPut += await db.SaveChangesAsync();

                        //Decission /Payment Sub Details
                        if (isMonthly)
                        {
                            _OutPut += await MonthlyFeesCollect(lstfeesPaymentDetailsSub, lstfeesBillDetails, lstMonth);
                        }
                        else
                        {
                            _OutPut += await YearlyFeesCollect(lstfeesPaymentDetailsSub, lstfeesBillDetails);
                        }


                        //Commit
                        transaction.Commit();
                        return _OutPut;
                    }
                    catch (DbEntityValidationException e)
                    {
                        transaction.Rollback();
                        var newException = new FormattedDbEntityValidationException(e);
                        throw newException;
                    }

                }

                // return false;
            }
        }

        private async Task<int> YearlyFeesCollect(List<FeesPaymentDetailsSub> lstfeesPaymentDetailsSub, List<FeesBillDetail> lstfeesBillDetails)
        {
            int _OutPut = 0;

            if (lstfeesPaymentDetailsSub.IsValidList())
            {
                //=Fees payment sub save=
                db.FeesPaymentDetailsSubs.AddRange(lstfeesPaymentDetailsSub);
                _OutPut += await db.SaveChangesAsync();

                //-Fees BillDetails-
                foreach (FeesBillDetail feesBillDetail in lstfeesBillDetails)
                {
                    //Find
                    var data = db.FeesBillDetails
                        .Where(fb => fb.BillId == feesBillDetail.BillId)
                        .Where(fa => fa.FeesAmountId == feesBillDetail.FeesAmountId).First();

                    //Update Data
                    if (data.ISValidObject())
                    {
                        data.PaidFees = data.PaidFees + feesBillDetail.PaidFees;
                        data.Concession = data.Concession + feesBillDetail.Concession;
                        data.DueFees = feesBillDetail.DueFees;

                        //Modify
                        db.Entry(data).State = EntityState.Modified;
                        _OutPut += await db.SaveChangesAsync();
                    }
                    else
                    {
                        //Bill Details Not Update
                    }
                }
            }
            return _OutPut;
        }

        private async Task<int> MonthlyFeesCollect(List<FeesPaymentDetailsSub> lstfeesPaymentDetailsSub, List<FeesBillDetail> lstfeesBillDetails, List<Month> lstMonth)
        {
            int _OutPut = 0;

            if (!lstfeesPaymentDetailsSub.IsValidList())
            { return _OutPut; }

            //Month Check
            if (!lstMonth.IsValidList())
            { return _OutPut; }

            //=Fees payment sub save=
            db.FeesPaymentDetailsSubs.AddRange(lstfeesPaymentDetailsSub);
            _OutPut += await db.SaveChangesAsync();

            foreach (var feesBillDetail in lstfeesBillDetails)
            {
                double? _totalPaidFees = feesBillDetail.PaidFees;

                foreach (var month in lstMonth)
                {
                    #region -=Month loop-=
                    //Find
                    var db_data = db.FeesBillDetails
                        .Where(fb => fb.BillId == feesBillDetail.BillId)
                        .Where(fa => fa.FeesAmountId == feesBillDetail.FeesAmountId)
                        .Where(m => m.Month == month.MonthName)
                        .First();


                    //Update Data
                    //N.B.::CONCESSION IS NOT SUPPORT MOTHLY FEES(ONLY SUBSIDY)--
                    if (db_data.ISValidObject())
                    {
                        if (_totalPaidFees > 0 && db_data.DueFees > 0)
                        {
                            if (_totalPaidFees > db_data.DueFees)
                            {
                                //initialize
                                #region true
                                _totalPaidFees -= db_data.DueFees;

                                // Fill Data
                                db_data.PaidFees = db_data.DueFees;
                                db_data.DueFees = 0;
                                db_data.Concession = 0;


                                //Modify
                                db.Entry(db_data).State = EntityState.Modified;
                                _OutPut += await db.SaveChangesAsync();
                                #endregion
                            }
                            else
                            {
                                #region false
                                // Fill Data
                                db_data.DueFees = db_data.DueFees - _totalPaidFees;
                                db_data.PaidFees = _totalPaidFees;
                                db_data.Concession = 0;

                                //Modify
                                db.Entry(db_data).State = EntityState.Modified;
                                _OutPut += await db.SaveChangesAsync();

                                //initialize
                                _totalPaidFees = 0;
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
            }

            return _OutPut;
        }
        #endregion

        #region --==List==--
        public async Task<List<FeesBillView>> GetDueFees(Guid billId, List<string> lstmonth, CustomeAndFineFees _CustomeAndFineFees)
        {
            var isMonthly = (await feesConfigUtils.GetFeesConfig()).IsMonthly;
            List<FeesBillView> lstFeesBillViews = new List<FeesBillView>();


            #region Switch
            //Variable
            string qryCustomCheck = string.Empty;
            bool _IsFine = true;
            bool _IsCustom = true;

            switch (_CustomeAndFineFees)
            {
                case CustomeAndFineFees.WithCustom_OR_IsFine:
                    qryCustomCheck = ("(IsFine=@0 or IsCustom=@1)");/**/
                    break;
                case CustomeAndFineFees.WithOutCustomAndIsFine:
                    qryCustomCheck = ("IsFine!=@0 and IsCustom!=@1");/* */
                    break;

            }
            #endregion

            #region linq
            var result = await db.FeesBillViews
                  .Where(s => s.DueFees >= 0)
                   .Where(b => b.BillId == billId)
                   .Where(qryCustomCheck, _IsFine, _IsCustom)
                .OrderBy(l => l.LedgerName).ToListAsync();

            #region -=if Monthly=-
            if (isMonthly)
            {
                result = result.Where(s => lstmonth.Contains(s.Month)).ToList();
            }
            #endregion


            var q = result.AsEnumerable();

            lstFeesBillViews = (from val in q
                                group val by new
                                {
                                    val.LedgerId,
                                    val.FeesAmountID,
                                    val.LedgerName,
                                } into p
                                select new FeesBillView()
                                {
                                    LedgerId = p.First().LedgerId,
                                    FeesAmountID = p.First().FeesAmountID,
                                    LedgerName = p.First().LedgerName,

                                    //AMount
                                    FeesBillDetails_Fees = p.Sum(r => r.FeesBillDetails_Fees),
                                    DueFees = p.Sum(r => r.DueFees),
                                    PaidFees = p.Sum(r => r.PaidFees),
                                    Concession = p.Sum(r => r.Concession)

                                }).ToList();

           
            #endregion

            return lstFeesBillViews;
            //ref:https://weblogs.asp.net/scottgu/dynamic-linq-part-1-using-the-linq-dynamic-query-library
        }


        public async Task<IEnumerable<StudentFeesListVM>> GetDueStudentFeesList(int _ClassId, _StudentFeesListEnum studentFeesListEnum)
        {
            //Param
            var param1 = new SqlParameter("@ClassId", _ClassId);

            #region Sub-Query Check
            string subqry = string.Empty;
            switch (studentFeesListEnum)
            {
                case _StudentFeesListEnum.DUE_LIST:
                    subqry = " where DueFees>0 and ClassId=@ClassId";
                    break;
                case _StudentFeesListEnum.PAID_LIST:
                    subqry = " where DueFees=0 and ClassId=@ClassId";
                    break;
                case _StudentFeesListEnum.ALL:
                    subqry = "where ClassId=@ClassId ";
                    break;
                default:
                    break;
            }
            #endregion

            #region Query
            string qry = "  select StudentID,RegisterNo,StudentName,ClassName,SectionName,RollNo,FeesBill_SessionId as SessionId,session,  DOB, FatherName, ClassId,BillId,  " +
               "   sum(FeesBillDetails_Fees) as Fees,sum(PaidFees) as PaidFees,sum(DueFees) as DueFees,sum(Concession) as Concession from  " +
               "  (SELECT     StudentListView.RegisterNo,StudentListView.StudentName, StudentListView.session, StudentListView.StudentID, StudentListView.Gender, StudentListView.DOB, StudentListView.FatherName,   " +
               "  FeesBillView.ClassId,StudentListView.ClassName,StudentListView.SectionName,StudentListView.RollNo,  " +
               "  FeesBillView.FeesBillDetails_Fees, FeesBillView.DueFees,  FeesBillView.Concession, FeesBillView.PaidFees,  " +
               "  FeesBillView.FeesBill_SessionId, FeesBillView.BillId  " +
               "  FROM StudentListView INNER JOIN  " +
               "  FeesBillView ON FeesBillView.RegisterNo = StudentListView.RegisterNo) as table1  " +
                subqry +
               "  group by FeesBill_SessionId,session,RegisterNo, StudentID,DOB, FatherName,ClassId,BillId ,StudentName,ClassName,SectionName,RollNo  ";

            #endregion

            //Must Be Field Type Or Name same Then the Auto mapper Work other wise show the error
            var data = await db.Database.SqlQuery<StudentFeesListVM>(qry, param1).ToListAsync();
            return data;
        }

        #endregion

    }
}
