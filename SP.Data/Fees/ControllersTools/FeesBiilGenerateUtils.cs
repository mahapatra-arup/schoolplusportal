﻿using SP.Data.Fees.Models;
using SP.Data.Models;
using SP.Data.Others.Tools;
using SP.Data.Student;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using static SP.Data.Enums.StudentsEnum;
using static SP.Data.Enums.StudentsFeesEnum;

namespace SP.Data.Fees.ControllersTools
{
    public class FeesBiilGenerateUtils
    {
        #region -= Object=-
        SP_DataEntities db = new SP_DataEntities();
        FeesStructureUtils feesStructureUtils = new FeesStructureUtils();
        StudentTools studentTools = new StudentTools();
        FeesConfigUtils feesConfigUtils = new FeesConfigUtils();
        MonthTools monthTools = new MonthTools();

        #endregion

        #region Generate Fees Bill

        public async Task<bool> GenerateFeesBill(string registerNo, long classId, _AdmissionType _AdmissionType)
        {
            #region  var
            Guid _BillId = Guid.NewGuid();
            var isMonthly = (await feesConfigUtils.GetFeesConfig()).IsMonthly;
            var std_Registers = (await studentTools.GetStd_Register(registerNo));

            var _StudentID = std_Registers.StudentId;
            var _SessionId = std_Registers.SessionID;
            #endregion

            //Fees Bill
            var _FeesBill = new FeesBill
            {
                BillId = _BillId,
                Date = DateTime.Now,
                RegisterNo = registerNo,
                SessionId = _SessionId
            };

            //Save
            using (db = new SP_DataEntities())
            {
                db.Database.Log = Console.Write;

                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Fees Bill
                        db.FeesBills.Add(_FeesBill);
                        await db.SaveChangesAsync();

                        //Decission /Bill Details
                        switch (_AdmissionType)
                        {
                            case _AdmissionType.Admission:
                                #region Admission 
                                if (isMonthly)
                                {
                                    await MonthlyBillGenerteForAdmission(_BillId, classId, _StudentID);
                                }
                                else
                                {
                                    await YearlyBillGenerteForAdmission(_BillId, classId, _StudentID);
                                }
                                #endregion
                                break;
                            case _AdmissionType.Readmission:
                                #region readmission
                                if (isMonthly)
                                {
                                }
                                else
                                {

                                }
                                #endregion
                                break;
                        }
                        //Commit
                        transaction.Commit();

                        return true;
                    }
                    catch (DbEntityValidationException e)
                    {
                        transaction.Rollback();//New add
                        var newException = new FormattedDbEntityValidationException(e);
                        throw newException;
                    }
                }

                // return false;
            }
        }

        #region --==Admission==--
        private async Task YearlyBillGenerteForAdmission(Guid _BillId, long _ClassId, Guid _StudentID)
        {

            var lstFeesForAdmission = await feesStructureUtils.GetFeesForAdmission(_ClassId, _StudentID, CustomeAndFineFees.WithOutCustomAndIsFine);

            if (lstFeesForAdmission.IsValidList())
            {
                var data = lstFeesForAdmission.Select(a => new FeesBillDetail
                {
                    BillId = _BillId,
                    FeesAmountId = a.FeesAmounts.FirstOrDefault().FeesAmountID,
                    Fees = a.FeesAmounts.FirstOrDefault().Fees,

                    DueFees = a.FeesAmounts.FirstOrDefault().Fees,
                    PaidFees = 0,
                    Concession = 0
                }
                );

                //Fees Bill save
                db.FeesBillDetails.AddRange(data);
                await db.SaveChangesAsync();
            }
        }

        private async Task<bool> MonthlyBillGenerteForAdmission(Guid _BillId, long _ClassId, Guid _StudentID)
        {
            //LIST                  
            var lstFeesForAdmission = await feesStructureUtils.GetFeesForAdmission(_ClassId, _StudentID, CustomeAndFineFees.WithOutCustomAndIsFine);
            //MONTHS    
            var lstMonth = await monthTools.GetMonthsAsync(_ClassId);

            if (!lstFeesForAdmission.IsValidIEnumerable())
            { return false; }//return

            if (!lstMonth.IsValidList())
            { return false; }//return
            int i = 0;

            foreach (Month month in lstMonth)
            {
                string strMonth = month.MonthName;
                var Yearly_Fees = lstFeesForAdmission.Where(s => s.IsMonthly == false);
                var Monthly_Fees = lstFeesForAdmission.Where(s => s.IsMonthly == true);

                // ONLY FIRST MONTH ACCEPT YEARLY FEES
                if (i == 0)
                {
                    #region Yearly Fees Save
                    var data_Yearly = Yearly_Fees.Select(a => new FeesBillDetail
                    {
                        BillId = _BillId,
                        FeesAmountId = a.FeesAmounts.FirstOrDefault().FeesAmountID,
                        Fees = a.FeesAmounts.FirstOrDefault().Fees,
                        Month = strMonth,
                        BillType = FeesBillTypeModel.YEARLY,

                        DueFees = a.FeesAmounts.FirstOrDefault().Fees,
                        PaidFees = 0,
                        Concession = 0

                    }

              );

                    //Fees Bill save
                    db.FeesBillDetails.AddRange(data_Yearly);
                    await db.SaveChangesAsync();
                    #endregion
                }

                #region Monthly Fees Save
                var data_Monthly = Monthly_Fees.Select(a => new FeesBillDetail
                {
                    BillId = _BillId,
                    FeesAmountId = a.FeesAmounts.FirstOrDefault().FeesAmountID,
                    Fees = a.FeesAmounts.FirstOrDefault().Fees,
                    Month = strMonth,
                    BillType = FeesBillTypeModel.MONTHLY,

                    DueFees = a.FeesAmounts.FirstOrDefault().Fees,
                    PaidFees = 0,
                    Concession = 0
                }

             );

                //Fees Bill save
                db.FeesBillDetails.AddRange(data_Monthly);
                await db.SaveChangesAsync();
                #endregion

                i++;
            }

            return true;
        }
        #endregion 
        #endregion

        #region ==List==
        public async Task<List<StudentListView>> DueBillStudentList()
        {
            List<StudentListView> studentListViews = new List<StudentListView>();

            var slv = db.StudentListViews;
            // var feesBills = db.FeesBills;

            if (slv.IsValidIEnumerable())
            {
                studentListViews = await (from sl in slv
                                          where !(from fb in db.FeesBills
                                                  select fb.RegisterNo)
                           .Contains(sl.RegisterNo)
                                          select sl).ToListAsync();
            }
            return studentListViews;
        }
        #endregion

        #region Generate Custom Bill
        public async Task<int> CustomBillGenerate(List<FeesBillDetail> feesBillDetail)
        {
           
            //Fees Bill save
            db.FeesBillDetails.AddRange(feesBillDetail);
          return  await db.SaveChangesAsync();
        }
        #endregion
    }
}
