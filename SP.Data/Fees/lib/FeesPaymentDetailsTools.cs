﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP.Data.Fees
{
   public class FeesPaymentDetailsTools
    {
        SP_DataEntities db = new SP_DataEntities();

        /// <summary>
        /// Max Fees bill no
        /// </summary>
        /// <returns></returns>
        public async Task<int>  MaxFeesBillNo()
        {
            int maxslNo = 0;
            try
            {
                 maxslNo =  await db.FeesPaymentDetails.MaxAsync(s => s.BillNo);
            }
            catch (Exception)
            {
            }
            return  maxslNo;
            
        }
    }
}
