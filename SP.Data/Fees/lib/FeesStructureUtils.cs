﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using static SP.Data.Enums.StudentsEnum;
using static SP.Data.Enums.StudentsFeesEnum;

namespace SP.Data.Fees
{
    public class FeesStructureUtils
    {
        private SP_DataEntities db = new SP_DataEntities();


        #region -=List=-
        public async Task<List<FeesStructure>> GetFeesForAdmission(long _ClassId, Guid _StudentId, CustomeAndFineFees _CustomeAndFineFees)
        {

            List<FeesStructure> feesStructures = new List<FeesStructure>();
            switch (_CustomeAndFineFees)
            {
                case CustomeAndFineFees.WithCustom_OR_IsFine:
                    #region WithCustomAndIsFine
                    feesStructures = await (from fs in db.FeesStructures
                                            from fa in fs.FeesAmounts
                                            where fa.ClassId == _ClassId
                                            && fs.IsAdmission == true
                                            && (fs.IsFine == true || fs.IsCustom == true)

                                            //Hs lab subject Query
                                            //&& !(from fb in db.FeesBills
                                            //                      select fb.RegisterNo)
                                            //                .Contains(sl.RegisterNo)


                                            select fs).
                    ToListAsync();
                    #endregion
                    break;


                case CustomeAndFineFees.WithOutCustomAndIsFine:
                    #region WithOutCustomAndIsFine
                    feesStructures = await (from fs in db.FeesStructures
                                            from fa in fs.FeesAmounts

                                                //===Hs lab subject Query add====
                                                //let ifHs = .IsClassCategory.(from fb in db.HsStdSubjects select fb.SubjectId)
                                                //    .Contains(fs.SubjectId)

                                            where fa.ClassId == _ClassId
                                            && fs.IsAdmission == true
                                            && fs.IsFine == false && fs.IsCustom == false


                                            //===Hs lab subject Query add====
                                            // && ifHs
                                            //end

                                            select fs).
                    ToListAsync();
                    #endregion
                    break;

            }



            return feesStructures;
        }

        #endregion

        #region ==bool===
        public async Task<bool> IsHaveFeesAsync(long? classId)
        {
            var feesAmounts = db.FeesAmounts;
            if (feesAmounts.IsValidIEnumerable())
            {
                var sData = await (from fa in feesAmounts
                                   where fa.ClassId == classId
                                   select fa).ToListAsync();
                if (sData.IsValidList())
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region Fees Structure View Data

        public async Task<IEnumerable<FeesStructureView>> GetFeesStructureView(long _ClassId, _AdmissionType _AdmissionType, CustomeAndFineFees _CustomeAndFineFees)
        {
            List<FeesStructureView> feesStructureViews = new List<FeesStructureView>();

            #region Switch
            //Variable
            string qryCustomCheck = string.Empty;
            bool _IsFine = true;
            bool _IsCustom = true;

            switch (_CustomeAndFineFees)
            {
                case CustomeAndFineFees.WithCustom_OR_IsFine:
                    qryCustomCheck = ("(IsFine=@0 or IsCustom=@1)");/**/
                    break;
                case CustomeAndFineFees.WithOutCustomAndIsFine:
                    qryCustomCheck = ("IsFine!=@0 and IsCustom!=@1");/* */
                    break;

            }
            #endregion


            switch (_AdmissionType)
            {
                case _AdmissionType.Admission:
                    feesStructureViews = await db.FeesStructureViews.Where(f => f.ClassId == _ClassId)
                    .Where(g => g.IsAdmission == true).Where(qryCustomCheck, _IsFine, _IsCustom)
                    .ToListAsync();
                    break;
                case _AdmissionType.Readmission:
                    feesStructureViews = await db.FeesStructureViews.Where(f => f.ClassId == _ClassId)
                    .Where(g => g.IsReadmission == true).Where(qryCustomCheck, _IsFine, _IsCustom)
                    .ToListAsync();
                    break;
            }
            return feesStructureViews;
        }

        public async Task<FeesStructureView> GetFeesStructureView(long _ClassId, Guid _LedgerId)
        {
            var feesStructureView = await db.FeesStructureViews.Where(s => s.LedgerId == _LedgerId)
            .Where(c => c.ClassId == _ClassId)
            .FirstOrDefaultAsync();
            return feesStructureView;
        }

        public async Task<IEnumerable<FeesStructureView>> GetFeesStructureView(long _ClassId)
        {
            var feesStructureView = await db.FeesStructureViews
            .Where(c => c.ClassId == _ClassId).ToListAsync();
            return feesStructureView;
        }
        #endregion
    }
}
