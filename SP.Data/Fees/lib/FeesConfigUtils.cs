﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP.Data.Fees
{
    public class FeesConfigUtils
    {
        private SP_DataEntities db = new SP_DataEntities();
        //*********************************************************************************
        //Name : FeesConfig
        //Description : Is Define FeesConfig
        //*********************************************************************************
        public async Task<FeesConfig> GetFeesConfig()
        {
            var query =await db.FeesConfigs.FirstOrDefaultAsync();
            return query;
        }
    }
}
