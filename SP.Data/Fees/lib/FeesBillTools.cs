﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP.Data.Fees.lib
{
   public class FeesBillTools
    {
        private SP_DataEntities db = new SP_DataEntities();
        public async Task<List<FeesBillView>> GetFeesBillView(string RegiserNo)
        {
            var feesBillView = await db.FeesBillViews.Where(s => s.RegisterNo == RegiserNo).ToListAsync();
            return feesBillView;
        }
    }
}
