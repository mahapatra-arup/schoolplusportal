﻿namespace SP.Data.Fees.Models
{
    public class FeesBillTypeModel
    {
        public static string YEARLY { get => "YEARLY"; }
        public static string MONTHLY { get => "MONTHLY"; }
    }
}
