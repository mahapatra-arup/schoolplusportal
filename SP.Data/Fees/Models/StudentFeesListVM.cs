﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SP.Data.Fees.Models
{
    public class StudentFeesListVM
    {
        public int SessionId { get; set; }
        public string session { get; set; }
        public long ClassId { get; set; }
        public Guid BillId { get; set; }

        public Guid StudentID { get; set; }
        public string RegisterNo { get; set; }
        public string StudentName { get; set; }
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public int RollNo { get; set; }
        public DateTime DOB { get; set; }
        public string FatherName { get; set; }

        public double Fees { get; set; }
        public double PaidFees { get; set; }
        public double DueFees { get; set; }
        public double Concession { get; set; }
    }
}