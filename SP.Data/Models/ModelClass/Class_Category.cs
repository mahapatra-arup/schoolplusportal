
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(Class_CategoryMetadataType))]
    public partial class Class_Category
    {


    }
    public class Class_CategoryMetadataType
    {
        public long ID { get; set; }

       
        public string CategoryName { get; set; }

        
    }
}
