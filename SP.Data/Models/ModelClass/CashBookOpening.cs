
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using SysTools.Validator;

    [MetadataType(typeof(CashBookOpeningMetadata))]

    public partial class CashBookOpening
    {
    }

    public class CashBookOpeningMetadata
    {
        public int ID { get; set; }
        [Display(Name = "Cash book Opening Balance", Prompt = "Enter Cash book Opening Balance")]
        [Required(ErrorMessage = "Please Enter Cash book opening balance.")]

        public double OpeningAmount { get; set; }

        [Required(ErrorMessage = "Please Enter Cash book opening balance")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Cash book opening Date")]
        [DataType(DataType.Date)]
        public System.DateTime OpeningDate { get; set; }
    }
}
