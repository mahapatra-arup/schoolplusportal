
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;


    [MetadataType(typeof(NationalityMetaData))]
    public partial class Nationality
    {
    }
    public class NationalityMetaData
    {


        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter Nationality name.")]
        [MaxLength(200, ErrorMessage = "Nationality name must be under 200 characters.")]
        [Display(Name = "Nationality name : ", Prompt = "Enter Nationality Name.")]
        public string NationalityName { get; set; }


    }
}
