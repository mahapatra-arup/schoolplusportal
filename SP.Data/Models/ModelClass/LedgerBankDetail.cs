

namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(LedgerBankDetailMetadata))]
    public partial class LedgerBankDetail
    {
    }

    public  class LedgerBankDetailMetadata
    {
        public long ID { get; set; }
        public System.Guid LedgerID { get; set; }

        public System.Guid LedgerIDOF { get; set; }

        [Required(ErrorMessage = "Please Select Bank.")]
        [Display(Name = "Caste")]
        public int BankID { get; set; }

        [Required(ErrorMessage = "Please Enter Account No.")]
        [Display(Name = "Account No.",Prompt ="Enter Account No.")]
        [RegularExpression("([0-9]+)")]
        public string ACNo { get; set; }

        [Required(ErrorMessage = "Please Enter Bank IFSC Code.")]
        [Display(Name = "IFSC Code", Prompt = "Enter IFSC code")]
        [MaxLength(11, ErrorMessage = "IFSC code must be under 11 characters.")]
        public string IFSC { get; set; }

        [Display(Name = "MICR Code", Prompt = "Enter MICR code")]
        [MaxLength(9, ErrorMessage = "MICR code must be under 9 characters.")]
        public string MICR { get; set; }

        [Required(ErrorMessage = "Please Enter Branch Name")]
        [Display(Name = "Branch Name", Prompt = "Enter Branch Name")]
        [MaxLength(50, ErrorMessage = "Branch Name must be under 50 characters.")]
        public string BranchName { get; set; }

        [Display(Name = "Branch code", Prompt = "Enter Branch code")]
        [MaxLength(50, ErrorMessage = "Branch code must be under 50 characters.")]
        public string BranchCode { get; set; }

        [Display(Name = "Branch Address", Prompt = "Enter Branch Address")]
        [MaxLength(200, ErrorMessage = "Branch Address must be under 200 characters.")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Please Select Account Type")]
        [Display(Name = "Account Type")]
        [MaxLength(20, ErrorMessage = "Branch Address must be under 20 characters.")]
        public string ACType { get; set; }
    
    }
}
