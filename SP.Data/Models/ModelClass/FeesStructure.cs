

namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(FeesStructureMetadata))]
    public partial class FeesStructure
    {
    }
    public  class FeesStructureMetadata
    {
        public long ID { get; set; }
        public System.Guid FeesStructureID { get; set; }

        [Required(ErrorMessage = "Please Select Ledger.")]
        [Display(Name = "Ledger Name")]
        public System.Guid LedgerId { get; set; }
        public Nullable<long> SubjectId { get; set; }
        public System.DateTime Date { get; set; }
        public int SessionID { get; set; }
        public bool IsAdmission { get; set; }
        public bool IsReadmission { get; set; }
        public Nullable<bool> IsMonthly { get; set; }
        public Nullable<bool> IsFine { get; set; }
        public Nullable<bool> IsCustom { get; set; }
    
    }
}
