
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(BloodGroupMetaData))]
    public partial class BloodGroup
    {
    }

    public class BloodGroupMetaData
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter blood group.")]
        [MaxLength(20, ErrorMessage = "Blood group name must be under 20 characters.")]
        [Display(Name = "Group name", Prompt = "Enter blood group name.")]
        public string GroupName { get; set; }

      
    }
}
