

namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(FeesPaymentDetailsSubMetadata))]
    public partial class FeesPaymentDetailsSub
    {
    }

    public class FeesPaymentDetailsSubMetadata
    {
        public long Id { get; set; }
        public Nullable<System.Guid> paymentId { get; set; }
        public Nullable<long> FeesAmountId { get; set; }
        public Nullable<double> Amount { get; set; }
        public Nullable<double> Concession { get; set; }
        
    }
}
