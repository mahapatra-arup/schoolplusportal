
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(MotherTongueMetaData))]
    public partial class MotherTongue
    {
    }
    public class MotherTongueMetaData
    {


        public long Id { get; set; }

        [Required(ErrorMessage = "Please enter Mother Toungue.")]
        [MaxLength(200, ErrorMessage = "Mother Toungue must be under 200 characters.")]
        [Display(Name = "Mother Toungue : ", Prompt = "Enter Mother Toungue.")]
        public string MotherTongueName { get; set; }

       
    }
}
