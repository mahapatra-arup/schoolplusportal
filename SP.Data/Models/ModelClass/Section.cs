
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(SectionMetaData))]
    public partial class Section
    {
    }

    public class SectionMetaData
    {
        public long ID { get; set; }

        [Required(ErrorMessage = "Please select a class .")]
        [Display(Name = "Class name")]
        public long ClassID { get; set; }

        [Required(ErrorMessage = "Please enter section name.")]
        [MaxLength(50, ErrorMessage = "Section name must be under 50 characters.")]
        [Display(Name = "Sction name", Prompt = "Enter Section Name.")]
        public string SectionName { get; set; }
    }
}
