
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(ClassMetaDataMetaData))]
    public partial class ClassMetaData
    {
    }

    public  class ClassMetaDataMetaData
    {
    
        public long Id { get; set; }
        public int SlNo { get; set; }
        public int SessionId { get; set; }

        [Required(ErrorMessage = "Please enter name of the class.")]
        [Display(Name = "Class name : ", Prompt = "Enter class name.")]
        [MaxLength(50, ErrorMessage = "Class name must be under 50 characters.")]
        public string ClassName { get; set; }

        [Required(ErrorMessage = "Please  select category of the class.")]
        [Display(Name = "Category")]
        public long CategoryID { get; set; }
    
      
    }
}
