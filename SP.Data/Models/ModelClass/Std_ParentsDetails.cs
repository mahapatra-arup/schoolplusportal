
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(Std_ParentsDetailsMetaData))]
    public partial class Std_ParentsDetails
    {
    }

    public class Std_ParentsDetailsMetaData
    {
        public System.Guid StudentID { get; set; }

        [Required(ErrorMessage = "Please Enter Father's Name.")]
        [MaxLength(250, ErrorMessage = "Father's Name must be under 250 characters.")]
        [Display(Name = "Father's Name", Prompt = "Enter Father's Name.")]
        public string FatherName { get; set; }

        [MaxLength(500, ErrorMessage = "Father's address must be under 500 characters.")]
        [Display(Name = "Father's Address", Prompt = "Enter Father's Address.")]
        public string FatherAddress { get; set; }

        [MaxLength(100, ErrorMessage = "Father's Occupation must be under 100 characters.")]
        [Display(Name = "Father's Occupation", Prompt = "Enter Father's Occupation.")]
        public string FatherOccupation { get; set; }

        [Range(0, int.MaxValue)]
        [Display(Name = "Father's Anual Income", Prompt = "Enter Father's Anual Income.")]
        [RegularExpression("([0-9]+)")]
        public Nullable<int> FatherAI { get; set; }

        [MaxLength(50, ErrorMessage = "Father's Qualification must be under 50 characters.")]
        [Display(Name = "Father's Qualification", Prompt = "Enter Father's Qualification.")]
        public string FatherQualification { get; set; }

        [Display(Name = "Father's Image")]
        public byte[] FatherPhoto { get; set; }

       
        [Display(Name = "Father's contact No", Prompt = "Enter  Father's  contact No.")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Enter Valid Father's  Contact No.")]
        [RegularExpression("([0-9]+)")]
        public string FatherContactNo { get; set; }

        [Display(Name = "Father's Voter ID", Prompt = "Enter  Father's Voter ID.")]
        [MaxLength(100, ErrorMessage = "Father's Voter ID Must be under 100 characters.")]
        public string FatherVoterId { get; set; }

        [Required(ErrorMessage = "Please Enter Mother's Name.")]
        [MaxLength(250, ErrorMessage = "Mother's Name must be under 250 characters.")]
        [Display(Name = "Mother's Name", Prompt = "Enter Mother's Name.")]
        public string MotherName { get; set; }

        [MaxLength(500, ErrorMessage = "Mother's address must be under 500 characters.")]
        [Display(Name = "Mother's Address", Prompt = "Enter Mother's Address.")]
        public string MotherAddress { get; set; }

        [MaxLength(100, ErrorMessage = "Mother's Occupation must be under 100 characters.")]
        [Display(Name = "Mother's Occupation", Prompt = "Enter Mother's Occupation.")]
        public string MotherOccupation { get; set; }

        [Range(0, int.MaxValue)]
        [Display(Name = "Mother's Anual Income", Prompt = "Enter Mother's Anual Income.")]
        [RegularExpression("([0-9]+)")]
        public Nullable<int> MotherAI { get; set; }

        [MaxLength(50, ErrorMessage = "Mother's Qualification must be under 50 characters.")]
        [Display(Name = "Mother's Qualification", Prompt = "Enter Mother's Qualification.")]
        public string MotherQualification { get; set; }

        [Display(Name = "Mother's Image")]
        public byte[] MotherPhoto { get; set; }

        
        [Display(Name = "Mother's contact No", Prompt = "Enter  Mother's  contact No.")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Enter Valid Mother's  Contact No.")]
        [RegularExpression("([0-9]+)")]
        public string MotherContactNo { get; set; }

        [Display(Name = "Mother's Voter ID", Prompt = "Enter  Mother's Voter ID.")]
        [MaxLength(100, ErrorMessage = "Mother's Voter ID Must be under 100 characters.")]
        public string MotherVoterId { get; set; }


    }
}
