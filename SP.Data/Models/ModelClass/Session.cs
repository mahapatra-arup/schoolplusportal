
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(SessionMetaData))]
    public partial class Session
    {


    }

    public class SessionMetaData
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Please enter HS Session name.")]
        [MaxLength(20, ErrorMessage = "HS Session name must be under 20 characters.")]
        [Display(Name = "HS Session name", Prompt = "Enter HS Session Name.")]
        public string XIToXII { get; set; }

        [Required(ErrorMessage = "Please enter MP Session name.")]
        [MaxLength(20, ErrorMessage = "MP Session name must be under 20 characters.")]
        [Display(Name = "MP Session name", Prompt = "Enter MP Session Name.")]
        public string VToX { get; set; }

        public Nullable<bool> Defult { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> SlNo { get; set; }
        public Nullable<bool> DefultHS { get; set; }

    }
}
