
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(SubCasteMetaData))]
    public partial class SubCaste
    {
    }
    public class SubCasteMetaData
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please Enter Sub-Caste name.")]
        [MaxLength(200, ErrorMessage = "Sub-Caste  Name must be under 200 characters.")]
        [Display(Name = "Sub-Caste Name", Prompt = "Enter  Sub-Caste  Name.")]
        public string SubCasteName { get; set; }
    }
}
