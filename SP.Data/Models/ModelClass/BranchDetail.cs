
namespace SP.Data.Models
{
    using SP.Data.SysTools.Validator;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(BranchDetailMetaData))]
    public partial class BranchDetail
    { }
    public class BranchDetailMetaData
    {
        public long Id { get; set; }


        [Required(ErrorMessage = "Please select bank name.")]
        [Display(Name = "Bank name")]
        public int BankID { get; set; }

        [Required(ErrorMessage = "Please enter branch name.")]
        [MaxLength(100, ErrorMessage = "Branch name must be under 100 characters.")]
        [Display(Name = "Branch name", Prompt = "Enter Branch name.")]
        public string BranchName { get; set; }
        
        [Custom_Validator_IFSC_Length]
        [Required(ErrorMessage = "Please enter IFSC code.")]
        [Display(Name = "IFSC code", Prompt = "Enter IFSC code.")]
        public string IFSCCode { get; set; }

        [Display(Name = "MICR code", Prompt = "Enter MICR code.")]
        public string MICRCode { get; set; }

        [Display(Name = "Branch code", Prompt = "Enter Branch code.")]
        public string BranchCode { get; set; }

        [Display(Name = "Set As Default?", Prompt = "Enter Branch code.")]
        public string Defult { get; set; }
    }
      
}
