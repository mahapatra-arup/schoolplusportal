

namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(Std_MoreDetailsMetaData))]
    public partial class Std_MoreDetails
    {
    }
    public class Std_MoreDetailsMetaData
    {
        public long ID { get; set; }
        public System.Guid StudentID { get; set; }


        [Display(Name = "Mother Tongue")]
        public Nullable<long> MotherTongueID { get; set; }

       
        [Display(Name = "Contact No", Prompt = "Enter contact No.")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Enter Valid Contact No.")]
        [RegularExpression("([0-9]+)")]
        public string ContactNo { get; set; }
       
        [Range(12, 12)]
        [Display(Name = "Aadhaar No", Prompt = "Enter Aadhaar No.")]
        [RegularExpression("([0-9]+)")]
        public string AadhaarNo { get; set; }

        [Display(Name = "Previous School Name")]
        public Nullable<long> PreviousSchoolID { get; set; }

       
        [MaxLength(10, ErrorMessage = "Previous class must be under 10 characters.")]
        [Display(Name = "Previous Class", Prompt = "Enter Previous Class.")]
        public string PreviousClass { get; set; }

        [Required(ErrorMessage = "Please Enter Guardian's Name.")]
        [MaxLength(50, ErrorMessage = "Guardian Name must be under 50 characters.")]
        [Display(Name = "Guardian Name", Prompt = "Enter  Guardian Name.")]
        public string GuardianName { get; set; }

        [Required(ErrorMessage = "Please Enter Relation With Guardian.")]
        [MaxLength(250, ErrorMessage = "Relation must be under 250 characters.")]
        [Display(Name = "Relation With Guardian", Prompt = "Enter Relation with Guardian.")]
        public string RelationWithGuardian { get; set; }

        [Display(Name = "Guardian's Address", Prompt = "Enter  Guardian's Address.")]
        public string GuardianAddress { get; set; }

        [MaxLength(100, ErrorMessage = "Occupation must be under 100 characters.")]
        [Display(Name = "Guardian's Occupation", Prompt = "Enter Occupation of The Guardian.")]
        public string GuardianOccupation { get; set; }

        [Range(0, int.MaxValue)]
        [Display(Name = "Guardian's Anual Income", Prompt = "Enter Guardian's Anual Income.")]
        [RegularExpression("([0-9]+)")]
        public Nullable<int> GuardianAI { get; set; }


        [MaxLength(50, ErrorMessage = "Guardian's Qualification must be under 50 characters.")]
        [Display(Name = "Guardian's Qualification", Prompt = "Enter Guardian's Qualification.")]
        public string GuardianQualification { get; set; }

        [Display(Name = "Guardian's Image")]
        public byte[] GuardianPhoto { get; set; }

        [Required(ErrorMessage ="Enter Guardian's Contact No.")]
        
        [Display(Name = "Guardian's Contact No", Prompt = "Enter Guardian's contact No.")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Enter Valid Guardian's Contact No.")]
        [RegularExpression("([0-9]+)")]
        public string GuardianContactNo { get; set; }

       
        [Display(Name = "Guardian's 2nd Contact No", Prompt = "Enter  Guardian's 2nd contact No.")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Enter Valid Guardian's 2nd Contact No.")]
        [RegularExpression("([0-9]+)")]
        public string GuardianContactNo2 { get; set; }

        [Display(Name = "Guardian's Voter ID", Prompt = "Enter  Guardian's Voter ID.")]
        [MaxLength(100,ErrorMessage = "Guardian's Voter ID Must be under 100 characters.")]
        public string GuardianVoterId { get; set; }

        [Display(Name = "Guardian's Mail ID", Prompt = "Enter  Guardian's Mail ID.")]
        [MaxLength(100, ErrorMessage = "Guardian's Voter ID Must be under 100 characters.")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Enter Valid Guardian's Mail ID.")]
        public string GuardianMailID { get; set; }

        [Display(Name = "Hostel Need?")]
        public bool IsHostelNeed { get; set; }

        [Display(Name = "Transport Need?")]
        public bool IsTransportNeed { get; set; }


    }
}
