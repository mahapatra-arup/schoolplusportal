
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(PreviousSchoolMetaData))]
    public partial class PreviousSchool
    {
    }

    public class PreviousSchoolMetaData
    {


        public long Id { get; set; }
        [Required(ErrorMessage = "Please enter school name.")]
        [MaxLength(200, ErrorMessage = "school name must be under 200 characters.")]
        [Display(Name = "School name : ", Prompt = "Enter School Name.")]
        public string SchoolName { get; set; }

        [Display(Name = "Contact Info : ", Prompt = "Enter Contact Info.")]
        public string ContactInfo { get; set; }

       
    }
}
