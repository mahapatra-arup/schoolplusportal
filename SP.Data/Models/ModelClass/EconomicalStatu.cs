
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;


    [MetadataType(typeof(EconomicalStatuMetaData))]

    public partial class EconomicalStatu
    {
    }
    public class EconomicalStatuMetaData
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter economical status name.")]
        [MaxLength(200, ErrorMessage = "Economical name must be under 200 characters.")]
        [Display(Name = "Economical name : ", Prompt = "Enter Economical name.")]
        public string EconomicalStatusName { get; set; }


    }
}
