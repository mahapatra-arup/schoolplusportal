
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using SysTools.Validator;

    [MetadataType(typeof(CashBookMetaData))]
    public partial class CashBook
    {
    }
    public class CashBookMetaData
    {
        public long ID { get; set; }
        public System.Guid LedgerID { get; set; }
        public System.DateTime Date { get; set; }
        public Nullable<long> VchNo { get; set; }
        public Nullable<double> Dr { get; set; }
        public Nullable<double> Cr { get; set; }
        public string Narration { get; set; }
        public string Type { get; set; }
        public Nullable<long> TransectionNo { get; set; }

        public virtual Ledger Ledger { get; set; }
    }
}
