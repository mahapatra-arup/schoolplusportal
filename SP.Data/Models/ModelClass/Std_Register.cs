
namespace SP.Data.Models
{
    using SP.Data.SysTools;
    using SP.Data.SysTools.Attribute;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(Std_RegisterMetaData))]
    public partial class Std_Register
    {
    }
    public class Std_RegisterMetaData
    {
        public long Id { get; set; }
        public System.Guid StudentId { get; set; }

        [Display(Name ="Session")]
        public int SessionID { get; set; }

        [Display(Name = "Class")]
        public long ClassID { get; set; }

        [Display(Name = "Section")]
        public long SectionID { get; set; }

        [Required(ErrorMessage ="Please enter Roll No.")]
        [Display(Name = "Roll No",Prompt ="Enter Roll No")]
        [Range(1, int.MaxValue)]
        [RegularExpression("([0-9]+)")]
        public int RollNo { get; set; }

       
        [Required(ErrorMessage = "Register No Not found!!")]
        [Display(Name = "Register No")]
        public string RegisterNo { get; set; }
    }
}
