

namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;


    [MetadataType(typeof(StdAddressMetaData))]
    public partial class Std_Address
    {
    }

    public class StdAddressMetaData
    {
        public long Id { get; set; }
        public Nullable<System.Guid> StudentID { get; set; }

        #region ============ Present Address Details ===============

        [Required(ErrorMessage = "Please enter present village name.")]
        [MaxLength(50, ErrorMessage = "Present village name must be under 50 characters.")]
        [Display(Name = "Present village name", Prompt = "Enter present village Name.")]
        public string PrVill { get; set; }

        [MaxLength(50, ErrorMessage = "Present block name must be under 50 characters.")]
        [Display(Name = "Present block name", Prompt = "Enter present block Name.")]
        public string PrBlock { get; set; }

        [MaxLength(50, ErrorMessage = "Present post office name must be under 50 characters.")]
        [Display(Name = "Present post office name", Prompt = "Enter present post office Name.")]
        public string PrPO { get; set; }

        [MaxLength(50, ErrorMessage = "Present Gram panchayat name must be under 50 characters.")]
        [Display(Name = "Present Gram panchayat name", Prompt = "Enter present Gram panchayat Name.")]
        public string PrGP { get; set; }

        [MaxLength(50, ErrorMessage = "Present police station name must be under 50 characters.")]
        [Display(Name = "Present police station name", Prompt = "Enter present police station Name.")]
        public string PrPS { get; set; }

        [Required(ErrorMessage = "Please Select present District name.")]
        [Display(Name = "Present District name", Prompt = "Enter present district Name.")]
        public Nullable<int> PrDistID { get; set; }

        [MaxLength(6, ErrorMessage = "Present PIN Code must be under 6 characters.")]
        [Display(Name = "Present  PIN Code", Prompt = "Enter present  PIN Code.")]
        public string PrPIN { get; set; }

        #endregion

        #region ============= Permanent Address ============

        [Required(ErrorMessage = "Please enter permanent village name.")]
        [MaxLength(50, ErrorMessage = "permanent village name must be under 50 characters.")]
        [Display(Name = "permanent village name", Prompt = "Enter permanent village Name.")]
        public string PmVill { get; set; }

        [MaxLength(50, ErrorMessage = "Permanent block name must be under 50 characters.")]
        [Display(Name = "Permanent block name", Prompt = "Enter permanent block Name.")]
        public string PmBlock { get; set; }

        [MaxLength(50, ErrorMessage = "Permanent post office name must be under 50 characters.")]
        [Display(Name = "Permanent post office name", Prompt = "Enter permanent post office Name.")]
        public string PmPO { get; set; }

        [MaxLength(50, ErrorMessage = "Permanent Gram panchayat name must be under 50 characters.")]
        [Display(Name = "Permanent Gram panchayat name", Prompt = "Enter permanent Gram panchayat Name.")]
        public string PmGP { get; set; }

        [MaxLength(50, ErrorMessage = "Permanent police station name must be under 50 characters.")]
        [Display(Name = "Permanent police station name", Prompt = "Enter permanent police station Name.")]
        public string PmPS { get; set; }

        [Required(ErrorMessage = "Please Select permanent District name.")]
        [Display(Name = "Permanent District name", Prompt = "Enter permanent district Name.")]
        public Nullable<int> PmDistID { get; set; }

        [MaxLength(6, ErrorMessage = "Permanent PIN Code must be under 6 characters.")]
        [Display(Name = "Permanent  PIN Code", Prompt = "Enter permanent  PIN Code.")]
        public string PmPIN { get; set; }


        #endregion
    }
}
