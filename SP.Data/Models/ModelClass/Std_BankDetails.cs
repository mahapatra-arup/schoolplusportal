

namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(Std_BankDetails_MetaData))]
    public partial class Std_BankDetails
    {
    }
    public class Std_BankDetails_MetaData
    {
        public long ID { get; set; }
        public System.Guid StudentId { get; set; }

        [Required(ErrorMessage = "Please select Branch.")]
        [Display(Name = "Branch Name")]
        public Nullable<long> BranchDetailsID { get; set; }

        [Required(ErrorMessage = "Please enter Account No")]
        [MaxLength(50, ErrorMessage = "Account No must be under 50 characters.")]
        [Display(Name = "Account No.", Prompt = "Enter Account No.")]
        public string AccountNo { get; set; }

        [Required(ErrorMessage = "Please enter Account Holder Name.")]
        [MaxLength(100, ErrorMessage = "Account Holder name must be under 100 characters.")]
        [Display(Name = "Account Holder Name.", Prompt = "Enter Account Holder Name.")]
        public string AccountHolderName { get; set; }

        
    }
}
