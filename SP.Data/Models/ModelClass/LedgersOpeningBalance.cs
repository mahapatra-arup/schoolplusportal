

namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(LedgersOpeningBalanceMetadata))]
    public partial class LedgersOpeningBalance
    {
    }
    public class LedgersOpeningBalanceMetadata
    {
        public long Id { get; set; }
        public System.Guid LedgerID { get; set; }

        [Display(Name = "Ledger's Opening Balance", Prompt = "Enter Ledger's opening balance")]
        [MaxLength(50, ErrorMessage = "Ledger name must be under 50 characters.")]
        [RegularExpression("([0-9]+)")]
        public double OpeningBalance { get; set; }

        [Display(Name = "Opening Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public System.DateTime Date { get; set; }

        [Required(ErrorMessage  = "Please select Case or Bank.")]
        [Display(Name = "Cash/Bank")]
        public string Type { get; set; }

    }
}
