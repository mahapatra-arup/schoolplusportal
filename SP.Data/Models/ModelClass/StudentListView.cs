
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(StudentListViewMetadataType))]
    public partial class StudentListView
    {
    }
    public partial class StudentListViewMetadataType
    {
        [Display (Name ="Student Name")]
        public string StudentName { get; set; }

        [Display(Name = "Student ID")]
        public System.Guid StudentID { get; set; }

        [Display(Name = "Gender")]
        public string Gender { get; set; }

        [Display(Name = "Date of Birth")]
        public System.DateTime DOB { get; set; }

        [Display(Name = "Student Image")]
        public byte[] Photo { get; set; }

        [Display(Name = "Caste ID")]
        public int CasteID { get; set; }

        [Display(Name = "Sub-Caste ID")]
        public int SubCasteID { get; set; }

        [Display(Name = "Religion ID")]
        public int ReligionID { get; set; }

        [Display(Name = "Nationality ID")]
        public int NationalityID { get; set; }

        [Display(Name = "Economical Satus ID")]
        public int EconomicalSatusID { get; set; }

        [Display(Name = "Economical Status No")]
        public string EconomicalStatusNo { get; set; }
         
        [Display(Name = "Supporting Doc ID")]
        public Nullable<long> SupportingDocID { get; set; }

        [Display(Name = "Is PC Disability")]
        public bool IsPCDisability { get; set; }

        [Display(Name = "PC Per")]
        public Nullable<long> PCPer { get; set; }

        [Display(Name = "Disability ID")]
        public Nullable<long> DisabilityID { get; set; }

        [Display(Name = "Father's Name")]
        public string FatherName { get; set; }

        [Display(Name = "Father's Address")]
        public string FatherAddress { get; set; }

        [Display(Name = "Father's Occupation")]
        public string FatherOccupation { get; set; }

        [Display(Name = "Father's Anual Income")]
        public Nullable<int> FatherAI { get; set; }

        [Display(Name = "Father's Qualification")]
        public string FatherQualification { get; set; }

        [Display(Name = "Father's Image")]
        public byte[] FatherPhoto { get; set; }

        [Display(Name = "Father's Contact No")]
        public string FatherContactNo { get; set; }

        [Display(Name = "Father's Voter Id")]
        public string FatherVoterId { get; set; }

        [Display(Name = "Mother's Name")]
        public string MotherName { get; set; }

        [Display(Name = "Mother's Address")]
        public string MotherAddress { get; set; }

        [Display(Name = "Mother's Occupation")]
        public string MotherOccupation { get; set; }

        [Display(Name = "Mother's Anual Income")]
        public Nullable<int> MotherAI { get; set; }

        [Display(Name = "Mother's Qualification")]
        public string MotherQualification { get; set; }

        [Display(Name = "Mother's Image")]
        public byte[] MotherPhoto { get; set; }

        [Display(Name = "Mother's Contact No")]
        public string MotherContactNo { get; set; }

        [Display(Name = "Mother's Voter Id")]
        public string MotherVoterId { get; set; }

        [Display(Name = "Present Village")]
        public string PrVill { get; set; }

        [Display(Name = "Present Block")]
        public string PrBlock { get; set; }

        [Display(Name = "Present Post Office")]
        public string PrPO { get; set; }

        [Display(Name = "Present District ID")]
        public Nullable<int> PrDistID { get; set; }

        [Display(Name = "Present Gram Panchayat")]
        public string PrGP { get; set; }

        [Display(Name = "Present Police station")]
        public string PrPS { get; set; }

        [Display(Name = "Present PIN CODE")]
        public string PrPIN { get; set; }

        [Display(Name = "Permanent Village")]
        public string PmVill { get; set; }

        [Display(Name = "Permanent Block")]
        public string PmBlock { get; set; }

        [Display(Name = "Permanent Post Office")]
        public string PmPO { get; set; }

        [Display(Name = "Permanent Gram Panchayat")]
        public string PmGP { get; set; }

        [Display(Name = "Permanent Police Station")]
        public string PmPS { get; set; }

        [Display(Name = "Permanent Dist ID")]
        public Nullable<int> PmDistID { get; set; }

        [Display(Name = "Permanent PIN CODE")]
        public string PmPIN { get; set; }

        [Display(Name = "Mother Tongue ID")]
        public Nullable<long> MotherTongueID { get; set; }

        [Display(Name = "Contact No")]
        public string ContactNo { get; set; }

        [Display(Name = "Aadhaar No")]
        public string AadhaarNo { get; set; }

        [Display(Name = "Previous School ID")]
        public Nullable<long> PreviousSchoolID { get; set; }

        [Display(Name = "Previous Class")]
        public string PreviousClass { get; set; }

        [Display(Name = "Guardian's Name")]
        public string GuardianName { get; set; }

        [Display(Name = "Relation With Guardian")]
        public string RelationWithGuardian { get; set; }

        [Display(Name = "Guardian's Occupation")]
        public string GuardianOccupation { get; set; }

        [Display(Name = "Guardian's Address")]
        public string GuardianAddress { get; set; }

        [Display(Name = "Guardian's Anual Income")]
        public Nullable<int> GuardianAI { get; set; }

        [Display(Name = "Guardian's Anual Qualification")]
        public string GuardianQualification { get; set; }

        [Display(Name = "Guardian's Image")]
        public byte[] GuardianPhoto { get; set; }

        [Display(Name = "Guardian's Contact No")]
        public string GuardianContactNo { get; set; }

        [Display(Name = "Guardian's 2nd Contact No")]
        public string GuardianContactNo2 { get; set; }

        [Display(Name = "Guardian's Voter Id")]
        public string GuardianVoterId { get; set; }

        [Display(Name = "Guardian's Mail ID")]
        public string GuardianMailID { get; set; }

        [Display(Name = "Is Hostel Need")]
        public bool IsHostelNeed { get; set; }

        [Display(Name = "Is Transport Need")]
        public bool IsTransportNeed { get; set; }

        [Display(Name = "Permanent Disease")]
        public string PermanentDisease { get; set; }

        [Display(Name = "Student's Weight")]
        public Nullable<double> Weight { get; set; }

        [Display(Name = "Student's Height")]
        public Nullable<double> Height { get; set; }

        [Display(Name = "Student's Chest")]
        public Nullable<double> Chest { get; set; }

        [Display(Name = "Student's Body Color")]
        public string BodyColor { get; set; }

        [Display(Name = "Student's Blood Group ID")]
        public Nullable<int> BloodGroupID { get; set; }

        [Display(Name = "Student's Blood Group")]
        public string BloodGroupName { get; set; }

        [Display(Name = "Student's Identification Mark")]
        public string IdentificationMark { get; set; }

        [Display(Name = "Nationality")]
        public string NationalityName { get; set; }

        [Display(Name = "Economical Status")]
        public string EconomicalStatusName { get; set; }

       
        public long Std_BankDetails_ID { get; set; }
        public long BranchDetailsID { get; set; }

        [Display(Name = "Account No")]
        public string AccountNo { get; set; }
         
        [Display(Name = "Account Holder's Name")]
        public string AccountHolderName { get; set; }

        
        public int BankID { get; set; }

        [Display(Name = "Branch Name")]
        public string BranchName { get; set; }

        [Display(Name = "IFSC Code")]
        public string IFSCCode { get; set; }

        [Display(Name = "MICR Code")]
        public string MICRCode { get; set; }

        [Display(Name = "Branch Code")]
        public string BranchCode { get; set; }

       
        public string Defult { get; set; }

        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

        [Display(Name = "Bank Short Name")]
        public string ShortName { get; set; }

        [Display(Name = "District Name")]
        public string DistName { get; set; }

        [Display(Name = "State ID")]
        public Nullable<int> StateID { get; set; }

        [Display(Name = "State")]
        public string State { get; set; }

        [Display(Name = "Mother Tongue Name")]
        public string MotherTongueName { get; set; }

        [Display(Name = "Previous School Name")]
        public string SchoolName { get; set; }

        [Display(Name = "Previous School Contact Info.")]
        public string PreviousSchool_ContactInfo { get; set; }

         [Display(Name = "Caste Name")]
        public string CasteName { get; set; }

        [Display(Name = "Disability Category Name")]
        public string CatagoryName { get; set; }

        [Display(Name = "Religion")]
        public string Religion { get; set; }

        [Display(Name = "Supportting Document")]
        public string DocumentName { get; set; }

        [Display(Name = "Sub-Caste Name")]
        public string SubCasteName { get; set; }

        
        public long Std_Register_ClassID { get; set; }
        public long Std_Register_SectionID { get; set; }

        [Display(Name = "Roll No")]
        public int RollNo { get; set; }

        [Display(Name = "Register No")]
        public string RegisterNo { get; set; }


        public int Std_Register_SessionID { get; set; }
        public Nullable<long> Std_AdmissionDetails_ClassID { get; set; }
        public Nullable<int> Std_AdmissionDetails_SessionID { get; set; }

        [Display(Name = "Admission No")]
        public string AdmissionNo { get; set; }

        [Display(Name = "Admission Date")]
        public Nullable<System.DateTime> AdmissionDate { get; set; }
        public long Std_AdmissionDetails_ID { get; set; }

        [Display(Name = "Section")]
        public string SectionName { get; set; }

        [Display(Name = "Calss")]
        public string ClassName { get; set; }

        [Display(Name = "Calss category Id")]
        public long Class_CategoryID { get; set; }
        public int Class_SlNo { get; set; }

        [Display(Name = "Calss Class Category")]
        public string CategoryName { get; set; }

        [Display(Name = "Session")]
        public string session { get; set; }

    }
}
