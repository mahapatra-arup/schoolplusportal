namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(HSSubjectMetaData))]
    public partial class HSSubject
    {

    }
    public partial class HSSubjectMetaData
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Please enter Subject name.")]
        [MaxLength(200, ErrorMessage = "Subject name must be under 200 characters.")]
        [Display(Name = "Subject name", Prompt = "Enter Subject name.")]
        public string SubjectName { get; set; }

        public string SubjectCode { get; set; }
        public Nullable<bool> IsLanguage { get; set; }
        public Nullable<bool> IsLab { get; set; }
    }
}
