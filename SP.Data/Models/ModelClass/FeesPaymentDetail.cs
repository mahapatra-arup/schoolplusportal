

namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(FeesPaymentDetailMetadata))]
    public partial class FeesPaymentDetail
    {
    }
    public class FeesPaymentDetailMetadata
    {
        public System.Guid PaymentId { get; set; }
        public Nullable<System.Guid> BillId { get; set; }

        [Required(ErrorMessage = "Please Enter Valid Bill No.")]
        [Display(Name = "Bill No")]
        public int BillNo { get; set; }

        [Display(Name = "Date", Prompt = "Enter date.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> Date { get; set; }

        
        [Display(Name = "Amount")]
        public Nullable<double> Amount { get; set; }

        [Display(Name = "Payment Month")]
        public string PaymentMonth { get; set; }

       
        [Display(Name = "Status")]
        public string Status { get; set; }

        public Nullable<System.Guid> PaymentByBankID { get; set; }

        [Display(Name = "Cheque No", Prompt = "Enter Check No.")]
        public string ChequeNo { get; set; }

        [Display(Name = "Cheque Date", Prompt = "Enter Cheque Date.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> ChequeDate { get; set; }

    }
}
