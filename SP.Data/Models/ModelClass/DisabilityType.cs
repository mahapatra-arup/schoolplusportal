
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(DisabilityTypeMetaData))]
    public  partial class DisabilityType
    {


    }
    public class DisabilityTypeMetaData
    {

        public long Id { get; set; }

        [Required(ErrorMessage = "Please enter category name.")]
        [MaxLength(100, ErrorMessage = "Category name must be under 100 characters.")]
        [Display(Name = "Category name : ", Prompt = "Enter category name.")]
        public string CatagoryName { get; set; }

    }
}
