
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;


    [MetadataType(typeof(SupportingDocMetaData))]
    public partial class SupportingDoc
    {
    }
    public class SupportingDocMetaData
    {

        public long Id { get; set; }

        [Required(ErrorMessage = "Please Enter Supporting Document Name.")]
        [MaxLength(200, ErrorMessage = "Supporting Document Name must be under 200 characters.")]
        [Display(Name = "Supporting Document Name", Prompt = "Enter  Supporting Document Name.")]
        public string DocumentName { get; set; }
    }
}
