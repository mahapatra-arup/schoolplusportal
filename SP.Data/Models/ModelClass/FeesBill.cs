

namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    [MetadataType(typeof(FeesBillMetadata))]
    public partial class FeesBill
    {
    }
    public class FeesBillMetadata
    {

        public long Id { get; set; }
        public System.Guid BillId { get; set; }

        [Display(Name = "Register No")]
        [Required(ErrorMessage = "Please Enter Register No")]
        public string RegisterNo { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Bill date")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Date { get; set; }
        public int SessionId { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }


    }
}
