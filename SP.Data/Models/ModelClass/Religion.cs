
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(ReligionMetaData))]
    public partial class Religion
    {
    }

    public class ReligionMetaData
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter Religion name.")]
        [MaxLength(200, ErrorMessage = "Religion name must be under 200 characters.")]
        [Display(Name = "Religion name : ", Prompt = "Enter religion name.")]
        public string Religion1 { get; set; }

    }

}
