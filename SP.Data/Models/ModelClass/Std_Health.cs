
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(Std_HealthMetaData))]
    public partial class Std_Health
    {
    }

    public class Std_HealthMetaData
    {
        public long ID { get; set; }
        public System.Guid StudentID { get; set; }

        [Display(Name = "Student's Weight ", Prompt = "Enter Student's Weight.")]
        [RegularExpression("([0-9]+)")]
        [Range(10,200, ErrorMessage = "Enter Weight between 10 and 200")]
        public Nullable<double> Weight { get; set; }

        [Display(Name = "Student's Height ", Prompt = "Enter Student's Height.")]
        [RegularExpression("([0-9]+)")]
        [Range(1, 10, ErrorMessage = "Enter Height between 1 and 10")]
        public Nullable<double> Height { get; set; }

        [Display(Name = "Student's Chest ", Prompt = "Enter Student's Chest.")]
        [RegularExpression("([0-9]+)")]
        [Range(1, 500, ErrorMessage = "Enter Height between 1 and 500")]
        public Nullable<double> Chest { get; set; }

        [Display(Name = "Student's Body Color ", Prompt = "Enter Student's Body Color.")]
        [MaxLength(50)]
        public string BodyColor { get; set; }
        public Nullable<int> BloodGroupID { get; set; }

        [Display(Name = "Student's Identification Mark ", Prompt = "Enter Student's Identification Mark.")]
        [MaxLength(250)]
        public string IdentificationMark { get; set; }

        [Display(Name = "Student's Permanent Disease ", Prompt = "Enter Student's Permanent Disease.")]
        [MaxLength(25)]
        public string PermanentDisease { get; set; }


    }
}
