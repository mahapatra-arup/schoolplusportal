//------------------------------------------------------------------------------

namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(DistrictMetaData))]
    public partial class District
    {
    }

    public class DistrictMetaData
    {


        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter District name.")]
        [MaxLength(50, ErrorMessage = "District name must be under 50 characters.")]
        [Display(Name = "District name : ", Prompt = "Enter District name.")]
        public string DistName { get; set; }

        [Required(ErrorMessage = "Please select state name.")]
        [Display(Name = "State name : ")]
        public Nullable<int> StateID { get; set; }

    }
}
