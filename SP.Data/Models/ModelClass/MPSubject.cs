namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(MPSubjectMetaData))]
    public partial class MPSubject
    {

    }
    public partial class MPSubjectMetaData
    {
        public long ID { get; set; }

        [Required(ErrorMessage = "Please enter Subject name.")]
        [MaxLength(200, ErrorMessage = "Subject name must be under 200 characters.")]
        [Display(Name = "Subject name", Prompt = "Enter Subject name.")]
        public string SubjectName { get; set; }

        [Required(ErrorMessage = "Please enter Sl. No.")]
        [Display(Name = "Sl.No.", Prompt = "Enter Sl. No.")]
        public long SlNo { get; set; }

        [Required(ErrorMessage = "Please selct valid Session")]
        [Display(Name = "Session", Prompt = "Enter Session")]
        public int SessionId { get; set; }

        [Required(ErrorMessage = "Please select valid Subject")]
        [Display(Name = "Subject", Prompt = "Enter Subject")]
        public string SubjectCode { get; set; }
       
    }
}
