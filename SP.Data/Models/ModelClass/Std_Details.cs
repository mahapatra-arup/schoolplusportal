
namespace SP.Data.Models
{
    using SP.Data.SysTools.Validator;
    using System;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(Std_DetailsMetaData))]
    public partial class Std_Details
    {

    }
    public class Std_DetailsMetaData
    {

        public long ID { get; set; }
        public System.Guid StudentID { get; set; }


        [Required(ErrorMessage = "Please Select Session.")]
        [Display(Name = "Session")]
        public int SessionID { get; set; }

        [Required(ErrorMessage = "Please enter Account Holder Name.")]
        [MaxLength(100, ErrorMessage = "Account Holder name must be under 100 characters.")]
        [Display(Name = "Student Name.", Prompt = "Enter Account Holder Name.")]
        public string StudentName { get; set; }

        [Required(ErrorMessage = "Please Select Gender.")]
        [MaxLength(10, ErrorMessage = "Gender name must be under 10 characters.")]
        [Display(Name = "Gender", Prompt = "Enter Gender Name.")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Please Enter  Date Of Birth of the Student's.")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date of birth")]
        [Custom_Validator_Date]
        [DataType(DataType.Date)]
        public System.DateTime DOB { get; set; }


        [Display(Name = "Student Image")]
        public byte[] Photo { get; set; }

        [Required(ErrorMessage = "Please Select Type of Caste.")]
        [Display(Name = "Caste")]
        public int CasteID { get; set; }

        [Required(ErrorMessage = "Please Select Type of Sub Caste.")]
        [Display(Name = "Sub Caste")]
        public int SubCasteID { get; set; }

        [Required(ErrorMessage = "Please Select Type of Religion.")]
        [Display(Name = "Religion")]
        public int ReligionID { get; set; }

        [Required(ErrorMessage = "Please Select Type of Nationality.")]
        [Display(Name = "Nationality")]
        public int NationalityID { get; set; }


        [Display(Name = "Economical Status")]
        public int EconomicalSatusID { get; set; }


        [MaxLength(50, ErrorMessage = "Economical Status No must be under 50 characters.")]
        [Display(Name = "Economical Status No", Prompt = "Enter Economical Status No.")]
        public string EconomicalStatusNo { get; set; }


        [Display(Name = "Supporting Document")]
        public Nullable<long> SupportingDocID { get; set; }


        [Display(Name = "Is Pc Disability?")]
        public bool IsPCDisability { get; set; }

       
        [Display(Name = "PC Percentage", Prompt = "Enter PC Percentage.")]
        public Nullable<long> PCPer { get; set; }


        [Display(Name = "Disability  Type")]
        public Nullable<long> DisabilityID { get; set; }

    }
}
