
namespace SP.Data.Models
{
    using SP.Data.SysTools.Attribute;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(Std_AdmissionDetailsMetaData))]
    public partial class Std_AdmissionDetails
    {
    }

    public class Std_AdmissionDetailsMetaData
    {
        public long Id { get; set; }

        
        public Nullable<System.Guid> StudentId { get; set; }

        //[Required(ErrorMessage = "Please Select Session.")]
        [Display(Name = "Session")]
        public Nullable<int> SessionId { get; set; }

        //[RemoteClientServer("IsAdmissionNoExist", "StdAdmissionController", "Student", ErrorMessage = "Admission No already in use")]
        [Required(ErrorMessage = "Please enter Addmission No.")]
        [MaxLength(50, ErrorMessage = "Addmission No. must be under 50 characters.")]
        [Display(Name = "Addmission No.", Prompt = "Enter Addmission No.")]
        public string AdmissionNo { get; set; }

        [Required(ErrorMessage = "Please enter Addmission Date.")]
        [Display(Name = "Addmission date", Prompt = "Enter Addmission date.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> AdmissionDate { get; set; }
        public Nullable<long> ClassID { get; set; }
    }
}
