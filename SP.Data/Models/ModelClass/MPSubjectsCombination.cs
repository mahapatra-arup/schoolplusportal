
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(MPSubjectsCombinationMetaData))]
    public partial class MPSubjectsCombination
    {

    }
    public partial class MPSubjectsCombinationMetaData
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Please selct valid Group")]
        [Display(Name = "Group", Prompt = "Enter Group name")]
        public Nullable<long> GroupId { get; set; }

        [Required(ErrorMessage = "Please selct valid Class")]
        [Display(Name = "Class", Prompt = "Enter Class")]
        public Nullable<long> ClassId { get; set; }

        [Required(ErrorMessage = "Please selct valid Subject")]
        [Display(Name = "Subject", Prompt = "Enter Subject")]
        public Nullable<long> SubjectId { get; set; }

        [Required(ErrorMessage = "Please Enter Subject Type")]
        [Display(Name = "Subject Type", Prompt = "Enter Subject Type")]
        public string SubjectType { get; set; }
    
    }
}
