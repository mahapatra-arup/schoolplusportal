
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(MpStdSubjectMetaData))]
    public partial class MpStdSubject
    {

    }
    public partial class MpStdSubjectMetaData
    {
        public long Id { get; set; }
        public string RegisterNo { get; set; }
        public Nullable<long> SubjectId { get; set; }
    
        public virtual MPSubject MPSubject { get; set; }
        public virtual Std_Register Std_Register { get; set; }
    }
}
