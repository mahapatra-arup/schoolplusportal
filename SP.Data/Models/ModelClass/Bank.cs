
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(BankMetaData))]
    public partial class Bank
    {

    }
    public  class BankMetaData
    {
      
      
        public int ID { get; set; }

        [Required(ErrorMessage ="Please enter bank name.")]
        [MaxLength(200,ErrorMessage = "Bank name must be under 200 characters.")]
        [Display(Name ="Bank name",Prompt ="Enter bank name.")]
        public string BankName { get; set; }
        public string Code { get; set; }
        public string ShortName { get; set; }
       
    }
}
