namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(HSSubjectsCombinationMetaData))]
    public partial class HSSubjectsCombination
    {

    }
    public partial class HSSubjectsCombinationMetaData
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Please selct valid Group")]
        [Display(Name = "Group", Prompt = "Enter Group name")]
        public Nullable<long> GroupId { get; set; }

        [Required(ErrorMessage = "Please selct valid Stream")]
        [Display(Name = "Stream", Prompt = "Enter Stream")]
        public Nullable<long> StreamId { get; set; }

        [Required(ErrorMessage = "Please selct valid Subject")]
        [Display(Name = "Subject", Prompt = "Enter Subject")]
        public Nullable<long> SubjectId { get; set; }
    
    }
}
