

namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(LedgerMetadata))]
    public partial class Ledger
    {
    }
    public  class LedgerMetadata
    {

        public long Id { get; set; }
        public System.Guid LedgerId { get; set; }

        [Required(ErrorMessage ="Please Enter Ledger Name.")]
        [Display(Name ="Ledger Name",Prompt ="Enter Ledger Name")]
        [MaxLength(50, ErrorMessage = "Ledger name must be under 50 characters.")]
        public string LedgerName { get; set; }

        [Required(ErrorMessage = "Please select ledger category .")]
        [Display(Name = "Ledger Category")]
        public string Category { get; set; }

        [Required(ErrorMessage = "Please select type of Ledger.")]
        [Display(Name = "Ledger Type")]
        public string Type { get; set; }
        public Nullable<System.Guid> ParentId { get; set; }

        [Display(Name = "Is It Fiexed Ledger?")]
        public bool IsFixed { get; set; }

        [Display(Name = "Is It Fees Ledger?")]
        public bool IsFees { get; set; }

        [Display(Name = "Is It categorised Ledger?")]
        public bool IsAudit { get; set; }

        [Display(Name = "Is It Lab Ledger?")]
        public bool IsLab { get; set; }

        [Display(Name = "Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public System.DateTime Date { get; set; }
        
    }
}
