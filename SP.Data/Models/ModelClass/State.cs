
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(StateMetaData))]
    public partial class State
    {
    }
    public class StateMetaData
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Please enter state name.")]
        [MaxLength(50, ErrorMessage = "State name must be under 50 characters.")]
        [Display(Name = "State name", Prompt = "Enter State Name.")]
        public string State1 { get; set; }
    }
}
