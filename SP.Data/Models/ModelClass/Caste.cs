
namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(CasteMetaData))]
    public partial class Caste
    { }
    public class CasteMetaData
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter caste name.")]
        [MaxLength(200, ErrorMessage = "Caste name must be under 200 characters.")]
        [Display(Name = "Caste name : ", Prompt = "Enter Caste name.")]
        public string CasteName { get; set; }

       
    }
}
