

namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(FeesBillDetailMetadata))]
    public partial class FeesBillDetail
    {
    }
    public  class FeesBillDetailMetadata
    {
        public long BillDetailsID { get; set; }
        public Nullable<System.Guid> BillId { get; set; }
        public Nullable<long> FeesAmountId { get; set; }

        [Display(Name ="Total Fees Amount")]
        public Nullable<double> Fees { get; set; }

        [Display(Name = "Total Due Amount")]
        public Nullable<double> DueFees { get; set; }

        [Display(Name = "Fees Month")]
        public string Month { get; set; }
        public string BillType { get; set; }


    }
}
