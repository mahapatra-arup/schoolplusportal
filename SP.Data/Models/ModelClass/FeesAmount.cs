

namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;


    [MetadataType(typeof(FeesAmountMetadata))]
    public partial class FeesAmount
    {
    }
    public class FeesAmountMetadata
    {
        public long FeesAmountID { get; set; }

        [Required(ErrorMessage ="Please Give Fees Stucture ID")]
        public System.Guid FeesStructureID { get; set; }

        [Required(ErrorMessage = "Please Enter Fees amount")]
        [Range(0, double.MaxValue)]
        [Display(Name = "Fees Amount", Prompt = "Enter Fess Amount")]
        [RegularExpression("([0-9]+)")]
        public double Fees { get; set; }

        [Required(ErrorMessage = "Please Enter Class name.")]
        [Display(Name = "Class", Prompt = "Enter Class name")]
        public long ClassId { get; set; }

    }
}
