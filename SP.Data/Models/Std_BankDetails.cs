//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SP.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Std_BankDetails
    {
        public long ID { get; set; }
        public System.Guid StudentId { get; set; }
        public Nullable<long> BranchDetailsID { get; set; }
        public string AccountNo { get; set; }
        public string AccountHolderName { get; set; }
    
        public virtual BranchDetail BranchDetail { get; set; }
        public virtual Std_Details Std_Details { get; set; }
    }
}
