﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP.Data.Models.others
{
   public class ControllerStructureModel
    {
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string AreaName { get; set; }
        public string ViewName { get; set; }
        public string ViewPath { get; set; }
        public string Layout { get; set; }
    }
}
