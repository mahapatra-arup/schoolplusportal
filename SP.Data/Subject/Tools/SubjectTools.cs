﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using SP.Data.Models.others;

namespace SP.Data.Subject
{
    public class SubjectTools
    {
        private SP_DataEntities db = new SP_DataEntities();
        public SubjectTools()
        {
            db = new SP_DataEntities();
        }

        #region Mp
        /// <summary>
        /// 
        /// </summary>
        /// <param name="MpSessionId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<MPSubject>> GetMPSubjectAsync(int MpSessionId)
        {
            var lstsub = db.MPSubjects.Include(b => b.Session);
            IEnumerable<MPSubject> subData = null;
            if (lstsub.IsValidIEnumerable())
            {
                subData =await lstsub.Where(s => s.SessionId == MpSessionId).ToListAsync();
            }
            return subData;
        }

        /// <summary>
        /// Get MPSubject Not In CombinationSubject
        /// </summary>
        /// <param name="MpSessionId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<MPSubject>> GetMPSubNotInCombinationSubAsync(int MpSessionId)
        {
            var lstsub = db.MPSubjects.Include(b => b.Session);
            IEnumerable<MPSubject> subData = null;
            if (lstsub.IsValidIEnumerable())
            {
                subData =await lstsub.Where(s => s.SessionId == MpSessionId)
                    .Where(a => !db.MPSubjectsCombinations.Any(b => b.SubjectId == a.ID)).ToListAsync();
            }
            return  subData;
        }

        #region ======Bool===
        public async Task<bool> IsValidMpSubjectAsync(string subjectName, int MpSessionId)
        {
            var s = db.MPSubjects;
            MPSubject _SubDetails = new MPSubject();

            if (s.ISValidObject())
            {
                var cls =await (from sub in s
                          where sub.SubjectName == subjectName && sub.SessionId == MpSessionId
                          select sub).ToListAsync();
                _SubDetails = cls.ISValidObject() ? cls.FirstOrDefault() : new MPSubject();
            }
            return _SubDetails.ISValidObject() ? _SubDetails.SubjectName.ISValidObject() : false;
        }

        #endregion
        #endregion

        #region HS
        /// <summary>
        /// 
        /// </summary>
        /// <param name="MpSessionId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<HSSubject>> GetHSLabSubjectAsync()
        {
            var lstsub = db.HSSubjects;
            List<HSSubject> subData = new List<HSSubject>();
            if (lstsub.IsValidIEnumerable())
            {
                subData = await lstsub.Where(s => s.IsLab==true).ToListAsync();
            }
            return subData;
        }
        #endregion
    }
}
