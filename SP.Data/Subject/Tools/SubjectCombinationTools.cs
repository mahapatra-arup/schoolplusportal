﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace SP.Data.Subject
{
    public class SubjectCombinationTools
    {
        private SP_DataEntities db = new SP_DataEntities();
        public SubjectCombinationTools()
        {
            db = new SP_DataEntities();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<MPSubjectsCombination>> GetMpSubjectsCombinationsAsync()
        {
            var mPSubjectsCombinations = db.MPSubjectsCombinations.Include(m => m.Class).Include(m => m.MPSubject).Include(m => m.SubjectsGroup);
            return await mPSubjectsCombinations.ToListAsync();
        }
        /// <summary>
       
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public async Task<List<MPSubjectsCombination>> GetMpSubjectsCombinationsAsync(long? classId, long? groupId)
        {
            var mPSubjectsCombinations = db.MPSubjectsCombinations.Include(m => m.Class).Include(m => m.MPSubject).Include(m => m.SubjectsGroup)
                .Where(c => c.ClassId == classId).Where(g => g.GroupId == groupId);
            return await mPSubjectsCombinations.ToListAsync();
        }

        #region ======Bool===
        public async  Task<bool> IsDublicateMpSubjectsForCombinationAsync(long? classId, long? groupId,long? subjectId)
        {
            var s = db.MPSubjectsCombinations;
            MPSubjectsCombination _SubDetails = new MPSubjectsCombination();

            if (s.ISValidObject())
            {
                var cls = from sub in s
                          where sub.ClassId == classId && sub.GroupId == groupId && sub.SubjectId==subjectId
                          select sub;
                _SubDetails = cls.ISValidObject() ?await cls.FirstOrDefaultAsync() : new MPSubjectsCombination();
            }
            return _SubDetails.ISValidObject() ? _SubDetails.SubjectId.ISValidObject() : false;
        }

        #endregion 
    }
}
