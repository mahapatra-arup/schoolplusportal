﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP.Data.Subject
{
    public class SubjectsGroupTools
    {
        private SP_DataEntities db = new SP_DataEntities();
        public SubjectsGroupTools()
        {
            db = new SP_DataEntities();
        }
       
        public async Task<IEnumerable<SubjectsGroup>> GetSubjectsGroupAsync()
        {
           return await db.SubjectsGroups.ToListAsync();
        }
    }
}
