﻿
using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace SP.Data.Student
{
    public class StudentTools
    {
        private SP_DataEntities db = new SP_DataEntities();


        #region list
        public async Task<IEnumerable<StudentListView>> GetStudentListViewAsync(long classId)
        {
            return await db.StudentListViews.Where(s => s.Std_Register_ClassID == classId).ToListAsync();
        }

        public async Task<Std_Register> GetStd_Register(string registerNo)
        {
            var Std_Registe = await db.Std_Register.Where(s => s.RegisterNo == registerNo).FirstOrDefaultAsync();
            return Std_Registe;
        }

        public async Task<StudentListView> GetStudentListViewAsync(string registerNo)
        {
            return await db.StudentListViews.Where(s => s.RegisterNo == registerNo).FirstOrDefaultAsync();
        }
        #endregion

        #region Bool
        public async Task<bool> IsAdmissionNoExistAsync(string AdmissionNo)
        {
            bool isExist = (await db.Std_AdmissionDetails.Where(u => u.AdmissionNo == AdmissionNo).ToListAsync()).IsValidList();
            return isExist;
        }

        public async Task<bool> IsRegisterNoExistAsync(string RegisterNo)
        {
            var isExist = (await db.Std_Register.Where(u => u.RegisterNo == RegisterNo).ToListAsync()).IsValidList();
            return isExist;
        }

        public async Task<bool> IsRegNoMoreTime(Guid _StudentId)
        {
            var query =await db.Std_Register.Where(u => u.StudentId == _StudentId).CountAsync();

            if (query>1)
            {
                return true;//for more register no, this student 
            }
            return false;
        } 
        #endregion

    }
}
