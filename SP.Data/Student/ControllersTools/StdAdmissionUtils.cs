﻿using SP.Data.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP.Data.Student
{
    public class StdAdmissionUtils
    {
        private SP_DataEntities db = new SP_DataEntities();


        #region <----- Insert All Students Data ------>
        public async  Task<bool> SaveStudentAsync(Std_Details std_Details,
                              Std_Address std_Address,
                              Std_ParentsDetails std_ParentsDetails,
                              Std_Register std_Register,
                              Std_Health std_Health,
                              Std_AdmissionDetails std_AdmissionDetails,
                              Std_MoreDetails std_MoreDetails,
                              Std_BankDetails std_BankDetails)

        {

            using (db = new SP_DataEntities())
            {
                db.Database.Log = Console.Write;

                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Std_Details
                        db.Std_Details.Add(std_Details);
                      await  db.SaveChangesAsync();

                        //Std_ParentsDetails
                        db.Std_ParentsDetails.Add(std_ParentsDetails);
                        await db.SaveChangesAsync();

                        //Std_Address
                        db.Std_Address.Add(std_Address);
                        await db.SaveChangesAsync();

                        //Std_MoreDetails
                        db.Std_MoreDetails.Add(std_MoreDetails);
                        await db.SaveChangesAsync();

                        //Std_Health
                        db.Std_Health.Add(std_Health);
                        await db.SaveChangesAsync();

                        //std_BankDetails
                        db.Std_BankDetails.Add(std_BankDetails);
                        await db.SaveChangesAsync();

                        //Std_AdmissionDetails
                        db.Std_AdmissionDetails.Add(std_AdmissionDetails);
                        await db.SaveChangesAsync();

                        //Std_Register
                        db.Std_Register.Add(std_Register);
                        await db.SaveChangesAsync();



                        //Commit
                        transaction.Commit();

                        return true;
                    }
                    catch (DbEntityValidationException e)
                    {
                        var newException = new FormattedDbEntityValidationException(e);
                        throw newException;
                    }
                }

               // return false;
            }
        }
        #endregion

        public async Task<int> DeleteStd_DetailsAsync(Guid id)
        {
            Std_Details std_Details = await db.Std_Details.FindAsync(id);
            db.Std_Details.Remove(std_Details);
          return  await db.SaveChangesAsync();
        }
        public async Task<int> DeleteStd_RegisterAsync(string  RegisterNo)
        {
            Std_Register std_Register = await db.Std_Register.FindAsync(RegisterNo);
            db.Std_Register.Remove(std_Register);
            return await db.SaveChangesAsync();
        }

        [Obsolete]
        public async Task<bool> IsValidStudentAsync(string sName, DateTime sDOB, string sFatherName)
        {
            var std_ParentsDetails = db.Std_ParentsDetails.Include(s => s.Std_Details);
            if (std_ParentsDetails.IsValidIEnumerable())
            {
                var sData = await (from std in std_ParentsDetails
                            where std.Std_Details.StudentName == sName && std.FatherName == sFatherName
                            && EntityFunctions.TruncateTime(std.Std_Details.DOB) == sDOB.Date
                            select std).ToListAsync();
                if (sData.IsValidList())
                {
                    return false;
                }
            }
            return true;
        }
    }
}
