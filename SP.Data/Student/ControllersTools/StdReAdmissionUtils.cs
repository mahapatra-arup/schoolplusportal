﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP.Data.Student.ControllersTools
{
   public class StdReAdmissionUtils
    {
        private SP_DataEntities db = new SP_DataEntities();

        public async Task<int> ReAdmissioneStudentAsync(IEnumerable<Std_Register> std_Register )
        {
            using (db = new SP_DataEntities())
            {
                db.Database.Log = Console.Write;

                //Check for NULL.
                if (std_Register == null)
                {
                    std_Register = new List<Std_Register>();
                }

                //Loop and insert records.
                foreach (Std_Register stdreg in std_Register)
                {
                    db.Std_Register.Add(stdreg);
                }
                var insertedRecords =await db.SaveChangesAsync();
                return insertedRecords;
            }
        }
    }
}
