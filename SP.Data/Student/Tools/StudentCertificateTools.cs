﻿using SP.Data.Enums;
using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP.Data.Student.Tools
{
    public class StudentCertificateTools
    {
        private SP_DataEntities db = new SP_DataEntities();

        #region CertificateTemplate
        public async Task<Std_CertificateTemplate> GetCertificateTemplateAsync(StudentsEnum.StudentCertificates studentCertificates)
        {
            var studentCertificatesstr = studentCertificates.GetDisplayName();
            return await db.Std_CertificateTemplate.Where(s => s.CertificateName == studentCertificatesstr).Where(p => p.Active == true).FirstOrDefaultAsync();
        }
        #endregion

        #region CertificateIssue
        public async Task<List<Std_CertificateIssue>> GetCertificateIssueAsync(StudentsEnum.StudentCertificates studentCertificates)
        {
            var studentCertificatesstr = studentCertificates.GetDisplayName();
            return await db.Std_CertificateIssue.Where(s => s.Certificate == studentCertificatesstr).ToListAsync();
        }

        public async Task<long> GetCertificateIssueMaxNoAsync(StudentsEnum.StudentCertificates studentCertificates)
        {
            var studentCertificatesstr = studentCertificates.GetDisplayName();
            var a = await (from sci in db.Std_CertificateIssue
                           where sci.Certificate == studentCertificatesstr
                           orderby sci.No descending
                          select sci.No).FirstOrDefaultAsync();
            var result = a.ISValidObject() ? a : 0;
            return result;
        }
        #endregion
    }
}
