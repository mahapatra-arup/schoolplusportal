﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SP.Data.Models;

namespace SP.Data.session
{
    public class SessionUtils
    {
       
        private static SP_DataEntities db = new SP_DataEntities();

        #region ===Defult Activate Session Field V====
        //I TO X(Defult)
        public string _gSessionVToX { get; set; }
        public int _gSessionIdVToX { get; set; }
        public int _gSlNoVToX { get; set; } 

        //XI and XII(DefultHS)
        public string _gSessionXIToXII { get; set; }
        public int _gSessionIdXIToXII { get; set; }
        public int _gSlNoXIToXII { get; set; }

        #endregion

        #region Cons
        public SessionUtils()
        {
            db = new SP_DataEntities();
            _gSessionVToX = (db.Sessions.Where(s => s.Defult == true).FirstOrDefault().VToX);
            _gSessionIdVToX = (db.Sessions.Where(s => s.Defult == true).FirstOrDefault().ID);
            _gSlNoVToX = (db.Sessions.Where(s => s.Defult == true).FirstOrDefault().SlNo).ConvertObjectToInt();

            //XI and XII(DefultHS)
            _gSessionXIToXII = (db.Sessions.Where(s => s.DefultHS == true).FirstOrDefault().XIToXII);
            _gSessionIdXIToXII = (db.Sessions.Where(s => s.DefultHS == true).FirstOrDefault().ID);
            _gSlNoXIToXII = (db.Sessions.Where(s => s.DefultHS == true).FirstOrDefault().SlNo).ConvertObjectToInt();

            // Close Connection
            //db.Dispose();
        }

        #endregion

        #region Method

        public async Task<Session> GetDefaultSessionAsync()
        {
            var s = db.Sessions;

            Session defaultSession = new Session();
            if (s.ISValidObject())
            {
                var session =await (from sess in s
                              where sess.Defult == true && sess.DefultHS == true
                              select sess).ToListAsync();
                defaultSession = session.ISValidObject() ? session.FirstOrDefault() : new Session();

            }
            return defaultSession;
        }

        public async Task<IEnumerable<Session>> GetNexSessionsAsync()
        {
            var s = db.Sessions;

            List<Session> defaultSession = new List<Session>();
            if (s.IsValidIEnumerable())
            {
                var session = from sess in s
                              select sess;
                defaultSession =await session.ToListAsync();

            }
            return defaultSession;
        }

        #endregion
    }
}
