﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SP.Data.Models;

namespace SP.Data.session
{
    public class SchoolUtils
    {
        private static SP_DataEntities db ;

        #region Cons
        public SchoolUtils()
        {
            db = new SP_DataEntities();
        }

        #endregion

        #region Method

        public async Task<SchoolProfile> GetSchoolProfileAsync()
        {
            return await db.SchoolProfiles.FirstOrDefaultAsync();
        }
      
        #endregion
    }
}
