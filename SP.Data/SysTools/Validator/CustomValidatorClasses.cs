﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace SP.Data.SysTools.Validator
{
    
    public class Custom_Validator_Date : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                if (value != null)
                {
                    DateTime date = (DateTime)value;

                    if (date.Date < DateTime.Now.Date)
                    {
                        return ValidationResult.Success;
                    }
                    else
                    {
                        return new ValidationResult("Please Enter a valid date.");
                    }
                }
                else
                {
                    return new ValidationResult("" + validationContext.DisplayName + " is required");
                }
            }
            catch (Exception)
            {
                return new ValidationResult("" + validationContext.DisplayName + " is Invalid");
            }
        }
    }
    public class Custom_Validator_IFSC_Length : ValidationAttribute
    {
        protected override ValidationResult IsValid(object IFSCCode, ValidationContext validationContext)
        {
            try
            {
                if (IFSCCode != null)
                {
                    string ifsccode = IFSCCode.ConvertObjectToString();

                    if (ifsccode.Length==11)
                    {
                        return ValidationResult.Success;
                    }
                    else
                    {
                        return new ValidationResult("Please Enter a valid IFSC Code.");
                    }
                }
                else
                {
                    return new ValidationResult("" + validationContext.DisplayName + " is required");
                }
            }
            catch (Exception)
            {
                return new ValidationResult("" + validationContext.DisplayName + " is Invalid!!");
            }
        }
    }
   
}
