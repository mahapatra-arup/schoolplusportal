﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SP.Data
{
    public class WebTools
    {
        public static string GetPreviousLink()
         {
                string referencepage = "javascript:;";
                try
                {
                    referencepage = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                }
                catch (Exception)
                {

                    referencepage = "javascript:;";
                }
            return referencepage;
            }
    }
}