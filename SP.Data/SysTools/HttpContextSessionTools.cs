﻿using System;
using System.Web;

namespace SP.Data
{
    public static class HttpContextSessionTools
    {

        public static void CreateHttpContextSession<T>(string _SessionName, T _deataObject)
        {
            if (GetHttpContextSessionObject<T>(_SessionName) != null)
            {
                RemoveHttpContextSession(_SessionName);
            }
            HttpContext.Current.Session.Add(_SessionName, _deataObject);
        }

        public static void RemoveHttpContextSession(string _SessionName)
        {
            HttpContext.Current.Session.Remove(_SessionName);
        }

        /// <summary>
        /// Get Session level Storage Object Data
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_SessionName"></param>
        /// <returns></returns>
        public static T GetHttpContextSessionObject<T>(string _SessionName)
        {

            var data = HttpContext.Current != null ? HttpContext.Current.Session[_SessionName] : null;
            if (data.ISValidObject())
            {
                return (T)Convert.ChangeType(data, typeof(T));
            }
            return default(T);
        }
        /// <summary>
        ///  Get Session level Storage Object Data
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="current"></param>
        /// <param name="_SessionName"></param>
        /// <returns></returns>
        public static T GetHttpContextSessionObject<T>(this HttpContext current, string _SessionName)
        {
            var data = current != null ? current.Session[_SessionName] : null;
            if (data.ISValidObject())
            {
                return (T)Convert.ChangeType(data, typeof(T));
            }
            return default(T);
        }
    }
}
