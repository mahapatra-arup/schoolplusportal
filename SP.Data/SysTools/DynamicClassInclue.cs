﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace  SP.Data
{
    public static class DynamicClassInclue
    {
        //calling process
        //protected void Page_Load(object sender, EventArgs e)

        //{
        //    this.IncludeJavascript("MyJs.js");
        //    this.IncludeStylesheet("MyCss.css");
        //}
        public static void IncludeJavascript(this Page objPage, string strJsPath)

        {

            HtmlGenericControl objJs = new HtmlGenericControl();

            objJs.TagName = "script";

            objJs.Attributes.Add("type", "text/javascript");

            objJs.Attributes.Add("language", "javascript");

            objJs.Attributes.Add("src", objPage.ResolveClientUrl("~/Javascript/" +
            strJsPath));

            objPage.Header.Controls.Add(objJs);

        }

        public static void IncludeStylesheet(this Page objPage, string strCssPath)

        {

            HtmlGenericControl objCss = new HtmlGenericControl();

            objCss.TagName = "link";

            objCss.Attributes.Add("type", "text/css");

            objCss.Attributes.Add("rel", "stylesheet");

            objCss.Attributes.Add("href", objPage.ResolveClientUrl("~/CSS/" + strCssPath));

            objPage.Header.Controls.Add(objCss);

        }
    }
}