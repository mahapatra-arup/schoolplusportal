﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using SP.Data;

namespace SP.Data
{
    public static class HttpPostedFileBaseExtensions
    {

        public static bool IsImage(this HttpPostedFileBase postedFile, int ImageMaxiMumBytes,out string _messege)
        {
            _messege = "";
            if (postedFile.ISValidObject())
            {

                //-------------------------------------------
                //  Check the image mime types
                //-------------------------------------------
                if (!string.Equals(postedFile.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
            !string.Equals(postedFile.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
            !string.Equals(postedFile.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
            !string.Equals(postedFile.ContentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
            !string.Equals(postedFile.ContentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
            !string.Equals(postedFile.ContentType, "image/png", StringComparison.OrdinalIgnoreCase))
                {
                    _messege = postedFile.FileName + " is invalid formate";
                    return false;
                }
                //-------------------------------------------
                //  Check the image extension
                //-------------------------------------------
                var postedFileExtension = Path.GetExtension(postedFile.FileName);
                if (!string.Equals(postedFileExtension, ".jpg", StringComparison.OrdinalIgnoreCase)
            && !string.Equals(postedFileExtension, ".png", StringComparison.OrdinalIgnoreCase)
            && !string.Equals(postedFileExtension, ".gif", StringComparison.OrdinalIgnoreCase)
            && !string.Equals(postedFileExtension, ".jpeg", StringComparison.OrdinalIgnoreCase))
                {
                    _messege = postedFile.FileName + " is invalid formate";
                    return false;
                }


                //-------------------------------------------
                //  Attempt to read the file and check the first bytes
                //-------------------------------------------
                try
                {
                    if (!postedFile.InputStream.CanRead)
                    {
                        _messege = "Invalid : Can not Read the Image !";
                        return false;
                    }


                    //------------------------------------------
                    //check whether the image size exceeding the limit or not
                    //------------------------------------------ 
                    if (postedFile.ContentLength > ImageMaxiMumBytes)
                    {
                        _messege = postedFile.FileName +" is too large, Maximum file Size is !"+ ImageMaxiMumBytes.ConvertObjectToDouble().ConvertBytesToKilobyte()+" kb";
                        return false;
                    }

                    byte[] buffer = new byte[ImageMaxiMumBytes];
                    postedFile.InputStream.Read(buffer, 0, ImageMaxiMumBytes);
                    string content = System.Text.Encoding.UTF8.GetString(buffer);
                    if (Regex.IsMatch(content, @"<script|<html|<head|<title|<body|<pre|<table|<a\s+href|<img|<plaintext|<cross\-domain\-policy",
                        RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Multiline))
                    {
                        _messege = "Invalid : Can not Read the Image !";
                        return false;
                    }
                }
                catch (Exception)
                {
                    _messege = "Invalid : Can not Read the Image !";
                    return false;
                }
                //-------------------------------------------
                //  Try to instantiate new Bitmap, if .NET will throw exception
                //  we can assume that it's not a valid image
                //-------------------------------------------

                try
                {
                    using (var bitmap = new System.Drawing.Bitmap(postedFile.InputStream))
                    {
                    }
                }
                catch (Exception)
                {
                    _messege = "Invalid Image Input Stream !";
                    return false;
                }
                finally
                {
                    postedFile.InputStream.Position = 0;
                }

                return true;
            }
            else
            {
                _messege = "Invalid Image !";
                return false;
            }
           
        }

    }
}