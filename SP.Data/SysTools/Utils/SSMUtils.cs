﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Globalization;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace SP.Data
{
    public static class SSMUtils
    {

        public static string ReturnExtension(string fileExtension)
        {
            switch (fileExtension)
            {
                case ".htm":
                case ".html":
                case ".log":
                    return "text/HTML";
                case ".txt":
                    return "text/plain";
                case ".doc":
                    return "application/ms-word";
                case ".tiff":
                case ".tif":
                    return "image/tiff";
                case ".asf":
                    return "video/x-ms-asf";
                case ".avi":
                    return "video/avi";
                case ".zip":
                    return "application/zip";
                case ".xls":
                case ".csv":
                    return "application/vnd.ms-excel";
                case ".gif":
                    return "image/gif";
                case ".jpg":
                case "jpeg":
                    return "image/jpeg";
                case ".bmp":
                    return "image/bmp";
                case ".wav":
                    return "audio/wav";
                case ".mp3":
                    return "audio/mpeg3";
                case ".mpg":
                case "mpeg":
                    return "video/mpeg";
                case ".rtf":
                    return "application/rtf";
                case ".asp":
                    return "text/asp";
                case ".pdf":
                    return "application/pdf";
                case ".fdf":
                    return "application/vnd.fdf";
                case ".ppt":
                    return "application/mspowerpoint";
                case ".dwg":
                    return "image/vnd.dwg";
                case ".msg":
                    return "application/msoutlook";
                case ".xml":
                case ".sdxl":
                    return "application/xml";
                case ".xdp":
                    return "application/vnd.adobe.xdp+xml";
                default:
                    return "application/octet-stream";
            }
        }

        public static string SetCurrentDateInTextBox(this TextBox dtpBox)
        {
            try
            {
                //Current Date Set
                DateTime dttime = DateTime.Now.Date;
                dtpBox.Text = dttime.Date.ToString("yyyy-MM-dd");
            }
            catch (Exception)
            {

            }
            return string.Empty;

        }

        public static bool isValidTable(DataTable dtInvoiceNum)
        {
            try
            {
                if (dtInvoiceNum != null && dtInvoiceNum.Rows.Count > 0)
                {
                    return true;
                }
            }
            catch (Exception)
            {
            }
            return false;
        }

        public static bool isNimeric(string txt, char c)
        {
            if (c == '\b')
            {
                return true;
            }
            else if (char.IsWhiteSpace(c))
            {
                return false;
            }
            else
            {
                try
                {
                    double dbl = Double.Parse(txt);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }

        }

        public static List<string> GetListFromDataTbale(DataTable dataTable)
        {

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                List<string> lst = new List<string>();
                foreach (DataRow dr in dataTable.Rows)
                {
                    string val = dr[0].ToString();
                    lst.Add(val);
                }
                return lst;
            }
            return null;
        }

        public static int GetIdByValue(Dictionary<int, string> dic, string value)
        {
            int key = 0;
            foreach (var entry in dic)
            {
                if (entry.Value.Equals(value))
                {
                    key = entry.Key;
                }

            }
            return key;

        }

    }

    public static class StringExtenssion
    {

        public static bool ISNullOrWhiteSpace(this string strVal)
        {

            if (strVal == null || string.IsNullOrEmpty(strVal.Trim()))
            {
                return true;
            }
            return false;
        }
    }

    public static class DataTableExtension
    {
        public static bool IsValidDataTable(this DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        public static bool IsValidList<T>(this List<T> lst)
        {
            if (lst != null && lst.Count > 0)
            {
                return true;
            }
            return false;
        }
        public static bool ISValidObject(this object o)
        {
            if (o != null && o != DBNull.Value)
            {
                if (!o.ToString().ISNullOrWhiteSpace())
                {
                    return true;
                }
            }

            return false;
        }
    }

    public static class DictionaryExtention
    {
        public static bool IsNullOrEmpty<T, U>(this IDictionary<T, U> Dictionary)
        {
            return (Dictionary == null || Dictionary.Count < 1);
        }
    }




    /// <summary>
    /// =-===Convert Method=====
    /// </summary>
    public static class ConvertExtension
    {
        public static object ConvertNullEmptyToZero(this string value)
        {
            object obj = value.ISNullOrWhiteSpace() ? "0" : value;
            return obj;
        }
        public static object ConvertDBNullTONull(this object value)
        {
            object obj = value.ISValidObject() ? value : null;
            return obj;
        }

        public static object ConvertNullToDBNull(this object value)
        {
            object obj = value.ISValidObject() ? value : DBNull.Value;
            return obj;
        }
        public static string ConvertObjectToString(this object value)
        {
            string obj = string.Empty;
            if (value.ISValidObject())
            {
                obj = value.ToString();
            }
            return obj;
        }
        public static double ConvertObjectToDouble(this object value)
        {
            double obj = 0;
            double.TryParse(value.ConvertObjectToString(), out obj);
            return obj;
        }

        public static Guid? ConvertObjectToGuid(this object value)
        {
            Guid? obj = null;
            try
            {
                obj = Guid.Parse(value.ConvertObjectToString());
            }
            catch (Exception)
            {
                obj = null;
            }
            return obj;
        }

        public static Guid ConvertToGuid(this object value)
        {
            Guid obj = new Guid();
            try
            {
                obj = Guid.Parse(value.ConvertObjectToString());
            }
            catch (Exception )
            {
              return  default(Guid);
               // throw e;
            }
            return obj;
        }
        public static int ConvertObjectToInt(this object value)
        {
            int obj = 0;
            int.TryParse(value.ConvertObjectToString(), out obj);
            return obj;
        }
        public static long ConvertObjectToLong(this object value)
        {
            long obj = 0;
            long.TryParse(value.ConvertObjectToString(), out obj);
            return obj;
        }
        /// <summary>
        ///  object is null then  datetime is MinValue
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime ConvertObjectDateTimeToDateTime(this object datetime)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            DateTime _datetime = DateTime.Now;
            try
            {
                _datetime = DateTime.ParseExact(datetime.ConvertObjectToString().Trim(), new string[] { "MM.dd.yyyy", "MM-dd-yyyy", "dd-MMM-yyyy", "MM/dd/yyyy", "MM/dd/yyyy hh:mm:ss tt", "MM/dd/yyyy hh:mm:ss tt" }, provider, DateTimeStyles.None);
            }
            catch (Exception)
            {
            }
            return _datetime;
        }
        /// <summary>
        /// Convert Object To DateTime
        /// return DateTime/null
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime? ConvertObjectToDateTimeWithNull(this object value)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            DateTime? obj = null;
            if (value.ISValidObject())
            {
                try
                {
                    obj = DateTime.ParseExact(value.ConvertObjectToString().Trim(), new string[] { "MM.dd.yyyy", "MM-dd-yyyy", "dd-MMM-yyyy", "MM/dd/yyyy" }, provider, DateTimeStyles.None);
                }
                catch (Exception)
                {
                    obj = null;
                }
            }
            return obj;
        }
        /// <summary>
        ///  object is null then  bool is false
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ConvertObjectToBool(this object value)
        {
            bool obj = false;
            bool.TryParse(value.ConvertObjectToString(), out obj);
            return obj;
        }

        /// <summary>
        /// Convert To Datatable From List 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        public static void ToDataTable<T>(this DataTable dataTable, List<T> items)
        {
            dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
        }

      
    }

    public static class EnumExtension
    {
       
        public static string ConvertEnumFieldToString<T>(this T enumval)
        {
            string strAttribute = string.Empty;
            if (enumval.IsEnumValue())
            {
                strAttribute = Enum.GetName(typeof(T), enumval);
            }
            else
            {
                throw new InvalidCastException("The specified object is not an enum.");
            }
            return strAttribute;
        }

        public static T ConvertStringToEnumField<T>(this string enumFieldstr)
        {
            T EnumrAttribute ;
            if (!enumFieldstr.ISNullOrWhiteSpace())
            {
                try
                {
                    EnumrAttribute=(T)Enum.Parse(typeof(T), enumFieldstr);
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                throw new InvalidCastException("Invalid Enum Field String !!");
            }
            return EnumrAttribute;
        }

        /// <summary>
        /// Get Enum Field From DisplayName
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public static T GetEnumFieldFromDisplayName<T>(this string DisplayName)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();

            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(DisplayAttribute)) as DisplayAttribute;
                if (attribute != null)
                {
                    if (attribute.Name == DisplayName)
                    {
                        return (T)field.GetValue(null);
                    }
                }
                else
                {
                    if (field.Name == DisplayName)
                        return (T)field.GetValue(null);
                }
            }

            throw new ArgumentOutOfRangeException("DisplayName");
        }
        /// <summary>
        /// Get Enum Display Name From EnumField
        /// like [Display(Name="Get This")]
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static string GetDisplayName(this Enum enumValue)
        {
            return enumValue
                .GetType()
                .GetMember(enumValue.ToString())
                .First()
                .GetCustomAttribute<DisplayAttribute>()
                .GetName();
        }
        /// <summary>
        /// Value Is Enum Type Or Not
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static bool IsEnumValue<T>(this T enumValue)
        {
            if (typeof(T).BaseType != typeof(Enum))
            {
                return false;
                // throw new InvalidCastException("The specified object is not an enum.");
            }
            return true;
        }

        #region SelectList
        /// <summary>
        /// Here Value={Enum Field_Name} and Text={Enum Field_Name}
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="enumObj"></param>
        /// <param name="SelectedValue"></param>
        /// <returns></returns>
        public static SelectList ConvertTo_SelectList<TEnum>(this TEnum enumObj, string SelectedValue = "")
            where TEnum : struct, IComparable, IFormattable, IConvertible
        {
            var values = from TEnum e in Enum.GetValues(typeof(TEnum))
                         select new { Id = e, Name = e.ToString() };
            return new SelectList(values, "Id", "Name", SelectedValue);
        }
       
        /// <summary>
        /// Here Value={Enum Field_Name} and Text={DisplayName}
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="obj"></param>
        /// <param name="SelectedValue"></param>
        /// <returns></returns>
        public static SelectList ConvertToSelectList<TEnum>(this TEnum obj, string SelectedValue = "") where TEnum : struct, IComparable, IFormattable, IConvertible // correct one    
        {
            return new SelectList(Enum.GetValues(typeof(TEnum))
            .OfType<Enum>()
            .Select(x => new SelectListItem
            {
                Text = x.GetDisplayName(),//Display Name
            Value = x.ToString() //FieldName
        }), "Value", "Text");
        } 
        #endregion
    }


        public static class EnumerableExtension
    {
        public static bool IsValidIEnumerable<T>(this IEnumerable<T> lst)
        {
            if (lst != null && lst.Count() > 0)
            {
                return true;
            }
            return false;
        }
    }

    public static class FieldPropertiesExtension
    {
        /// <summary>
        ///  // fieldName has string value of `Field`
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="memberAccess"></param>
        /// <returns></returns>
        public static string GetMemberName<T, TValue>(Expression<Func<T, TValue>> memberAccess)
        {
            //Use :var fieldName = GetMemberName((MyClass c) => c.Field);
            return ((MemberExpression)memberAccess.Body).Member.Name;
        }

        public static object[] ConvertPropertyToArray(this object o)
        {
            return o.GetType().GetProperties()
                .Select(p => p.GetValue(o, null))
                .ToArray();
        }
    }

    public static class MethodPropertiesExtension
    {
        //public static string GetMemberName<T, TValue>(Expression<Func<T, TValue>> memberAccess)
        //{
        //    return ((MethodCallExpression)memberAccess.Body).Method.Name;
        //}
    }
}
