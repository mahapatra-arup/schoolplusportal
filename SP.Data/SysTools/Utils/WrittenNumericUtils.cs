﻿using System;

namespace SP.Data.SysTools
{
   public class WrittenNumericUtils
    {
          readonly string[] onesOrdinal = new string[] { "", "First", "Second", "Third", "Fourth", "Fifth",
            "Sixth", "Seventh", "Eighth", "Ninth" };
          readonly string[] teensOrdinals = new string[] { "Tenth", "Eleventh", "Twelfth", "Thirteenth",
            "Fourteenth", "Fifteenth", "Sixteenth", "Seventeenth", "Eighteenth", "Nineteenth" };
          readonly string[] ones = new string[] { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
          readonly string[] teens = new string[] { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
          readonly string[] tens = new string[] { "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
          readonly string[] thousandsGroups = { "", " Thousand", " Million", " Billion" };

        private   string FriendlyInteger(int n, string leftDigits, int thousands)
        {
            if (n == 0)
                return leftDigits;

            string friendlyInt = leftDigits;
            if (friendlyInt.Length > 0)
                friendlyInt += " ";

            if (n < 10)
                friendlyInt += ones[n];
            else if (n < 20)
                friendlyInt += teens[n - 10];
            else if (n < 100)
                friendlyInt += FriendlyInteger(n % 10, tens[n / 10 - 2], 0);
            else if (n < 1000)
                friendlyInt += FriendlyInteger(n % 100, (ones[n / 100] + " Hundred"), 0);
            else
                friendlyInt += FriendlyInteger(n % 1000, FriendlyInteger(n / 1000, "", thousands + 1), 0);

            return friendlyInt + thousandsGroups[thousands];
        }

        public   string DateToWritten(DateTime date)
        {
            return string.Format("{0} {1} {2}", IntegerToOrdinal(date.Day), date.ToString("MMMM"), IntegerToWritten(date.Year));
        }

        public   string IntegerToWritten(int n)
        {
            if (n == 0)
                return "Zero";
            else if (n < 0)
                return "Negative " + IntegerToWritten(-n);

            return FriendlyInteger(n, "", 0);
        }

        public   string IntegerToOrdinal(int n)
        {
            switch (n)
            {
                case 1:
                    return "First";
                case 2:
                    return "Second";
                case 3:
                    return "Third";
                case 4:
                    return "Fourth";
                case 5:
                    return "Fifth";
                case 6:
                    return "Sixth";
                case 7:
                    return "Seventh";
                case 8:
                    return "Eighth";
                case 9:
                    return "Ninth";
                case 10:
                    return "Tenth";
                case 11:
                    return "Eleventh";
                case 12:
                    return "Twelfth";
                case 13:
                    return "Thirteenth";
                case 14:
                    return "Fourteenth";
                case 15:
                    return "Fifteenth";
                case 16:
                    return "Sixteenth";
                case 17:
                    return "Seventeenth";
                case 18:
                    return "Eighteenth";
                case 19:
                    return "Nineteenth";
                case 20:
                    return "Twentieth";
                case 21:
                    return "Twenty First";
                case 22:
                    return "Twenty Second";
                case 23:
                    return "Twenty Third";
                case 24:
                    return "Twenty Fourth";
                case 25:
                    return "Twenty Fifth";
                case 26:
                    return "Twenty Sixth";
                case 27:
                    return "Twenty Seventh";
                case 28:
                    return "Twenty Eighth";
                case 29:
                    return "Twenty Ninth";
                case 30:
                    return "Thirtieth";
                case 31:
                    return "Thirty First";
            }
            return null;
        }

        public   string NumbersToWords(int inputNumber)
        {
            int inputNo = inputNumber;

            if (inputNo == 0)
                return "Zero";

            int[] numbers = new int[4];
            int first = 0;
            int u, h, t;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (inputNo < 0)
            {
                sb.Append("Minus ");
                inputNo = -inputNo;
            }

            string[] words0 = {"" ,"One ", "Two ", "Three ", "Four ",
            "Five " ,"Six ", "Seven ", "Eight ", "Nine "};
            string[] words1 = {"Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ",
            "Fifteen ","Sixteen ","Seventeen ","Eighteen ", "Nineteen "};
            string[] words2 = {"Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ",
            "Seventy ","Eighty ", "Ninety "};
            string[] words3 = { "Thousand ", "Lakh ", "Crore " };

            numbers[0] = inputNo % 1000; // units
            numbers[1] = inputNo / 1000;
            numbers[2] = inputNo / 100000;
            numbers[1] = numbers[1] - 100 * numbers[2]; // thousands
            numbers[3] = inputNo / 10000000; // crores
            numbers[2] = numbers[2] - 100 * numbers[3]; // lakhs

            for (int i = 3; i > 0; i--)
            {
                if (numbers[i] != 0)
                {
                    first = i;
                    break;
                }
            }
            for (int i = first; i >= 0; i--)
            {
                if (numbers[i] == 0) continue;
                u = numbers[i] % 10; // ones
                t = numbers[i] / 10;
                h = numbers[i] / 100; // hundreds
                t = t - 10 * h; // tens
                if (h > 0) sb.Append(words0[h] + "Hundred ");
                if (u > 0 || t > 0)
                {
                    if (h > 0 || i == 0) sb.Append("and ");
                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);
                }
                if (i != 0) sb.Append(words3[i - 1]);
            }
            return sb.ToString().TrimEnd() + " Only.";
        }

        public   string MarksToWords(int inputNumber)
        {
            int inputNo = inputNumber;

            if (inputNumber == 0)
                return "Zero";

            #region <<<<<<<<<<<<<< Convert Integer Number >>>>>>>>>>>>>>>
            int[] numbers = new int[4];
            int first = 0;
            int u, h, t;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (inputNo < 0)
            {
                sb.Append("Minus ");
                inputNo = -inputNo;
            }

            string[] words0 = {"" ,"One ", "Two ", "Three ", "Four ",
            "Five " ,"Six ", "Seven ", "Eight ", "Nine "};
            string[] words1 = {"Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ",
            "Fifteen ","Sixteen ","Seventeen ","Eighteen ", "Nineteen "};
            string[] words2 = {"Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ",
            "Seventy ","Eighty ", "Ninety "};
            string[] words3 = { "Thousand ", "Lakh ", "Crore " };

            numbers[0] = inputNo % 1000; // units
            numbers[1] = inputNo / 1000;
            numbers[2] = inputNo / 100000;
            numbers[1] = numbers[1] - 100 * numbers[2]; // thousands
            numbers[3] = inputNo / 10000000; // crores
            numbers[2] = numbers[2] - 100 * numbers[3]; // lakhs

            for (int i = 3; i > 0; i--)
            {
                if (numbers[i] != 0)
                {
                    first = i;
                    break;
                }
            }
            for (int i = first; i >= 0; i--)
            {
                if (numbers[i] == 0) continue;
                u = numbers[i] % 10; // ones
                t = numbers[i] / 10;
                h = numbers[i] / 100; // hundreds
                t = t - 10 * h; // tens
                if (h > 0) sb.Append(words0[h] + "Hundred ");
                if (u > 0 || t > 0)
                {
                    // if (h > 0 || i == 0) sb.Append("and ");
                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);
                }
                if (i != 0) sb.Append(words3[i - 1]);
            }
            #endregion


            return sb.ToString().TrimEnd();
        }


        public   string MaxDayOfTheMonth(string monthName)
        {
            switch (monthName)
            {
                case "January":
                    return "31";
                case "February":
                    return "28";
                case "March":
                    return "31";
                case "April":
                    return "30";
                case "May":
                    return "31";
                case "June":
                    return "30";
                case "July":
                    return "31";
                case "August":
                    return "31";
                case "September":
                    return "30";
                case "October":
                    return "31";
                case "November":
                    return "30";
                case "December":
                    return "31";
            }
            return null;
        }

        public   int MonthNoByMonthName(string monthName)
        {
            switch (monthName)
            {
                case "January":
                    return 1;
                case "February":
                    return 2;
                case "March":
                    return 3;
                case "April":
                    return 4;
                case "May":
                    return 5;
                case "June":
                    return 6;
                case "July":
                    return 7;
                case "August":
                    return 8;
                case "September":
                    return 9;
                case "October":
                    return 10;
                case "November":
                    return 11;
                case "December":
                    return 12;
            }
            return 0;
        }

        public   string NumericToWord(string numeric)
        {
            switch (numeric)
            {
                case "I":
                    return "ONE";
                case "II":
                    return "TWO";
                case "III":
                    return "THREE";
                case "IV":
                    return "FOUR";
                case "V":
                    return "FIVE";
                case "VI":
                    return "SIX";
                case "VII":
                    return "SEVEN";
                case "VIII":
                    return "EIGHT";
                case "IX":
                    return "NINE";
                case "X":
                    return "TEN";
                case "XI":
                    return "ELEVEN";
                case "XII":
                    return "TWELVE";
            }
            return null;
        }
    }
}
