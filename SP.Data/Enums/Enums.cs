﻿using System.ComponentModel.DataAnnotations;

namespace SP.Data.Enums
{

    public class StudentsEnum
    {
        public enum _AdmissionType
        {
            Admission,
            Readmission
        }

        public enum StudentCertificates
        {
            [Display(Name = "Charecter Certificate")]
            Charecter_Certificate,

            [Display(Name = "Transfer Certificate")]
            Transfer_Certificate,

            [Display(Name = "TransferCertificateDuplicate")]
            Transfer_Certificate_Duplicate,

            [Display(Name = "MPLeavingCertificate")]
            MPLeavingCertificate,

            [Display(Name = "HSLeavingCertificate")]
            HSLeavingCertificate,

            [Display(Name = "SchoolCertificate")]
            SchoolCertificate,

            [Display(Name = "CasteCertificate")]
            CasteCertificate
        }
    }
    public class StudentsFeesEnum
    {
        public enum CustomeAndFineFees { WithCustom_OR_IsFine, WithCustom_And_IsFine, WithOutCustomAndIsFine }
        public enum _StudentFeesListEnum { DUE_LIST, PAID_LIST, ALL }
    }
    public class SessionEnum
    {
        public enum _SessionType
        {
            VToX,
            XIToXII
        }
    }
}
