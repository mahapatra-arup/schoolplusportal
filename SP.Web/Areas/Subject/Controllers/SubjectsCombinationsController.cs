﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SP.Data.Models;
using SP.Web.Components.Subject;
using SP.Data.Academic.Tools;
using SP.Data.session;
using SP.Web.Components.Academic;
using SP.Data;
using SP.Data.Subject;

namespace SP.Web.Areas.Subject.Controllers
{
    public class SubjectsCombinationsController : Controller
    {
        #region obj
        private SP_DataEntities db = new SP_DataEntities();
        SubjectGroupComponent subjectGroupComponent = new SubjectGroupComponent();
        SubjectComponent subjectComponent = new SubjectComponent();
        SessionUtils sessionUtils = new SessionUtils();
        ClassComponent classComponent = new ClassComponent();
        SubjectCombinationTools subjectCombinationTools = new SubjectCombinationTools();
        #endregion


        public ActionResult Index()
        {
            return View();
        }

        #region mp
        public ActionResult MpSubCombinationIndex()
        {
            return RedirectToAction("MpSubCombCreate");
        }

        /// <summary>
        /// Mp Subject Combination Create
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> MpSubCombCreate()
        {
          await  MpDropDownlistFill();
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> MpSubCombCreate([Bind(Include = "GroupId,ClassId,SubjectId,SubjectType")]MPSubjectsCombination mPSubjectsCombination, string btnAddSubCombination)
        {
            if (btnAddSubCombination != null)
            {
                if (ModelState.IsValid)
                {
                    if (await subjectCombinationTools.IsDublicateMpSubjectsForCombinationAsync(mPSubjectsCombination.ClassId, mPSubjectsCombination.GroupId, mPSubjectsCombination.SubjectId))
                    {
                        ModelState.AddModelError("", "This Subject was already Present in same Group");
                    }

                    if (ModelState.IsValid)
                    {
                        db.MPSubjectsCombinations.Add(mPSubjectsCombination);
                        var IsResult = await db.SaveChangesAsync();
                    }
                }
            }

            //Fill Grid
            ViewBag.lstMpSubjectsCombinations = await subjectCombinationTools.GetMpSubjectsCombinationsAsync(mPSubjectsCombination.ClassId, mPSubjectsCombination.GroupId);

            //Fill DropDownList
            await  MpDropDownlistFill(mPSubjectsCombination.ClassId.ConvertObjectToString(), mPSubjectsCombination.SubjectId.ConvertObjectToString(), mPSubjectsCombination.GroupId.ConvertObjectToString());


            //return
            return PartialView(mPSubjectsCombination);
        }

        private async Task  MpDropDownlistFill(string ClassId_SelectValue = "", string SubjectId_SelectValue = "", string GroupId_SelectValue = "")
        {
            ViewBag.ClassId =await classComponent.CmbMpClassAsync(sessionUtils._gSessionIdVToX, ClassId_SelectValue);
            ViewBag.GroupId =await subjectGroupComponent.CmbSubjectGroupAsync(GroupId_SelectValue);

            //Subject
            ViewBag.SubjectId =await subjectComponent.CmbMpSubjectNotInCombinationSubjectAsync(sessionUtils._gSessionIdVToX, SubjectId_SelectValue);
        }

        [HttpGet]
        public async Task DeleteMpCombinationSubject(long SubID, long? GrpId)
        {
            if (GrpId.ISValidObject())
            {

            }
            MPSubjectsCombination mPSubjectsCombination = await db.MPSubjectsCombinations.FindAsync(SubID);
            db.MPSubjectsCombinations.Remove(mPSubjectsCombination);
            await db.SaveChangesAsync();
            //return Redirect(Request.Url.AbsoluteUri);
        } 
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
