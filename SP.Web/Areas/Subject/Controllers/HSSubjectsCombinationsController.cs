﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;

using SP.Data.Models;

namespace SP.Web.Areas.Subject.Controllers
{
    public class HSSubjectsCombinationsController : Controller
    {
        private SP_DataEntities db = new SP_DataEntities();

        // GET: Subject/HSSubjectsCombinations
        public async Task<ActionResult> Index()
        {
            var hSSubjectsCombinations = db.HSSubjectsCombinations.Include(h => h.HSSubject).Include(h => h.Stream).Include(h => h.SubjectsGroup);
            return View(await hSSubjectsCombinations.ToListAsync());
        }

        // GET: Subject/HSSubjectsCombinations/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HSSubjectsCombination hSSubjectsCombination = await db.HSSubjectsCombinations.FindAsync(id);
            if (hSSubjectsCombination == null)
            {
                return HttpNotFound();
            }
            return View(hSSubjectsCombination);
        }

        // GET: Subject/HSSubjectsCombinations/Create
        public ActionResult Create()
        {
            ViewBag.SubjectId = new SelectList(db.HSSubjects, "Id", "SubjectName");
            ViewBag.StreamId = new SelectList(db.Streams, "ID", "Stream1");
            ViewBag.GroupId = new SelectList(db.SubjectsGroups, "Id", "GroupName");
            return View();
        }

        // POST: Subject/HSSubjectsCombinations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,GroupId,StreamId,SubjectId")] HSSubjectsCombination hSSubjectsCombination)
        {
            if (ModelState.IsValid)
            {
                db.HSSubjectsCombinations.Add(hSSubjectsCombination);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.SubjectId = new SelectList(db.HSSubjects, "Id", "SubjectName", hSSubjectsCombination.SubjectId);
            ViewBag.StreamId = new SelectList(db.Streams, "ID", "Stream1", hSSubjectsCombination.StreamId);
            ViewBag.GroupId = new SelectList(db.SubjectsGroups, "Id", "GroupName", hSSubjectsCombination.GroupId);
            return View(hSSubjectsCombination);
        }

        // GET: Subject/HSSubjectsCombinations/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HSSubjectsCombination hSSubjectsCombination = await db.HSSubjectsCombinations.FindAsync(id);
            if (hSSubjectsCombination == null)
            {
                return HttpNotFound();
            }
            ViewBag.SubjectId = new SelectList(db.HSSubjects, "Id", "SubjectName", hSSubjectsCombination.SubjectId);
            ViewBag.StreamId = new SelectList(db.Streams, "ID", "Stream1", hSSubjectsCombination.StreamId);
            ViewBag.GroupId = new SelectList(db.SubjectsGroups, "Id", "GroupName", hSSubjectsCombination.GroupId);
            return View(hSSubjectsCombination);
        }

        // POST: Subject/HSSubjectsCombinations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,GroupId,StreamId,SubjectId")] HSSubjectsCombination hSSubjectsCombination)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hSSubjectsCombination).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.SubjectId = new SelectList(db.HSSubjects, "Id", "SubjectName", hSSubjectsCombination.SubjectId);
            ViewBag.StreamId = new SelectList(db.Streams, "ID", "Stream1", hSSubjectsCombination.StreamId);
            ViewBag.GroupId = new SelectList(db.SubjectsGroups, "Id", "GroupName", hSSubjectsCombination.GroupId);
            return View(hSSubjectsCombination);
        }

        // GET: Subject/HSSubjectsCombinations/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HSSubjectsCombination hSSubjectsCombination = await db.HSSubjectsCombinations.FindAsync(id);
            if (hSSubjectsCombination == null)
            {
                return HttpNotFound();
            }
            return View(hSSubjectsCombination);
        }

        // POST: Subject/HSSubjectsCombinations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            HSSubjectsCombination hSSubjectsCombination = await db.HSSubjectsCombinations.FindAsync(id);
            db.HSSubjectsCombinations.Remove(hSSubjectsCombination);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
