﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SP.Data.Models;
using SP.Data;
using SP.Web.Infrastructure.ControllerStructure.Subject;
using SP.Data.session;

namespace SP.Web.Areas.Subject.Controllers
{
    public class SubjectsController : Controller
    {
        #region obj
        private SP_DataEntities db = new SP_DataEntities();
        SessionUtils sessionUtils = new SessionUtils();
        #endregion
        public ActionResult Index()
        {
            return RedirectToAction(SubjectsIStruct.MpIndex.ActionName);
        }

        #region ============Mp============
        // GET: Subject/Subjects
        public async Task<ActionResult> MpIndex()
        {
            var mPSubjects = db.MPSubjects.Include(m => m.Session);
            return View(await mPSubjects.ToListAsync());
        }


        public async Task<ActionResult> AddEditMpSubjects(long? id)
        {
            //Get
            MPSubject mPSubject = await db.MPSubjects.FindAsync(id);

            //DropDownlist
            var sessionId = mPSubject.ISValidObject() ? mPSubject.SessionId.ConvertObjectToString() : "";
            ViewBag.SessionId = new SelectList(db.Sessions, "ID", "VToX", sessionId);

            //return
            return View(SubjectsIStruct.AddEditMpSubjects.ActionName, mPSubject);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddEditMpSubjects([Bind(Include = "ID,SubjectName,SubjectCode,SubjectType,SlNo,SessionId")] MPSubject mPSubject)
        {
            if (mPSubject.ISValidObject() && mPSubject.ID > 0)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(mPSubject).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction(SubjectsIStruct.MpIndex.ActionName);
                }
            }
            else
            {
                ModelState.Remove("ID");
                ModelState.Remove("SessionId");
                if (ModelState.IsValid)
                {
                    mPSubject.SessionId = sessionUtils._gSessionIdVToX;
                    db.MPSubjects.Add(mPSubject);
                    await db.SaveChangesAsync();
                    //Return
                    return RedirectToAction(SubjectsIStruct.MpIndex.ActionName);
                }
            }

            ViewBag.SessionId = new SelectList(db.Sessions, "ID", "VToX", mPSubject.SessionId);
            return View(mPSubject);
        }


        public async Task<ActionResult> DeleteMpSubjects(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MPSubject mPSubject = await db.MPSubjects.FindAsync(id);
            if (mPSubject == null)
            {
                return HttpNotFound();
            }
            return View(mPSubject);
        }

        [HttpPost, ActionName("DeleteMpSubjects")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteMpSubConfirmed(long id)
        {
            MPSubject mPSubject = await db.MPSubjects.FindAsync(id);
            db.MPSubjects.Remove(mPSubject);
            await db.SaveChangesAsync();
            return RedirectToAction(SubjectsIStruct.MpIndex.ActionName);
        }
        #endregion

        #region =============Hs===========
        public async Task<ActionResult> HsIndex()
        {
            return View(await db.HSSubjects.ToListAsync());
        }


        public async Task<ActionResult> AddEditHsSubjects(long? id)
        {
            //Get
            HSSubject hSSubject = await db.HSSubjects.FindAsync(id);

            //Return
            return View(hSSubject);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddEditHsSubjects([Bind(Include = "Id,SubjectName,SubjectCode,IsLanguage,IsLab")] HSSubject hSSubject)
        {
            if (hSSubject.ISValidObject() && hSSubject.Id > 0)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(hSSubject).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction(SubjectsIStruct.HsIndex.ActionName);
                }
            }
            else
            {
                ModelState.Remove("Id");
                if (ModelState.IsValid)
                {
                    db.HSSubjects.Add(hSSubject);
                    await db.SaveChangesAsync();
                    return RedirectToAction(SubjectsIStruct.HsIndex.ActionName);
                }
            }
            return View(hSSubject);
        }

     
        public async Task<ActionResult> DeleteHsSubjects(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HSSubject hSSubject = await db.HSSubjects.FindAsync(id);
            if (hSSubject == null)
            {
                return HttpNotFound();
            }
            return View(hSSubject);
        }

        [HttpPost, ActionName("DeleteHsSubjects")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteHsSubConfirmed(long id)
        {
            HSSubject hSSubject = await db.HSSubjects.FindAsync(id);
            db.HSSubjects.Remove(hSSubject);
            await db.SaveChangesAsync();
            return RedirectToAction(SubjectsIStruct.HsIndex.ActionName);
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
