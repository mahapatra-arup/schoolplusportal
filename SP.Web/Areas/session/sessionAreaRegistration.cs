﻿using System.Web.Mvc;

namespace SP.Web.Areas.session
{
    public class sessionAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "session";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "session_default",
                "session/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}