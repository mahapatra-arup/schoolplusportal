﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SP.Data.Models;

namespace SP.Web.Areas.session.Controllers
{
    public class SessionsController : Controller
    {
        private SP_DataEntities db = new SP_DataEntities();

        #region Sesion
        public async Task<ActionResult> Index()
        {
            return View(await db.Sessions.ToListAsync());
        }


        // GET: session/Sessions/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,XIToXII,VToX,Defult,Date,SlNo,DefultHS")] Session session)
        {
            if (ModelState.IsValid)
            {
                db.Sessions.Add(session);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(session);
        }

        // GET: session/Sessions/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Session session = await db.Sessions.FindAsync(id);
            if (session == null)
            {
                return HttpNotFound();
            }
            return View(session);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,XIToXII,VToX,Defult,Date,SlNo,DefultHS")] Session session)
        {
            if (ModelState.IsValid)
            {
                db.Entry(session).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(session);
        }

        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Session session = await db.Sessions.FindAsync(id);
            if (session == null)
            {
                return HttpNotFound();
            }
            return View(session);
        }

        // POST: session/Sessions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Session session = await db.Sessions.FindAsync(id);
            db.Sessions.Remove(session);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        #endregion

        #region Default Session Set
        [HttpGet]
        public ActionResult DefaultSessionSet()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult DefaultSessionSet([Bind(Include = "XIToXII,VToX")] Session session)
        {
           // var classid = register.ISValidObject() ? register.ClassID.ConvertObjectToString() : "";

            //DropDownlist
           // ViewBag.VToX = classComponent.CmbClass(sessionUtils._gSessionIdVToX, sessionUtils._gSessionIdXIToXII);
           // ViewBag.XIToXII = classComponent.CmbClass(sessionUtils._gSessionIdVToX, sessionUtils._gSessionIdXIToXII);

            return PartialView();
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
