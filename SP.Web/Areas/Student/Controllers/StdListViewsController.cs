﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SP.Data.Models;
using SP.Data;

namespace SP.Web.Areas.Student.Controllers
{
    public class StdListViewsController : Controller
    {
        private SP_DataEntities db = new SP_DataEntities();

        // GET: Student/StdListViews
        public async Task<ActionResult> Index()
        {
            return View(await db.StudentListViews.ToListAsync());
        }

        // GET: Student/StdListViews/Details/5
        public async Task<ActionResult> Details(Guid studenId)
        {
            if (studenId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentListView studentListView = await db.StudentListViews.Where(i=>i.StudentID== studenId).FirstOrDefaultAsync();
            if (studentListView == null)
            {
                return HttpNotFound();
            }
            return View(studentListView);
        }

        
       // GET: Student/StdListViews/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentListView studentListView = await db.StudentListViews.FindAsync(id);
            if (studentListView == null)
            {
                return HttpNotFound();
            }
            return View(studentListView);
        }

        // POST: Student/StdListViews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            StudentListView studentListView = await db.StudentListViews.FindAsync(id);
            db.StudentListViews.Remove(studentListView);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
