// SP.Web.Areas.Student.Controllers.StdReadmissionController
using SP.Data;
using SP.Data.Models;
using SP.Data.session;
using SP.Data.Student;
using SP.Data.Student.ControllersTools;
using SP.Web.Areas.Student.Models;
using SP.Web.Components.Academic;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
namespace SP.Web.Areas.Student.Controllers
{
    public class StdReadmissionController : Controller
    {
        #region Object
        private SP_DataEntities db = new SP_DataEntities();

        private ClassComponent classComponent = new ClassComponent();

        private SessionUtils sessionUtils = new SessionUtils();

        private StudentTools studentTools = new StudentTools();

        private StdReAdmissionUtils stdReAdmissionUtils = new StdReAdmissionUtils();
        StdAdmissionHttpSessionModels stdAdmissionHttpSessionModels = new StdAdmissionHttpSessionModels(); 
        #endregion

        public async Task<ActionResult> StdReadmissionManually(long? ddlCurrentClassID)
        { List<StudentListView> model = new List<StudentListView>();
 model = (await studentTools.GetStudentListViewAsync(ddlCurrentClassID.ConvertObjectToLong())).ToList();
            

            Task.Run(async () => {
                var data  = await classComponent.CmbClassAsync(sessionUtils._gSessionIdVToX, sessionUtils._gSessionIdXIToXII);
                ViewBag.ddlCurrentClassID = data;
            }).Wait();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> StdReadmissionManually(List<StudentListView> studentListViews)
        {
            //Get Session data(Source :_PartialViews/SessionWithClassViews)
            var stdentreg = HttpContextSessionTools.GetHttpContextSessionObject<Std_Register>(stdAdmissionHttpSessionModels._sStdRegister);
            if (stdentreg.ISValidObject())
            {
                if (!stdentreg.ClassID.ISValidObject()|| !stdentreg.SectionID.ISValidObject()|| !stdentreg.SessionID.ISValidObject())
                {
                    ModelState.AddModelError("", "Please Select valid Promoted  Details");
                }
            }
            else
            {
                ModelState.AddModelError("","Please Select valid Promoted  Details");
            }

            if (!studentListViews.IsValidIEnumerable())
            {
                ModelState.AddModelError("", "Invalid Student");
            }

            //Clear Session
            HttpContextSessionTools.RemoveHttpContextSession(stdAdmissionHttpSessionModels._sStdRegister);

            if (ModelState.IsValid)
            {
                List<Std_Register> lststd_Registers = new List<Std_Register>();
                if (studentListViews.IsValidIEnumerable())
                {
                    foreach (var item in studentListViews)
                    {
                        string registerNo = stdentreg.SessionID + "/" + stdentreg.ClassID + "/" + stdentreg.SectionID + "/" + item.RollNo;
                        lststd_Registers.Add(new Std_Register
                        {
                            SessionID = stdentreg.SessionID,
                            StudentId = item.StudentID,
                            RegisterNo = registerNo,
                            RollNo = item.RollNo,
                            ClassID = stdentreg.ClassID,
                            SectionID = stdentreg.SectionID
                        });
                    }
                    var isSuccess = await stdReAdmissionUtils.ReAdmissioneStudentAsync(lststd_Registers);
                   
                    //Return
                    return RedirectToAction("StdReadmissionManually");
                }

            }

            Task.Run(async () =>
            {
                //Class
                ViewBag.ddlCurrentClassID = await classComponent.CmbClassAsync(sessionUtils._gSessionIdVToX, sessionUtils._gSessionIdXIToXII);

            }).Wait();
               return View(studentListViews);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}