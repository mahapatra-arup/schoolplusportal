﻿using SP.Data;
using SP.Data.Academic;
using SP.Data.Models;
using SP.Data.session;
using SP.Data.Student;
using SP.Web.Areas.Student.Models;
using SP.Web.Components.Academic;
using SP.Web.Components.Student;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SP.Web.Areas.Student.Controllers
{
    public class StdAdmissionController : Controller
    {

        #region obj
        //===Use when Student Edit Process Running==
        string StudentIdForEditSessionName { get => "StudentIdForEditSessionName"; }
        public ActionResult RedirectToAfterEdit { get => RedirectToAction("Index", "StdListViews"); }

        //==Objects==
        //Db
        private SP_DataEntities db = new SP_DataEntities();

        //Class/ Sec /Session
        SessionUtils sessionUtils = new SessionUtils();
        ClassTools _classTools = new ClassTools();
        SectionComponent _sectionComponent = new SectionComponent();
        ClassComponent classComponent = new ClassComponent();

        //Student
        StdAdmissionUtils stdAdmissionUtils = new StdAdmissionUtils();
        StudentTools studentTools = new StudentTools();
        StdAdmissionHttpSessionModels _stdAdmissionHttpSessionModels = new StdAdmissionHttpSessionModels();
        #endregion

        #region ====== Student Details ======

        #region <----- Create---- ->

        [ActionName("StdDetailsCreate")]
        public ActionResult StdDetailsCreate()
        {
            //Get the Session Storage  
            Std_Details stdentDetails = HttpContextSessionTools.GetHttpContextSessionObject<Std_Details>(_stdAdmissionHttpSessionModels._sStdDetails);

            //DropDownList  Data Select
            var caste = stdentDetails.ISValidObject() ? stdentDetails.CasteID.ConvertObjectToString() : "";
            var disability = stdentDetails.ISValidObject() ? stdentDetails.DisabilityID.ConvertObjectToString() : "";
            var economicalSatus = stdentDetails.ISValidObject() ? stdentDetails.EconomicalSatusID.ConvertObjectToString() : "";
            var nationality = stdentDetails.ISValidObject() ? stdentDetails.NationalityID.ConvertObjectToString() : "";
            var religion = stdentDetails.ISValidObject() ? stdentDetails.ReligionID.ConvertObjectToString() : "";
            var supportingDoc = stdentDetails.ISValidObject() ? stdentDetails.SupportingDocID.ConvertObjectToString() : "";
            var subCasteNameId = stdentDetails.ISValidObject() ? stdentDetails.SubCasteID.ConvertObjectToString() : "";

            //DropDownListBox
            ViewBag.CasteID = new SelectList(db.Castes, "Id", "CasteName", caste);
            ViewBag.SubCasteID = new SelectList(db.SubCastes, "Id", "SubCasteName", subCasteNameId);
            ViewBag.DisabilityID = new SelectList(db.DisabilityTypes, "Id", "CatagoryName", disability);
            ViewBag.EconomicalSatusID = new SelectList(db.EconomicalStatus, "Id", "EconomicalStatusName", economicalSatus);
            ViewBag.NationalityID = new SelectList(db.Nationalities, "Id", "NationalityName", nationality);
            ViewBag.ReligionID = new SelectList(db.Religions, "Id", "Religion1", religion);
            ViewBag.SupportingDocID = new SelectList(db.SupportingDocs, "Id", "DocumentName", supportingDoc);

            //Return
            return PartialView(stdentDetails);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StdDetailsCreate([Bind(Include = "ID,StudentID,SessionID,StudentName,Gender,DOB,Photo,CasteID,SubCasteID,ReligionID,NationalityID,EconomicalSatusID,EconomicalStatusNo,SupportingDocID,IsPCDisability,PCPer,DisabilityID")] Std_Details std_Details, System.Web.HttpPostedFileBase StudentImage, string btnPrevious, string btnNext, string btnReset)
        {
            string imgMessge = "";
            //Student Image
            #region Image
            if (StudentImage != null)
            {
                if (StudentImage.IsImage(1048576, out imgMessge))
                {
                    std_Details.Photo = new byte[StudentImage.ContentLength];
                    StudentImage.InputStream.Read(std_Details.Photo, 0, StudentImage.ContentLength);
                }
                else
                {
                    ModelState.AddModelError("Photo", imgMessge);
                }
            }

            #endregion

            if (ModelState.IsValid)
            {

                //Create Session Storage and  Store data
                HttpContextSessionTools.CreateHttpContextSession<Std_Details>(_stdAdmissionHttpSessionModels._sStdDetails, std_Details);

                //WIzered Process
                return CreateStepWizered("StdDetailsCreate", btnPrevious, btnNext, btnReset);
            }

            //DropDownList
            ViewBag.CasteID = new SelectList(db.Castes, "Id", "CasteName", std_Details.CasteID);
            ViewBag.SubCasteID = new SelectList(db.SubCastes, "Id", "SubCasteName", std_Details.SubCasteID);
            ViewBag.DisabilityID = new SelectList(db.DisabilityTypes, "Id", "CatagoryName", std_Details.DisabilityID);
            ViewBag.EconomicalSatusID = new SelectList(db.EconomicalStatus, "Id", "EconomicalStatusName", std_Details.EconomicalSatusID);
            ViewBag.NationalityID = new SelectList(db.Nationalities, "Id", "NationalityName", std_Details.NationalityID);
            ViewBag.ReligionID = new SelectList(db.Religions, "Id", "Religion1", std_Details.ReligionID);
            ViewBag.SupportingDocID = new SelectList(db.SupportingDocs, "Id", "DocumentName", std_Details.SupportingDocID);

            return PartialView(std_Details);
        }

        #endregion

        #region <----- Edit ---->

        [ActionName("StdDetailsEdit")]
        public async Task<ActionResult> StdDetailsEdit(Guid? id)
        {
            if (id == null)
            {
                //Get Student Id
                id = HttpContextSessionTools.GetHttpContextSessionObject<Guid>(StudentIdForEditSessionName);
            }

            //Store student Id in session
            HttpContextSessionTools.CreateHttpContextSession<Guid>(StudentIdForEditSessionName, id.ConvertToGuid());

            //Check Id
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Std_Details std_Details = await db.Std_Details.FindAsync(id);
            if (std_Details == null)
            {
                return HttpNotFound();
            }

            //DropDownList
            ViewBag.CasteID = new SelectList(db.Castes, "Id", "CasteName", std_Details.CasteID);
            ViewBag.DisabilityID = new SelectList(db.DisabilityTypes, "Id", "CatagoryName", std_Details.DisabilityID);
            ViewBag.EconomicalSatusID = new SelectList(db.EconomicalStatus, "Id", "EconomicalStatusName", std_Details.EconomicalSatusID);
            ViewBag.NationalityID = new SelectList(db.Nationalities, "Id", "NationalityName", std_Details.NationalityID);
            ViewBag.ReligionID = new SelectList(db.Religions, "Id", "Religion1", std_Details.ReligionID);
            ViewBag.SubCasteID = new SelectList(db.SubCastes, "Id", "SubCasteName", std_Details.SubCasteID);
            ViewBag.SupportingDocID = new SelectList(db.SupportingDocs, "Id", "DocumentName", std_Details.SupportingDocID);

            //Return
            return View(std_Details);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> StdDetailsEdit([Bind(Include = "StudentID,SessionID,StudentName,Gender,DOB,Photo,CasteID,SubCasteID,ReligionID,NationalityID,EconomicalSatusID,EconomicalStatusNo,SupportingDocID,IsPCDisability,PCPer,DisabilityID")] Std_Details std_Details, System.Web.HttpPostedFileBase StudentImage)
        {
            string imgMessge = "";
            #region Image
            if (StudentImage != null)
            {
                if (StudentImage.IsImage(1048576, out imgMessge))
                {
                    std_Details.Photo = new byte[StudentImage.ContentLength];
                    StudentImage.InputStream.Read(std_Details.Photo, 0, StudentImage.ContentLength);
                }
                else
                {
                    ModelState.AddModelError("Photo", imgMessge);
                }
            }
            else
            {
                //var _dataDetails= db.Std_Details.Find(std_Details.StudentID);
                // std_Details.Photo = _dataDetails.Photo;
            }
            #endregion

            //Update
            if (ModelState.IsValid)
            {
                db.Entry(std_Details).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAfterEdit;
            }

            //DropDownList
            ViewBag.CasteID = new SelectList(db.Castes, "Id", "CasteName", std_Details.CasteID);
            ViewBag.DisabilityID = new SelectList(db.DisabilityTypes, "Id", "CatagoryName", std_Details.DisabilityID);
            ViewBag.EconomicalSatusID = new SelectList(db.EconomicalStatus, "Id", "EconomicalStatusName", std_Details.EconomicalSatusID);
            ViewBag.NationalityID = new SelectList(db.Nationalities, "Id", "NationalityName", std_Details.NationalityID);
            ViewBag.ReligionID = new SelectList(db.Religions, "Id", "Religion1", std_Details.ReligionID);
            ViewBag.SubCasteID = new SelectList(db.SubCastes, "Id", "SubCasteName", std_Details.SubCasteID);
            ViewBag.SupportingDocID = new SelectList(db.SupportingDocs, "Id", "DocumentName", std_Details.SupportingDocID);

            //Return
            return View(std_Details);
        }

        #endregion

        #endregion

        #region ====== - ParentsDetails======

        #region <----- Create ----->

        // GET: Student/Std_ParentsDetails/Create
        public ActionResult StdParentsDetailsCreate()
        {
            //Session Storage  Create Store data
            Std_ParentsDetails parentDetails = HttpContextSessionTools.GetHttpContextSessionObject<Std_ParentsDetails>(_stdAdmissionHttpSessionModels._sStdParentsDetails);

            //Return
            return PartialView(parentDetails);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> StdParentsDetailsCreate([Bind(Include = "StudentID,FatherName,FatherAddress,FatherOccupation,FatherAI,FatherQualification,FatherPhoto,FatherContactNo,FatherVoterId,MotherName,MotherAddress,MotherOccupation,MotherAI,MotherQualification,MotherPhoto,MotherContactNo,MotherVoterId")] Std_ParentsDetails std_ParentsDetails,
            HttpPostedFileBase infMotherPhoto, HttpPostedFileBase infFatherPhoto, string btnPrevious, string btnNext, string btnReset)
        {
            string _Messge = "";
            //Session Storage  Data Get
            Std_Details stdentDetails = HttpContextSessionTools.GetHttpContextSessionObject<Std_Details>(_stdAdmissionHttpSessionModels._sStdDetails);
            var studentName = stdentDetails.ISValidObject() ? stdentDetails.StudentName.ConvertObjectToString() : "";
            var studentDob = stdentDetails.ISValidObject() ? stdentDetails.DOB : DateTime.MinValue.Date;

            if (ModelState.IsValid)
            {
                #region ==Image==
                //Father Image
                if (infFatherPhoto != null)
                {
                    string imgMessge = "";
                    if (infFatherPhoto.IsImage(1048576, out imgMessge))
                    {
                        std_ParentsDetails.FatherPhoto = new byte[infFatherPhoto.ContentLength];
                        infFatherPhoto.InputStream.Read(std_ParentsDetails.FatherPhoto, 0, infFatherPhoto.ContentLength);
                    }
                    else
                    {
                        _Messge += Environment.NewLine + imgMessge;
                        ModelState.AddModelError("FatherPhoto", imgMessge);
                    }
                }

                //Mother Photo
                if (infMotherPhoto != null)
                {
                    string imgMessge = "";
                    if (infMotherPhoto.IsImage(1048576, out imgMessge))
                    {
                        std_ParentsDetails.MotherPhoto = new byte[infMotherPhoto.ContentLength];
                        infMotherPhoto.InputStream.Read(std_ParentsDetails.MotherPhoto, 0, infMotherPhoto.ContentLength);
                    }
                    else
                    {
                        _Messge += Environment.NewLine + imgMessge;
                        ModelState.AddModelError("MotherPhoto", imgMessge);

                    }
                }
                #endregion

                #region ==Valid Student==
                if (!(await stdAdmissionUtils.IsValidStudentAsync(studentName, studentDob, std_ParentsDetails.FatherName)))
                {
                    _Messge += Environment.NewLine + studentName + " - " + studentDob.Date + " - " + std_ParentsDetails.FatherName + " are already exists";
                    ModelState.AddModelError("", _Messge);
                }
                #endregion
            }

            if (ModelState.IsValid)
            {
                //Session Storage  Create Store data
                HttpContextSessionTools.CreateHttpContextSession<Std_ParentsDetails>(_stdAdmissionHttpSessionModels._sStdParentsDetails, std_ParentsDetails);

                //Step Wise Process
                return CreateStepWizered("StdParentsDetailsCreate", btnPrevious, btnNext, btnReset);
            }

            ViewBag.StudentID = new SelectList(db.Std_Details, "StudentID", "StudentName");
            return PartialView(std_ParentsDetails);
        }

        #endregion

        #region <----- Edit ------->
        // GET: Student/Std_ParentsDetails/Edit/5
        public async Task<ActionResult> StdParentsDetailsEdit()
        {
            //Get Student Id
            var id = HttpContextSessionTools.GetHttpContextSessionObject<Guid>(StudentIdForEditSessionName);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Std_ParentsDetails std_ParentsDetails = await db.Std_ParentsDetails.FindAsync(id);
            if (std_ParentsDetails == null)
            {
                return HttpNotFound();
            }
            ViewBag.StudentID = new SelectList(db.Std_Details, "StudentID", "StudentName", std_ParentsDetails.StudentID);
            return PartialView(std_ParentsDetails);
        }
        // POST: Student/Std_ParentsDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> StdParentsDetailsEdit([Bind(Include = "StudentID,FatherName,FatherAddress,FatherOccupation,FatherAI,FatherQualification,FatherPhoto,FatherContactNo,FatherVoterId,MotherName,MotherAddress,MotherOccupation,MotherAI,MotherQualification,MotherPhoto,MotherContactNo,MotherVoterId")] Std_ParentsDetails std_ParentsDetails, HttpPostedFileBase infMotherPhoto, HttpPostedFileBase infFatherPhoto)
        {
            string _Messge = "";
            //Session Storage  Data Get
            Std_Details stdentDetails = HttpContextSessionTools.GetHttpContextSessionObject<Std_Details>(_stdAdmissionHttpSessionModels._sStdDetails);
            var studentName = stdentDetails.ISValidObject() ? stdentDetails.StudentName.ConvertObjectToString() : "";
            var studentDob = stdentDetails.ISValidObject() ? stdentDetails.DOB : DateTime.MinValue.Date;

            if (ModelState.IsValid)
            {
                #region ==Image==
                //Father Image
                if (infFatherPhoto != null)
                {
                    string imgMessge = "";
                    if (infFatherPhoto.IsImage(1048576, out imgMessge))
                    {
                        std_ParentsDetails.FatherPhoto = new byte[infFatherPhoto.ContentLength];
                        infFatherPhoto.InputStream.Read(std_ParentsDetails.FatherPhoto, 0, infFatherPhoto.ContentLength);
                    }
                    else
                    {
                        _Messge += Environment.NewLine + imgMessge;
                        ModelState.AddModelError("FatherPhoto", imgMessge);
                    }
                }

                //Mother Photo
                if (infMotherPhoto != null)
                {
                    string imgMessge = "";
                    if (infMotherPhoto.IsImage(1048576, out imgMessge))
                    {
                        std_ParentsDetails.MotherPhoto = new byte[infMotherPhoto.ContentLength];
                        infMotherPhoto.InputStream.Read(std_ParentsDetails.MotherPhoto, 0, infMotherPhoto.ContentLength);
                    }
                    else
                    {
                        _Messge += Environment.NewLine + imgMessge;
                        ModelState.AddModelError("MotherPhoto", imgMessge);

                    }
                }
                #endregion

                #region ==Valid Student==
                if (!(await stdAdmissionUtils.IsValidStudentAsync(studentName, studentDob, std_ParentsDetails.FatherName)))
                {
                    _Messge += Environment.NewLine + studentName + " - " + studentDob.Date + " - " + std_ParentsDetails.FatherName + " are already exists";
                    ModelState.AddModelError("", _Messge);
                }
                #endregion
            }
            if (ModelState.IsValid)
            {
                db.Entry(std_ParentsDetails).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAfterEdit;
            }
            ViewBag.StudentID = new SelectList(db.Std_Details, "StudentID", "StudentName", std_ParentsDetails.StudentID);
            return PartialView(std_ParentsDetails);
        }

        #endregion

        #endregion

        #region =========  Address  =========

        #region <-- Create -->

        // GET: Student/Std_Address/Create
        public ActionResult StdAddressCreate()
        {
            //Session Storage  Create Store data
            Std_Address std_Address = HttpContextSessionTools.GetHttpContextSessionObject<Std_Address>(_stdAdmissionHttpSessionModels._sStdAddress);

            var prDist = std_Address.ISValidObject() ? std_Address.PrDistID.ConvertObjectToString() : "";
            var pmDist = std_Address.ISValidObject() ? std_Address.PmDistID.ConvertObjectToString() : "";


            ViewBag.PrDistID = new SelectList(db.Districts, "Id", "DistName", prDist);
            ViewBag.PmDistID = new SelectList(db.Districts, "Id", "DistName", pmDist);
            return PartialView(std_Address);
        }
        // POST: Student/Std_Address/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StdAddressCreate([Bind(Include = "Id,StudentID,PrVill,PrBlock,PrPO,PrGP,PrPS,PrDistID,PrPIN,PmVill,PmBlock,PmPO,PmGP,PmPS,PmDistID,PmPIN")] Std_Address std_Address, string btnPrevious, string btnNext, string btnReset)
        {
            if (ModelState.IsValid)
            {
                //Session Storage  Create Store data
                HttpContextSessionTools.CreateHttpContextSession<Std_Address>(_stdAdmissionHttpSessionModels._sStdAddress, std_Address);

                //Step Wise Process
                return CreateStepWizered("StdAddressCreate", btnPrevious, btnNext, btnReset);
            }

            ViewBag.PrDistID = new SelectList(db.Districts, "Id", "DistName");
            ViewBag.PmDistID = new SelectList(db.Districts, "Id", "DistName");

            return PartialView(std_Address);
        }

        #endregion

        #region <----- Edit --->

        // GET: Student/Std_Address/Edit/5
        public async Task<ActionResult> StdAddressEdit()
        {
            //Get Student Id
            var sid = HttpContextSessionTools.GetHttpContextSessionObject<Guid>(StudentIdForEditSessionName);

            if (sid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Std_Address std_Address = await db.Std_Address.Where(s => s.StudentID == sid).FirstOrDefaultAsync();
            if (std_Address == null)
            {
                return HttpNotFound();
            }
            ViewBag.PrDistID = new SelectList(db.Districts, "Id", "DistName", std_Address.PmDistID);
            ViewBag.PmDistID = new SelectList(db.Districts, "Id", "DistName", std_Address.PmDistID);
            return PartialView(std_Address);
        }

        // POST: Student/Std_Address/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> StdAddressEdit([Bind(Include = "Id,StudentID,PrVill,PrBlock,PrPO,PrGP,PrPS,PrDistID,PrPIN,PmVill,PmBlock,PmPO,PmGP,PmPS,PmDistID,PmPIN")] Std_Address std_Address)
        {
            if (ModelState.IsValid)
            {
                db.Entry(std_Address).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAfterEdit;
            }
            ViewBag.PrDistID = new SelectList(db.Districts, "Id", "DistName", std_Address.PmDistID);
            ViewBag.PmDistID = new SelectList(db.Districts, "Id", "DistName", std_Address.PmDistID);
            return PartialView(std_Address);
        }
        #endregion

        #endregion

        #region =======  More Details =======

        #region <-- Create -->
        // GET: Student/Std_MoreDetails/Create
        public ActionResult StdMoreDetailsCreate()
        {
            //Session Storage  Create Store data
            Std_MoreDetails std_MoreDetails = HttpContextSessionTools.GetHttpContextSessionObject<Std_MoreDetails>(_stdAdmissionHttpSessionModels._sStdMoreDetails);
            var motherTongu = std_MoreDetails.ISValidObject() ? std_MoreDetails.MotherTongueID.ConvertObjectToString() : "";
            var previousSchoo = std_MoreDetails.ISValidObject() ? std_MoreDetails.PreviousSchoolID.ConvertObjectToString() : "";

            ViewBag.MotherTongueID = new SelectList(db.MotherTongues, "Id", "MotherTongueName", motherTongu);
            ViewBag.PreviousSchoolID = new SelectList(db.PreviousSchools, "Id", "SchoolName", previousSchoo);
            return PartialView(std_MoreDetails);
        }

        // POST: Student/Std_MoreDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StdMoreDetailsCreate([Bind(Include = "ID,StudentID,MotherTongueID,ContactNo,AadhaarNo,PreviousSchoolID,PreviousClass,GuardianName,RelationWithGuardian,GuardianAddress,GuardianOccupation,GuardianAI,GuardianQualification,GuardianPhoto,GuardianContactNo,GuardianContactNo2,GuardianVoterId,GuardianMailID,IsHostelNeed,IsTransportNeed")] Std_MoreDetails std_MoreDetails, string btnPrevious, string btnNext, string btnReset)
        {
            if (ModelState.IsValid)
            {
                //Session Storage  Create Store data
                HttpContextSessionTools.CreateHttpContextSession<Std_MoreDetails>(_stdAdmissionHttpSessionModels._sStdMoreDetails, std_MoreDetails);

                //Step Wise Process
                return CreateStepWizered("StdMoreDetailsCreate", btnPrevious, btnNext, btnReset);
            }
            ViewBag.MotherTongueID = new SelectList(db.MotherTongues, "Id", "MotherTongueName");
            ViewBag.PreviousSchoolID = new SelectList(db.PreviousSchools, "Id", "SchoolName");
            ViewBag.StudentID = new SelectList(db.Std_Details, "StudentID", "StudentName");
            return PartialView();
        }

        #endregion

        #region <--< Edit -->

        // GET: Student/Std_MoreDetails/Edit/5
        public async Task<ActionResult> StdMoreDetailsEdit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Std_MoreDetails std_MoreDetails = await db.Std_MoreDetails.FindAsync(id);
            if (std_MoreDetails == null)
            {
                return HttpNotFound();
            }
            ViewBag.MotherTongueID = new SelectList(db.MotherTongues, "Id", "MotherTongueName", std_MoreDetails.MotherTongueID);
            ViewBag.PreviousSchoolID = new SelectList(db.PreviousSchools, "Id", "SchoolName", std_MoreDetails.PreviousSchoolID);
            return View(std_MoreDetails);
        }

        // POST: Student/Std_MoreDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> StdMoreDetailsEdit([Bind(Include = "ID,StudentID,MotherTongueID,ContactNo,AadhaarNo,PreviousSchoolID,PreviousClass,GuardianName,RelationWithGuardian,GuardianAddress,GuardianOccupation,GuardianAI,GuardianQualification,GuardianPhoto,GuardianContactNo,GuardianContactNo2,GuardianVoterId,GuardianMailID,IsHostelNeed,IsTransportNeed")] Std_MoreDetails std_MoreDetails)
        {
            if (ModelState.IsValid)
            {
                db.Entry(std_MoreDetails).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAfterEdit;
            }
            ViewBag.MotherTongueID = new SelectList(db.MotherTongues, "Id", "MotherTongueName", std_MoreDetails.MotherTongueID);
            ViewBag.PreviousSchoolID = new SelectList(db.PreviousSchools, "Id", "SchoolName", std_MoreDetails.PreviousSchoolID);
            return PartialView(std_MoreDetails);
        }

        #endregion

        #endregion

        #region ======  Health Details ======

        #region <-- Create-->

        // GET: Student/Std_Health/Create
        public ActionResult StdHealthCreate()
        {
            //Session Storage  Create Store data
            Std_Health std_Health = HttpContextSessionTools.GetHttpContextSessionObject<Std_Health>(_stdAdmissionHttpSessionModels._sStdHealth);
            var bloodGroup = std_Health.ISValidObject() ? std_Health.BloodGroupID.ConvertObjectToString() : "";

            ViewBag.BloodGroupID = new SelectList(db.BloodGroups, "Id", "GroupName", bloodGroup);

            return PartialView(std_Health);
        }

        // POST: Student/Std_Health/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StdHealthCreate([Bind(Include = "ID,StudentID,Weight,Height,Chest,BodyColor,BloodGroupID,IdentificationMark,PermanentDisease")] Std_Health std_Health, string btnPrevious, string btnNext, string btnReset)
        {
            if (ModelState.IsValid)
            {
                //Session Storage  Create Store data
                HttpContextSessionTools.CreateHttpContextSession<Std_Health>(_stdAdmissionHttpSessionModels._sStdHealth, std_Health);

                //Step Wise Process
                return CreateStepWizered("StdHealthCreate", btnPrevious, btnNext, btnReset);
            }

            ViewBag.BloodGroupID = new SelectList(db.BloodGroups, "Id", "GroupName");
            ViewBag.StudentID = new SelectList(db.Std_Details, "StudentID", "StudentName");
            return PartialView(std_Health);
        }

        #endregion

        #region <-- Edit -->

        // GET: Student/Std_Health/Edit/5
        public async Task<ActionResult> StdHealthEdit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Std_Health std_Health = await db.Std_Health.FindAsync(id);
            if (std_Health == null)
            {
                return HttpNotFound();
            }
            ViewBag.BloodGroupID = new SelectList(db.BloodGroups, "Id", "GroupName", std_Health.BloodGroupID);
            return PartialView(std_Health);
        }

        // POST: Student/Std_Health/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> StdHealthEdit([Bind(Include = "ID,StudentID,Weight,Height,Chest,BodyColor,BloodGroupID,IdentificationMark,PermanentDisease")] Std_Health std_Health)
        {
            if (ModelState.IsValid)
            {
                db.Entry(std_Health).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAfterEdit;
            }
            ViewBag.BloodGroupID = new SelectList(db.BloodGroups, "Id", "GroupName", std_Health.BloodGroupID);
            return PartialView(std_Health);
        }

        #endregion
        #endregion

        #region  ====  Register/Admission ====

        #region <--- Create -->

        public async Task<ActionResult> StdRegisterCreate()
        {

            //Create Session Storage and  Store ther data
            Std_Register register = HttpContextSessionTools.GetHttpContextSessionObject<Std_Register>(_stdAdmissionHttpSessionModels._sStdRegister);
            Std_AdmissionDetails admission = HttpContextSessionTools.GetHttpContextSessionObject<Std_AdmissionDetails>(_stdAdmissionHttpSessionModels._sStdAdmissionDetails);
            var toupleData = new StdRegisterVM() { _cStd_Register = register, _cStd_AdmissionDetails = admission };

            var classid = register.ISValidObject() ? register.ClassID.ConvertObjectToString() : "";

            //DropDownlist
            //Class
            ViewBag.ClassID =await classComponent.CmbClassAsync(sessionUtils._gSessionIdVToX, sessionUtils._gSessionIdXIToXII);
            //Section
            ViewBag.SectionID =await _sectionComponent.CmbSectionAsync(classid.ConvertObjectToInt());

            //Return
            return PartialView(toupleData);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> StdRegisterCreate(StdRegisterVM _data, string btnPrevious, string btnFinish, String btnReset)
        {

            // "Register"
            HttpContextSessionTools.CreateHttpContextSession<Std_Register>(_stdAdmissionHttpSessionModels._sStdRegister, _data._cStd_Register);

            // "Admission"
            HttpContextSessionTools.CreateHttpContextSession<Std_AdmissionDetails>(_stdAdmissionHttpSessionModels._sStdAdmissionDetails, _data._cStd_AdmissionDetails);



            if (btnFinish != null)
            {

                //Admission No
                if (await studentTools.IsAdmissionNoExistAsync(_data._cStd_AdmissionDetails.AdmissionNo))
                {
                    ModelState.AddModelError("", "Admission No Alredy Exist");
                }

                //RegisterNo
                if (await studentTools.IsRegisterNoExistAsync(_data._cStd_Register.RegisterNo))
                {
                    ModelState.AddModelError("", "Register No Alredy Exist");
                }

                if (ModelState.IsValid)
                {
                    if (await SaveData())
                    {
                        return RedirectToAction("Index", "StdListViews");
                    }
                }
            }

            if (btnPrevious != null)
            {
                //Step Wise Process
                return CreateStepWizered("StdHealthCreate", btnPrevious, null, btnReset);
            }

            var classId = _data._cStd_Register.ISValidObject() ? _data._cStd_Register.ClassID.ConvertObjectToString() : "";

            //Class
            ViewBag.ClassID =await classComponent.CmbClassAsync(sessionUtils._gSessionIdVToX, sessionUtils._gSessionIdXIToXII);
            //Section
            ViewBag.SectionID =await _sectionComponent.CmbSectionAsync(classId.ConvertObjectToInt());

            //Return
            return PartialView(_data);
        }

        #endregion

        #endregion

        #region =========== Bank  ===========
        #region Create
        [ChildActionOnly]
        public ActionResult StdBankCreate()
        {
            //Get data
            Std_BankDetails std_BankDetails = HttpContextSessionTools.GetHttpContextSessionObject<Std_BankDetails>(_stdAdmissionHttpSessionModels._sStd_BankDetails);

            //DropDownBox
            var branchDetailsID = std_BankDetails.ISValidObject() ? std_BankDetails.BranchDetailsID.ConvertObjectToString() : "";
            ViewBag.BranchDetailsID = new SelectList(db.BranchDetails, "Id", "BranchName", branchDetailsID);

            //Return
            return PartialView(std_BankDetails);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StdBankCreate([Bind(Include = "BranchDetailsID,AccountNo,AccountHolderName")] Std_BankDetails std_BankDetails)
        {
            //Create Session Storage and  Store the data
            HttpContextSessionTools.CreateHttpContextSession<Std_BankDetails>(_stdAdmissionHttpSessionModels._sStd_BankDetails, std_BankDetails);


            //DropDownBox
            var branchDetailsID = std_BankDetails.ISValidObject() ? std_BankDetails.BranchDetailsID.ConvertObjectToString() : "";
            ViewBag.BranchDetailsID = new SelectList(db.BranchDetails, "Id", "BranchName", branchDetailsID);

            return PartialView(std_BankDetails);
        }

        #endregion

        #region ==Edit==
        // GET: Student/Std_BankDetails/Edit/5
        public async Task<ActionResult> StdBankEdit()
        {
            //Get Student Id
            var id = HttpContextSessionTools.GetHttpContextSessionObject<Guid>(StudentIdForEditSessionName);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var std_BankDetails = await db.Std_BankDetails.Where(s => s.StudentId == id).FirstOrDefaultAsync();

            //Branch
            var branchDetailsID = std_BankDetails.ISValidObject() ? std_BankDetails.BranchDetailsID.ConvertObjectToString() : "";
            ViewBag.BranchDetailsID = new SelectList(db.BranchDetails, "Id", "BranchName", branchDetailsID);

            return View(std_BankDetails);
        }

        // POST: Student/Std_BankDetails/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> StdBankEdit([Bind(Include = "ID,StudentId,BranchDetailsID,AccountNo,AccountHolderName")] Std_BankDetails std_BankDetails)
        {
            if (ModelState.IsValid)
            {
                db.Entry(std_BankDetails).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.BranchDetailsID = new SelectList(db.BranchDetails, "Id", "BranchName", std_BankDetails.BranchDetailsID);
            return View(std_BankDetails);
        }

        #endregion
        #endregion

        #region****Create Wizered Tools******

        [NonAction]
        public ActionResult CreateStepWizered(string _CurrentAction, string btnPrevious, string btnNext, string btnReset)
        {
            if (btnNext.ISValidObject())
            {

                switch (_CurrentAction)
                {
                    case "StdDetailsCreate":
                        return RedirectToAction("StdParentsDetailsCreate");


                    case "StdParentsDetailsCreate":
                        return RedirectToAction("StdAddressCreate");


                    case "StdAddressCreate":
                        return RedirectToAction("StdMoreDetailsCreate");


                    case "StdMoreDetailsCreate":
                        return RedirectToAction("StdHealthCreate");

                    case "StdHealthCreate":
                        return RedirectToAction("StdRegisterCreate");

                }

            }
            if (btnPrevious != null)
            {
                switch (_CurrentAction)
                {
                    // Student Details <<--Parent Details
                    case "StdParentsDetailsCreate":
                        return RedirectToAction("StdDetailsCreate");


                    //  StdParentsDetailsCreate <<--StdAddressCreate
                    case "StdAddressCreate":
                        return RedirectToAction("StdParentsDetailsCreate");


                    //  StdAddressCreate <<--StdMoreDetailsCreate
                    case "StdMoreDetailsCreate":
                        return RedirectToAction("StdAddressCreate");


                    //  StdMoreDetailsCreate <<--StdHealthCreate
                    case "StdHealthCreate":
                        return RedirectToAction("StdMoreDetailsCreate");

                    //  StdHealthCreate <<--StdRegisterCreate
                    case "StdRegisterCreate":
                        return RedirectToAction("StdHealthCreate");
                }
            }
            if (btnReset != null)
            {
                return RedirectToAction(_CurrentAction);
            }
            return View(_CurrentAction);
        }
        [NonAction]
        public ActionResult ResetStepWizered(string _CurrentAction, string btnReset)
        {

            if (btnReset != null)
            {
                ModelState.Clear();
                return RedirectToAction(_CurrentAction);
            }
            return View(_CurrentAction);

        }
        [NonAction]
        private async Task<bool> SaveData()
        {
            //std_Details
            Std_Details std_Details = HttpContextSessionTools.GetHttpContextSessionObject<Std_Details>(_stdAdmissionHttpSessionModels._sStdDetails);

            //Std_ParentsDetails
            Std_ParentsDetails std_ParentsDetails = HttpContextSessionTools.GetHttpContextSessionObject<Std_ParentsDetails>(_stdAdmissionHttpSessionModels._sStdParentsDetails);


            //Std_Address
            Std_Address std_Address = HttpContextSessionTools.GetHttpContextSessionObject<Std_Address>(_stdAdmissionHttpSessionModels._sStdAddress);


            //Std_MoreDetails
            Std_MoreDetails std_MoreDetails = HttpContextSessionTools.GetHttpContextSessionObject<Std_MoreDetails>(_stdAdmissionHttpSessionModels._sStdMoreDetails);


            //Std_Health
            Std_Health std_Health = HttpContextSessionTools.GetHttpContextSessionObject<Std_Health>(_stdAdmissionHttpSessionModels._sStdHealth);


            //Std_Register/Std_AdmissionDetails
            Std_Register std_Register = HttpContextSessionTools.GetHttpContextSessionObject<Std_Register>(_stdAdmissionHttpSessionModels._sStdRegister);
            Std_AdmissionDetails std_AdmissionDetails = HttpContextSessionTools.GetHttpContextSessionObject<Std_AdmissionDetails>(_stdAdmissionHttpSessionModels._sStdAdmissionDetails);

            //Std_BankDetails
            Std_BankDetails std_BankDetails = HttpContextSessionTools.GetHttpContextSessionObject<Std_BankDetails>(_stdAdmissionHttpSessionModels._sStd_BankDetails);

            //==classId fill==
            std_AdmissionDetails.ClassID = std_Register.ClassID;

            //====Filling StudentID in All Table===
            std_Details.StudentID = Guid.NewGuid();
            std_ParentsDetails.StudentID = std_Details.StudentID;
            std_Address.StudentID = std_Details.StudentID;
            std_MoreDetails.StudentID = std_Details.StudentID;
            std_Health.StudentID = std_Details.StudentID;
            std_Register.StudentId = std_Details.StudentID;
            std_AdmissionDetails.StudentId = std_Details.StudentID;

            //Bank
            if (std_BankDetails.ISValidObject())
            {
                std_BankDetails.StudentId = std_Details.StudentID;
                std_BankDetails.BranchDetailsID = std_BankDetails.BranchDetailsID.ISValidObject() ? std_BankDetails.BranchDetailsID : null;
                std_BankDetails.AccountHolderName = std_BankDetails.AccountHolderName.ISValidObject() ? std_BankDetails.AccountHolderName : null;
                std_BankDetails.AccountNo = std_BankDetails.AccountNo.ISValidObject() ? std_BankDetails.AccountNo : null;

            }
            else
            {
                std_BankDetails = new Std_BankDetails
                {
                    StudentId = std_Details.StudentID,
                    BranchDetailsID = null,
                    AccountHolderName = null,
                    AccountNo = null
                };
            }


            //==SessionId==
            std_Register.SessionID =(await  _classTools.GetClassAsync(std_Register.ClassID)).SessionId;
            std_AdmissionDetails.SessionId = std_Register.SessionID;
            std_Details.SessionID = std_Register.SessionID;

            //obj
            StdAdmissionUtils _stdAdmissionUtils = new StdAdmissionUtils();

            //Save
            return await _stdAdmissionUtils.SaveStudentAsync(std_Details, std_Address, std_ParentsDetails, std_Register, std_Health, std_AdmissionDetails, std_MoreDetails, std_BankDetails);
        }
        #endregion

        #region ======= Delete Student =======
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Std_Details std_Details = await db.Std_Details.Include(r => r.Std_Register).Where(i => i.StudentID == id).FirstOrDefaultAsync();
            if (std_Details == null)
            {
                return HttpNotFound();
            }
            return View(std_Details);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id, string RegisterNo, string btnPermanent)
        {
            if (btnPermanent.ISValidObject())
            {
                var result = await stdAdmissionUtils.DeleteStd_DetailsAsync(id);
            }
            else
            {
                var result = await stdAdmissionUtils.DeleteStd_RegisterAsync(RegisterNo);
            }
            return RedirectToAction("Index");
        }
        #endregion



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
