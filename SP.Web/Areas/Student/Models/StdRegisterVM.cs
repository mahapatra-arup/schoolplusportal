﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SP.Web.Areas.Student.Models
{
    /// <summary>
    /// use for Custom Bind Register and Admission Table
    /// </summary>
    public class StdRegisterVM  //vm mens View Model
    {
        public Std_Register _cStd_Register { get; set; }
        public Std_AdmissionDetails _cStd_AdmissionDetails { get; set; }
    }
}