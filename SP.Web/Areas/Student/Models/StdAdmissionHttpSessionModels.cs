﻿namespace SP.Web.Areas.Student.Models
{
    /// <summary>
    /// use for Store  data in Session Level like Std_Details,Std_Address , etc.
    /// </summary>
    public class StdAdmissionHttpSessionModels
    {

        //Wizered Static class Data
        public string _sStdDetails
        {
            get => "_sStdDetails";
        }
        public string _sStdAddress
        {
            get => "_sStdAddress";
        }
        public string _sStdParentsDetails
        {
            get => "_sStdParentsDetails";
        }
        public string _sStdRegister
        {
            get => "_sStdRegister";
        }
        public string _sStdMoreDetails
        {
            get => "_sStdMoreDetails";
        }
        public string _sStdHealth
        {
            get => "_sStdHealth";
        }
        public string _sStdAdmissionDetails
        {
            get => "_sStdAdmissionDetails";
        }


        public string _sStd_BankDetails
        {
            get => "_sStd_BankDetails";
        }

    }
}

