﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SP.Web.Areas.Student.Models
{
    public class StdCerificateVM
    {
        public string RegisterNo { get; set; }

        [Display(Name = "Certificates")]
        [Required(ErrorMessage ="Invalid Selection !!")]
        public string CertificateName { get; set; }
    }
}