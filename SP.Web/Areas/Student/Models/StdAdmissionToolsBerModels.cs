﻿using System.ComponentModel.DataAnnotations;

namespace SP.Web.Areas.Student.Models
{
    /// <summary>
    /// This properties are help hor create menu tab in admission time (a>serial menu and b> active menu)
    /// by defult all are False
    /// </summary>
    public class StdAdmissionToolsBerModels
    {
        [Display(Name = "Details")]
        public bool StudentDetails { get; set; } = false;

        [Display(Name = "Parent")]
        public bool StudentParentDetails { get; set; } = false;

        [Display(Name = "Address")]
        public bool StudentAddress { get; set; } = false;

        [Display(Name = "More")]
        public bool StudentMoreDetails { get; set; } = false;

        [Display(Name = "Health")]
        public bool StudentHealth { get; set; } = false;

        [Display(Name = "Academic")]
        public bool Studentacademic { get; set; } = false;

    }
}