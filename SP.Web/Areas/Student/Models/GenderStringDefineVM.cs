﻿namespace SP.Web.Areas.Student.Models
{
    public class GenderStringDefineVM
    {
        public string HeShe { get; set; }
        public string HisHer { get; set; }
        public string HimHer { get; set; }
        public string SriSmt { get; set; }
        public string SonDaughter { get; set; }
    }
}