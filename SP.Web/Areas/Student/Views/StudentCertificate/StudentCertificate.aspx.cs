﻿using CrystalDecisions.CrystalReports.Engine;
using SP.Data;
using SP.Data.Academic;
using SP.Data.Enums;
using SP.Data.Models;
using SP.Data.Others;
using SP.Data.session;
using SP.Data.Student;
using SP.Data.Student.Tools;
using SP.Data.SysTools;
using SP.Web.Areas.Student.Models;
using SP.Web.DataSets.Student;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SP.Web.Areas.Student.Views.StudentCertificate
{
    public partial class StudentCertificate : System.Web.UI.Page
    {
        //Var
        #region Var
        private SP_DataEntities db = new SP_DataEntities();
        private DataSet _DtSet;
        private StudentTools studentTools;
        WrittenNumericUtils writtenNumericUtils;
        SchoolUtils schoolUtils;
        StudentCertificateTools studentCertificateTools;
        ClassTools classTools;
        StateTools stateTools;
        DistrictTools districtTools;
        ReportDocument reportDocument = null;

        //Read only
        string Certificate_DsTable { get => "Character"; }
        string Transfer_DsTable { get => "Transfer"; }
        string StudentBoardResult_DsTable { get => "StudentBoardResult"; }


        #endregion

        #region Constructor
        public StudentCertificate()
        {
            db = new SP_DataEntities();
            _DtSet = new DSCertificate();
            studentTools = new StudentTools();
            writtenNumericUtils = new WrittenNumericUtils();
            schoolUtils = new SchoolUtils();
            studentCertificateTools = new StudentCertificateTools();

            classTools = new ClassTools();
            stateTools = new StateTools();
            districtTools = new DistrictTools();
        }
        #endregion

        protected async void Page_Load(object sender, EventArgs e)
        {
            string regNo = Request.QueryString["regNo"];
            string certificateName = Request.QueryString["certificateName"];

            //if (Page.IsPostBack)
            //{
                await LoadReport(regNo, certificateName);
            //}
        }

        #region No Action 
        private async Task LoadReport(string RegisterNo, string CertificateName)
        {
            if (RegisterNo.ISNullOrWhiteSpace())
            {
                throw new HttpException(404, "Invalid Register No!!!");
            }

            //Enum Convert
            var Certificate_Enum = CertificateName.ConvertStringToEnumField<StudentsEnum.StudentCertificates>();

            if (!Certificate_Enum.IsEnumValue())
            {
                throw new HttpException(404, "Invalid Certificate Selection");
            }

            //Fill Data
            var path = await CertificateFillConfig(RegisterNo, Certificate_Enum);

            //Load Report
            if (this.reportDocument != null)
            {
                this.reportDocument.Close();
                this.reportDocument.Dispose();
            }
            reportDocument = new ReportDocument();


            reportDocument.Load(Path.Combine(Server.MapPath(path.Item1), path.Item2));
            reportDocument.SetDataSource(_DtSet);

            // Report connection
            CrStudentCertificate.ReportSource = reportDocument;
            CrStudentCertificate.DataBind();
        }



        /// <summary>
        /// GetSetValues<Path,TemplateName>
        /// </summary>
        /// <param name="registerNo"></param>
        /// <param name="studentCertificates"></param>
        /// <returns></returns>

        private async Task<GetSetValues<string, string>> CertificateFillConfig(string registerNo, StudentsEnum.StudentCertificates studentCertificates)
        {
            GetSetValues<string, string> getSetValues = new GetSetValues<string, string>();

            //Path
            #region Path Define

            var CertifiTemp_result = await studentCertificateTools.GetCertificateTemplateAsync(studentCertificates);
            if (!CertifiTemp_result.ISValidObject())
            {
                throw new HttpException(404, "Certificate Template Not Found");
            }

            getSetValues.Item1 = CertifiTemp_result.Path;
            getSetValues.Item2 = CertifiTemp_result.TemplateName;
            #endregion


            #region Decission Making
            switch (studentCertificates)
            {
                case StudentsEnum.StudentCertificates.Charecter_Certificate:
                case StudentsEnum.StudentCertificates.MPLeavingCertificate:
                case StudentsEnum.StudentCertificates.HSLeavingCertificate:
                case StudentsEnum.StudentCertificates.SchoolCertificate:
                case StudentsEnum.StudentCertificates.CasteCertificate:
                    await FillCharacter(registerNo, studentCertificates);
                    break;

                case StudentsEnum.StudentCertificates.Transfer_Certificate:
                    // await FillTransfer(registerNo);
                    break;
                case StudentsEnum.StudentCertificates.Transfer_Certificate_Duplicate:
                    break;
            }
            #endregion


            return getSetValues;

        }

        private async Task FillCharacter(string _RegisterNo, StudentsEnum.StudentCertificates studentCertificates)
        {
            _DtSet.Tables[Certificate_DsTable].Clear();

            #region Get
            var school = await schoolUtils.GetSchoolProfileAsync();
            var std = await studentTools.GetStudentListViewAsync(_RegisterNo);

            //custom
            var IssueDate = DateTime.Now.ToString("dd-MMM-yyyy");
            var PrintDate = DateTime.Now.ToString("dd-MMM-yyyy");
            var FirstClass = std.FirstClassId.ISValidObject() ? (await classTools.GetClassAsync(std.FirstClassId)).ClassName : "";
            var slno = await studentCertificateTools.GetCertificateIssueMaxNoAsync(studentCertificates) + 1;
            var dist = await districtTools.GetDistrictAsync(std.PmDistID.ConvertObjectToInt());
            var state = await stateTools.GetStateAsync(std.PmStateId.ConvertObjectToInt());

            #endregion

            #region Data fill 
            //Do  not change serial system
            var charFillData = (new
            {
                No = slno,
                MemoNo = std.RegisterNo,

                #region student
                RegisterNo = std.RegisterNo,
                AdmissionNo = std.AdmissionNo,
                AdmissionDate = std.AdmissionDate,
                StudentName = std.StudentName,
                Image = std.Photo,
                DOB = std.DOB.ToString("dd-MMM-yyyy"),
                DOBNumeric = writtenNumericUtils.DateToWritten(std.DOB),
                AadharNo = std.AadhaarNo,
                #endregion

                #region Guardian
                FatherName = std.FatherName,
                MotherName = std.MotherName,
                GurdianName = std.GuardianName,
                GurdianVoterId = std.GuardianVoterId,
                #endregion

                #region Academic
                Session = "",
                Class = std.ClassName,
                Sec = std.SectionName,
                RollNo = std.RollNo,
                FirstClass = FirstClass,
                #endregion

                #region Address
                Vill = std.PmVill,
                PO = std.PmPO,
                Dist = dist.ISValidObject() ? dist.DistName : "",
                Pin = std.PmPIN,
                PS = std.PmPS,
                State = state.ISValidObject() ? state.State1 : "",
                Block = std.PmBlock,
                SubDevision = "",
                GP = std.PmGP,
                #endregion

                #region caste/ eco/ religion
                Caste = std.CasteName,
                SubCaste = std.SubCasteName,
                Religion = std.Religion,
                EconomicalStatus = std.EconomicalStatusName,
                EconomicalStatusNO = std.EconomicalStatusNo,
                KanyashreeID = std.KanyashreeID,
                #endregion

                #region board activity
                Board_Roll = "",
                Board_No = "",
                RegistrationYear = "",
                RegistrationNo = "",
                #endregion

                #region Gender wise
                SonDaughter = GenderStringDefine(std.Gender).SonDaughter,
                HeShe = GenderStringDefine(std.Gender).HeShe,
                HisHer = GenderStringDefine(std.Gender).HisHer,
                HimHer = GenderStringDefine(std.Gender).HimHer,
                SriSmt = GenderStringDefine(std.Gender).SriSmt,
                #endregion

                #region School
                SchoolName = school.SchoolName,
                WaterMark = school.WaterMark,
                Logo = school.Logo,
                Signature = school.HMSignature,
                SignatureArea = school.SignatureArea,
                #endregion

                IssueDate = IssueDate,
                PrintDate = PrintDate

            });
            #endregion

            if (std.ISValidObject())
            {
                var d = charFillData.ConvertPropertyToArray();
                _DtSet.Tables[Certificate_DsTable].Rows.Add(d);
            }
        }


        public GenderStringDefineVM GenderStringDefine(string _Gender)
        {
            if (_Gender == "MALE")
            {
                return new GenderStringDefineVM
                {
                    SonDaughter = "Son of",
                    HeShe = "He",
                    HisHer = "His",
                    HimHer = "him",
                    SriSmt = "Sri"//
                };

            }
            else
            {
                return new GenderStringDefineVM
                {
                    SonDaughter = "Daughter of",
                    HeShe = "She",
                    HisHer = "Her",
                    HimHer = "her",
                    SriSmt = "Smt"//
                };
            }
            return new GenderStringDefineVM();
        }


        #endregion

        protected async void Preview_Click(object sender, EventArgs e)
        {
            string regNo = Request.QueryString["regNo"];
            string certificateName = Request.QueryString["certificateName"];

            await LoadReport(regNo, certificateName);
        }
    }
}