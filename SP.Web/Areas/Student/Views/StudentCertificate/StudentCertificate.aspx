﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/CrystalRepoteViewer.Master" AutoEventWireup="true" CodeBehind="StudentCertificate.aspx.cs" Inherits="SP.Web.Areas.Student.Views.StudentCertificate.StudentCertificate" Async="true" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <br />
        <!-- Certificate-->
        <div class="card">
            <div class="card-header card-outline card-primary">
                <h3 class="card-title">Certificate</h3>
            </div>

            <div class="card-body p-0 mx-3">
                <div class="row">
                    <div class="col-md-9">

                        <%-- Report --%>
                        <cr:crystalreportviewer id="CrStudentCertificate" runat="server" hascrystallogo="False"
                            autodatabind="True" height="50px" enableparameterprompt="true" enabledatabaselogonprompt="false" toolpanelwidth=""
                            width="350px" borderstyle="Outset" grouptreestyle-backcolor="White" grouptreestyle-borderstyle="Ridge" toolbarstyle-backcolor="#65D6D2" toolbarstyle-bordercolor="#006666" toolbarstyle-borderstyle="Groove" toolpanelview="None" />

                        <asp:Button value="Preview" Text="Preview" runat="server" ID="Preview" type="submit" OnClick="Preview_Click" />
                    </div>

                    <!--History Of Certificate-->
                    <div class="col-md-3">
                        <div class="mx-3">
                            <table class="display table table-striped table-bordered text-sm nowrap text-center " style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>SlNo
                                        </th>
                                        <th>Date
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>


                                    <tr>
                                        <td></td>
                                        <td></td>


                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--//History Of Certificate-->

                </div>


            </div>

            <div class="card-footer">
            </div>

        </div>
        <!--//Certificate-->
    </div>

</asp:Content>
