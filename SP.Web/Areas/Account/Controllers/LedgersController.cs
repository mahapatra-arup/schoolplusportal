﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SP.Data.Models;
using SP.Web.Components.Account;
using SP.Data;
using SP.Data.Account.Tools;
using SP.Web.Infrastructure.ControllerStructure.Account;
using SP.Data.Account.Models;

namespace SP.Web.Areas.Account.Controllers
{
    public class LedgersController : Controller
    {
        //Object
        private SP_DataEntities db = new SP_DataEntities();
        LedgerComponent ledgerComponent = new LedgerComponent();

        public async Task<ActionResult> Index()
        {
            var ledgers = db.Ledgers.Include(l => l.Ledger2);
            return View(await ledgers.ToListAsync());
        }


        #region ========Ledger========
        #region Add
      
        public async Task<ActionResult> LedgerAddEdit(Ledger ledger, Guid? LedgerId)
        {
            //create
            ViewBag.Layout = null;

            //For Edit
            if (LedgerId.ISValidObject() && LedgerId != default(Guid))
            {
                ViewBag.Layout = LedgerIStruct.LedgerAddEdit.Layout;
                ledger = await db.Ledgers.FindAsync(LedgerId);
                if (ledger == null)
                {
                    return HttpNotFound();
                }
            }

            //Fill Drop DOwn List
            #region === DropDownList==
            ViewBag.Category = ledgerComponent.CmbLedgerCategory();
            ViewBag.Type = ledgerComponent.CmbLedgerType();
         
            Task.Run(async () =>
            {
                 ViewBag.ParentId = await ledgerComponent.CmbLedgerByCategoryAsync(LedgerCategories.FUND);
            }).Wait();
            #endregion


            //Return
            return PartialView(ledger);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LedgerAddEdit([Bind(Include = "Id,LedgerId,LedgerName,Category,Type,ParentId,IsFixed,IsFees,IsAudit,IsLab,Date")] Ledger ledger)
        {
            if (ledger.LedgerId.ISValidObject()&& ledger.LedgerId!=default(Guid))
            {
                if (ModelState.IsValid)
                {
                    ledger.Date = DateTime.Now;
                    db.Entry(ledger).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            else
            {
                if (ModelState.IsValid)
                {
                    ledger.LedgerId = Guid.NewGuid();
                    ledger.Date = DateTime.Now;

                    db.Ledgers.Add(ledger);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }


            //Fill Drop DOwn List
            string parentId = ledger.ParentId.ConvertObjectToString();
            ViewBag.Category = ledgerComponent.CmbLedgerCategory(ledger.Category);
            ViewBag.Type = ledgerComponent.CmbLedgerType(ledger.Type);
            Task.Run(async () =>
            {
                ViewBag.ParentId = await ledgerComponent.CmbLedgerByCategoryAsync(parentId);
            }).Wait();
            return PartialView(ledger);
        }

        #endregion

        #endregion

        #region ========== LedgersOpeningBalances=======

        #region Add
        public ActionResult LedgersOpeningBalancesCreate()
        {
            ViewBag.LedgerID = new SelectList(db.Ledgers, "LedgerId", "LedgerName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LedgersOpeningBalancesCreate([Bind(Include = "Id,LedgerID,OpeningBalance,Date,Type")] LedgersOpeningBalance ledgersOpeningBalance)
        {
            if (ModelState.IsValid)
            {
                db.LedgersOpeningBalances.Add(ledgersOpeningBalance);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.LedgerID = new SelectList(db.Ledgers, "LedgerId", "LedgerName", ledgersOpeningBalance.LedgerID);
            return View(ledgersOpeningBalance);
        }

        #endregion

        #region Edit
        public async Task<ActionResult> LedgersOpeningBalancesEdit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LedgersOpeningBalance ledgersOpeningBalance = await db.LedgersOpeningBalances.FindAsync(id);
            if (ledgersOpeningBalance == null)
            {
                return HttpNotFound();
            }
            ViewBag.LedgerID = new SelectList(db.Ledgers, "LedgerId", "LedgerName", ledgersOpeningBalance.LedgerID);
            return View(ledgersOpeningBalance);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LedgersOpeningBalancesEdit([Bind(Include = "Id,LedgerID,OpeningBalance,Date,Type")] LedgersOpeningBalance ledgersOpeningBalance)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ledgersOpeningBalance).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.LedgerID = new SelectList(db.Ledgers, "LedgerId", "LedgerName", ledgersOpeningBalance.LedgerID);
            return View(ledgersOpeningBalance);
        }

        #endregion

        public async Task<ActionResult> LedgersOpeningBalancesDetails(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LedgersOpeningBalance ledgersOpeningBalance = await db.LedgersOpeningBalances.FindAsync(id);
            if (ledgersOpeningBalance == null)
            {
                return HttpNotFound();
            }
            return View(ledgersOpeningBalance);
        }

        #endregion



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
