﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SP.Data.Models;
using SP.Data.session;
using SP.Data.Academic.Tools;
using SP.Data;
using SP.Web.Components.Academic;

namespace SP.Web.Areas.Academic.Controllers
{
    public class SectionsController : Controller
    {
        #region object
        private SP_DataEntities db = new SP_DataEntities();
        private SessionUtils sessionUtils = new SessionUtils();
        SectionTools sectionTools = new SectionTools();
        ClassComponent classComponent = new ClassComponent();
        #endregion

        #region View
        [ChildActionOnly]
        public async Task<ActionResult> SectionView()
        {
            Task.Run(async () =>
            {
                var data = await classComponent.CmbClassAsync(sessionUtils._gSessionIdVToX, sessionUtils._gSessionIdXIToXII);
                ViewBag.FilterClassID = data;
            }).Wait();
            return PartialView("SectionView");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SectionView(long? FilterClassID)
        {
            List<Section> lstSec = new List<Section>();
            if (FilterClassID.ISValidObject())
            {
                lstSec = (await sectionTools.GetSectionsAsync(FilterClassID)).ToList();
            }
            //Clsss Id
            var clsId = FilterClassID.ISValidObject() ? FilterClassID.ConvertObjectToString() : "";

            ViewBag.FilterClassID = await classComponent.CmbClassAsync(sessionUtils._gSessionIdVToX, sessionUtils._gSessionIdXIToXII, clsId);
            return PartialView(lstSec);
        }

        #endregion

        #region CreatedndEdit
        // GET: Classes/Classes/CreateAndEdit
        public async Task<ActionResult> CreateAndEdit(long? ID)
        {
            var sec = await sectionTools.GetSectionAsync(ID);
            //Clsss Id
            var clsId = sec.ISValidObject() ? sec.ClassID.ConvertObjectToString() : "";

            ViewBag.ClassId = await classComponent.CmbClassAsync(sessionUtils._gSessionIdVToX, sessionUtils._gSessionIdXIToXII, clsId);
            return View(sec);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAndEdit([Bind(Include = "ID,ClassID,SectionName")] Section sec)
        {

            if (sec.ID.ISValidObject() && sec.ID > 0)
            {

                if (await sectionTools.IsValidSectionAsync(sec.ClassID, sec.SectionName, sec.ID))
                {
                    ModelState.AddModelError("", "Same Section is  already holding");
                }
                if (ModelState.IsValid)
                {
                    db.Entry(sec).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }

            }
            else
            {
                //Avoid Model State Validation
                ModelState.Remove("ID");
                if (await sectionTools.IsValidSectionAsync(sec.ClassID, sec.SectionName))
                {
                    ModelState.AddModelError("", "Same Section is  already exist");
                }
                if (ModelState.IsValid)
                {
                    Section _class = new Section()
                    {
                        ClassID = sec.ClassID,
                        SectionName = sec.SectionName,
                    };

                    db.Sections.Add(sec);
                    await db.SaveChangesAsync();
                }
            }

            return RedirectToAction("RedirectPath");
        }

        #endregion

        public ActionResult RedirectPath()
        {
            return RedirectToAction("CreateAndEdit");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
