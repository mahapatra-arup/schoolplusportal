﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SP.Data.Models;
using SP.Data.Academic;
using SP.Data.session;
using SP.Data;
using SP.Data.Academic.Tools;
using SP.Data.Models.others;
using SP.Data.Academic.Models;

namespace SP.Web.Areas.Academic.Controllers
{
    public class ClassesController : Controller
    {
        #region object
        private SP_DataEntities db = new SP_DataEntities();
        private SessionUtils sessionUtils = new SessionUtils();
        ClassTools classTools = new ClassTools();
        ClassCategoryTools classCategoryTools = new ClassCategoryTools();
        #endregion

        public ActionResult Index()
        {
            return RedirectToAction("CreateAndEdit", "Classes");
        }

            // GET: Classes/Classes/Create

            public async Task<ActionResult> CreateAndEdit(long? Id)
        {
            var cls =await classTools.GetClassAsync(Id);
            var catId = cls.ISValidObject() ? cls.CategoryID.ConvertObjectToString() : "";
            ViewBag.CategoryID = new SelectList(db.Class_Category, "ID", "CategoryName", catId);
            return View(cls);
        }

      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAndEdit([Bind(Include = "Id,SlNo,SessionId,ClassName,CategoryID")] Class cls)
        {
            //Class Category
            var cls_category = await classCategoryTools.GetClassCategoryAsync(cls.CategoryID);
           
            //sessionId
            int sessionId = cls_category.CategoryName== ClassCategories.Higher_Secondary ? sessionUtils._gSessionIdXIToXII : sessionUtils._gSessionIdVToX;

            if (cls.Id.ISValidObject() && cls.Id > 0)
            {
                if (await classTools.IsValidClassAsync(sessionId, cls.ClassName, cls.Id))
                {
                    ModelState.AddModelError("","Same Class is  already holding");
                }
                if (ModelState.IsValid)
                {
                    db.Entry(cls).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }

            }
            else
            {
                //Avoid Model State Validation
                ModelState.Remove("Id");
                ModelState.Remove("SlNo");
                ModelState.Remove("SessionId");
                if (await classTools.IsValidClassAsync(sessionId,cls.ClassName))
                {
                    ModelState.AddModelError("", "Same Class is  already exist");
                }
                if (ModelState.IsValid)
                {
                    //Get SlNo
                    int slno =(await classTools.GetClassAsync(sessionUtils._gSessionIdVToX, sessionUtils._gSessionIdXIToXII)).Max(s=>s.SlNo)+1;
                    Class _class = new Class()
                    {
                        Id = cls.Id,
                        CategoryID = cls.CategoryID,
                        ClassName = cls.ClassName,
                        SlNo = slno,
                        SessionId=sessionId
                       
                    };

                    db.Classes.Add(_class);
                    await db.SaveChangesAsync();
                }
            }

            return RedirectToAction("Index");
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
