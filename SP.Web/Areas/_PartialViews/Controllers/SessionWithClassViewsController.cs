﻿using SP.Data;
using SP.Data.Enums;
using SP.Data.Models;
using SP.Data.session;
using SP.Web.Areas.Student.Models;
using SP.Web.Components.Academic;
using SP.Web.Components.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SP.Web.Areas._PartialViews.Controllers
{
    public class SessionWithClassViewsController : Controller
    {

        #region Objects
        SessionUtils sessionUtils = new SessionUtils();
        ClassComponent classComponent = new ClassComponent();
        SectionComponent _sectionComponent = new SectionComponent();
        StdAdmissionHttpSessionModels _stdAdmissionHttpSessionModels = new StdAdmissionHttpSessionModels();
        #endregion

        // GET: _PartialViews/SessionWithClassViews
        [AcceptVerbs(HttpVerbs.Get)]
        public async Task<ActionResult> SessionWithClassAndSecView(SessionEnum._SessionType? sessionType, int? SessionID, long? ClassID, long? SectionID)
        {
            Task.Run(async () =>
            {
                if (sessionType == SessionEnum._SessionType.XIToXII)
                {
                    //Session
                    ViewBag.Session = new SelectList(await sessionUtils.GetNexSessionsAsync(),
                            FieldPropertiesExtension.GetMemberName((SP.Data.Models.Session c) => c.ID),
                            FieldPropertiesExtension.GetMemberName((SP.Data.Models.Session c) => c.XIToXII));//HS


                    //Class

                    ViewBag.Class = await classComponent.CmbHsClassAsync(SessionID.ConvertObjectToInt());


                }
                else
                {
                    //Session
                    ViewBag.Session = new SelectList(await sessionUtils.GetNexSessionsAsync(),
                            FieldPropertiesExtension.GetMemberName((SP.Data.Models.Session c) => c.ID),
                            FieldPropertiesExtension.GetMemberName((SP.Data.Models.Session c) => c.VToX));//Mp


                    //Class

                    ViewBag.Class = await classComponent.CmbMpClassAsync(SessionID.ConvertObjectToInt());//Mp
                }
                //Section
                ViewBag.Section = await _sectionComponent.CmbSectionAsync(ClassID.ConvertObjectToInt());
            }).Wait();

            //View Bag
            Std_Register std_Register = new Std_Register()
            {
                SessionID = SessionID.ConvertObjectToInt(),
                ClassID = ClassID.ConvertObjectToLong(),
                SectionID = SectionID.ConvertObjectToLong()
            };
            //Session Storage  Create Store data
            HttpContextSessionTools.CreateHttpContextSession<Std_Register>(_stdAdmissionHttpSessionModels._sStdRegister, std_Register);

            // return ;
            return PartialView();
        }

        [ActionName("SessionWithClassAndSecView")]
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> SessionWithClassAndSecView1(SessionEnum._SessionType? sessionType, int? SessionID, long? ClassID, long? SectionID)
        {
            if (sessionType == SessionEnum._SessionType.XIToXII)
            {
                //Session
                ViewBag.Session = new SelectList(await sessionUtils.GetNexSessionsAsync(),
                        FieldPropertiesExtension.GetMemberName((SP.Data.Models.Session c) => c.ID),
                        FieldPropertiesExtension.GetMemberName((SP.Data.Models.Session c) => c.XIToXII));//HS


                //Class
                ViewBag.Class = await classComponent.CmbHsClassAsync(SessionID.ConvertObjectToInt()); //HS
            }
            else
            {
                //Session
                ViewBag.Session = new SelectList(await sessionUtils.GetNexSessionsAsync(),
                        FieldPropertiesExtension.GetMemberName((SP.Data.Models.Session c) => c.ID),
                        FieldPropertiesExtension.GetMemberName((SP.Data.Models.Session c) => c.VToX));//Mp


                //Class
                ViewBag.Class = await classComponent.CmbMpClassAsync(SessionID.ConvertObjectToInt());//Mp
            }
            //Section
            ViewBag.Section = await _sectionComponent.CmbSectionAsync(ClassID.ConvertObjectToInt());

            //View Bag
            Std_Register std_Register = new Std_Register()
            {
                SessionID = SessionID.ConvertObjectToInt(),
                ClassID = ClassID.ConvertObjectToLong(),
                SectionID = SectionID.ConvertObjectToLong()
            };
            //Session Storage  Create Store data
            HttpContextSessionTools.CreateHttpContextSession<Std_Register>(_stdAdmissionHttpSessionModels._sStdRegister, std_Register);
            /*Json(std_Register,JsonRequestBehavior.AllowGet)*/
            // return ;
            return PartialView(std_Register);
        }
    }
}