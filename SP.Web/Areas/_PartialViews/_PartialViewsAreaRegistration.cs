﻿using System.Web.Mvc;

namespace SP.Web.Areas._PartialViews
{
    public class _PartialViewsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "_PartialViews";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "_PartialViews_default",
                "_PartialViews/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}