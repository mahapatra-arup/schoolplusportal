﻿using SP.Data;
using SP.Data.Account.Models;
using SP.Data.Account.Tools;
using SP.Data.Fees;
using SP.Data.Fees.ControllersTools;
using SP.Data.Fees.Models;
using SP.Data.Models;
using SP.Data.Others.Tools;
using SP.Data.session;
using SP.Data.Student;
using SP.Web.Areas.Fees.Models;
using SP.Web.Components.Academic;
using SP.Web.Components.Account;
using SP.Web.Components.Banks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using static SP.Data.Enums.StudentsFeesEnum;

namespace SP.Web.Areas.Fees.Controllers
{
    public class FeesCollectController : Controller
    {
        #region obj with constructer
        private SP_DataEntities db = new SP_DataEntities();
        private StudentTools studentTools = new StudentTools();
        FeesCollectionUtils feesCollectionUtils = new FeesCollectionUtils();
        FeesPaymentDetailsTools feesPaymentDetailsTools = new FeesPaymentDetailsTools();
        ClassComponent classComponent = new ClassComponent();
        SessionUtils sessionUtils = new SessionUtils();
        FeesConfigUtils feesConfigUtils = new FeesConfigUtils();
        MonthTools monthTools = new MonthTools();
        LedgerComponent ledgerComponent = new LedgerComponent();

      
        #endregion

        #region View
        public ActionResult Index(string type)
        {
            ViewBag.type = type;
            return View();
        }


        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        //[ChildActionOnly]
        public async Task<ActionResult> PartialIndex(string type, long? FilterClassID)
        {
            //var obj
            IEnumerable<StudentFeesListVM> feesPaymentDetailsSubs = null;
            ViewBag.type = type;

            #region -=List Check=-
            var _studentFeesListEnum = _StudentFeesListEnum.DUE_LIST;
            switch (type)
            {
                case "DUE":
                    _studentFeesListEnum = _StudentFeesListEnum.DUE_LIST;
                    break;
                case "PAID":
                    _studentFeesListEnum = _StudentFeesListEnum.PAID_LIST;
                    break;
                case "ALL":
                    _studentFeesListEnum = _StudentFeesListEnum.ALL;
                    break;
                default:
                    _studentFeesListEnum = _StudentFeesListEnum.DUE_LIST;
                    break;
            }
            #endregion

            Task.Run(async () =>
         {
             feesPaymentDetailsSubs = await feesCollectionUtils.GetDueStudentFeesList(FilterClassID.ConvertObjectToInt(), _studentFeesListEnum);

             var data = await classComponent.CmbClassAsync(sessionUtils._gSessionIdVToX, sessionUtils._gSessionIdXIToXII);
             ViewBag.FilterClassID = data;
         }).Wait();
            return PartialView(feesPaymentDetailsSubs);
        }
        #endregion

        public async Task<ActionResult> FeesCollection(Guid? _BillId)
        {

            var isMonthly = (await feesConfigUtils.GetFeesConfig()).IsMonthly;
            FeesCollectionVM feesCollectionVM = null;

            if (_BillId == null || _BillId.Value == default(Guid))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           
                //Registe No
                var fb = await db.FeesBills.Where(b => b.BillId == _BillId).FirstOrDefaultAsync();
                string registerNo = fb != null ? fb.RegisterNo : "";

                //Due Fees List
                 feesCollectionVM = await GetDueFeesDetails(registerNo, _BillId.ConvertToGuid(), new List<string>());
           

            #region Viewbag
            ViewBag.PaymentByBankID =  await ledgerComponent.CmbLedgerByCategoryAsync(LedgerCategories.BANK);
            ViewBag.IsMonthly = isMonthly;

            //Fees Transection Mode
            var a = TransectionMode._TransectionMode.CASH;
            ViewBag.Status = a.ConvertTo_SelectList();
            #endregion

            return PartialView(feesCollectionVM);//Previous Index View Set
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> FeesCollection(FeesCollectionVM feesCollectionVM, string btnPay, string btnMonthChange)
        {
            #region Var
            var isMonthly = (await feesConfigUtils.GetFeesConfig()).IsMonthly;
           var _BillId = feesCollectionVM._FeesPaymentDetail.BillId;
            var registerNo = feesCollectionVM._StudentListView.RegisterNo;
            #endregion


            #region Payment
            if (!btnPay.ISNullOrWhiteSpace())
            {
                if (ModelState.IsValid)
                {
                    if (feesCollectionVM != null && feesCollectionVM._FeesPaymentDetail.ISValidObject())
                    {
                        var resultCollection = await SetFeesCollectDetails(feesCollectionVM);
                        await feesCollectionUtils.FeesCollection(resultCollection.Item1, resultCollection.Item2, resultCollection.Item3, resultCollection.Item4);

                        return RedirectToAction("Index");
                    }

                }
            }

            #endregion

            #region MONTHLY_DUE_LIST
            if (isMonthly && !btnMonthChange.ISNullOrWhiteSpace())
            {
                var months_result = feesCollectionVM._Months.Where(m => m.IsSelected).ToList();//check Month
                if (months_result.IsValidList() && _BillId != null && _BillId.Value != default(Guid))
                {
                    var _selected_months = new List<string>();
                    months_result.ForEach(p =>
                    _selected_months.Add(p.MonthName)
                    );

                    //Due Fees List
                    var due_Data = await GetDueFeesDetails(registerNo, _BillId.ConvertToGuid(), _selected_months);

                    #region Fill Details
                    //Only fill CompalsaryFees && Custom Fees
                    feesCollectionVM._FeesPaymentDetailVMForCompulsory = due_Data._FeesPaymentDetailVMForCompulsory;
                    feesCollectionVM._FeesPaymentDetailVMForCustom = due_Data._FeesPaymentDetailVMForCustom;

                    //Other Details
                    feesCollectionVM._StudentListView = due_Data._StudentListView;
                    #endregion

                }
                else
                {
                    //not Valid Month or Bill Id
                }
            }
            #endregion

            #region Viewbag
            //Months
            ViewBag.IsMonthly = isMonthly;


            //Fees Transection Mode
            var selectedTransMode = feesCollectionVM._FeesPaymentDetail.Status.ISValidObject() ? feesCollectionVM._FeesPaymentDetail.Status : "";
           // var Cash_Bank = TransectionMode._TransectionMode.CASH;
            ViewBag.Status = default(TransectionMode._TransectionMode).ConvertTo_SelectList(selectedTransMode);

            //PaymentBank
            var selectedlpaymentbankId = feesCollectionVM._FeesPaymentDetail.PaymentByBankID.ISValidObject() ? feesCollectionVM._FeesPaymentDetail.PaymentByBankID.ToString() : "";
            ViewBag.PaymentByBankID = await ledgerComponent.CmbLedgerByCategoryAsync(LedgerCategories.BANK, selectedlpaymentbankId);
           
            #endregion

            return PartialView(feesCollectionVM);
        }

        #region NONACTION WEBMETHOD
        //Savetime
        [NonAction]
        public async Task<GetSetValues<FeesPaymentDetail, List<FeesPaymentDetailsSub>, List<FeesBillDetail>, List<Month>>> SetFeesCollectDetails(FeesCollectionVM feesCollectionVM)
        {
            GetSetValues<FeesPaymentDetail, List<FeesPaymentDetailsSub>, List<FeesBillDetail>, List<Month>> getSetValues = new GetSetValues<FeesPaymentDetail, List<FeesPaymentDetailsSub>, List<FeesBillDetail>, List<Month>>();

            //
            #region --Object--
            FeesPaymentDetail feesPaymentDetail = new FeesPaymentDetail();
            List<FeesPaymentDetailsSub> lstfeesPaymentDetailsSub = new List<FeesPaymentDetailsSub>();
            List<FeesBillDetail> lstfeesBillDetails = new List<FeesBillDetail>();
            List<Month> lstMonths = new List<Month>();
            #endregion

            #region Add Into oNe List

            List<FeesPaymentDetailVM> _CimpalsuryandCustomFees = new List<FeesPaymentDetailVM>();

            if (feesCollectionVM._FeesPaymentDetailVMForCompulsory.IsValidList())
            {
                _CimpalsuryandCustomFees.AddRange(feesCollectionVM._FeesPaymentDetailVMForCompulsory);
            }
            if (feesCollectionVM._FeesPaymentDetailVMForCustom.IsValidList())
            {
                _CimpalsuryandCustomFees.AddRange(feesCollectionVM._FeesPaymentDetailVMForCustom);
            }
            #endregion

            //
            #region -=If Monthsly Fees Then Work=-
            var isMonthly = (await feesConfigUtils.GetFeesConfig()).IsMonthly;
            string strMonths = string.Empty;
            if (isMonthly && feesCollectionVM._Months.IsValidList())
            {
                //Selected Month
                var mont = feesCollectionVM._Months.Where(s => s.IsSelected = true).ToList();
                mont.ForEach(s =>
                lstMonths.Add(new Month
                {
                    MonthName = s.MonthName
                })
                );


                if (mont.IsValidList())
                {
                    var allMonths = mont.Select(s => s.MonthName).ToArray();
                    //Fill
                    strMonths = string.Join(",", allMonths);
                }
            }
            #endregion

            //
            #region FeesPaymentDetail
            #region Bank Details
            if (feesCollectionVM._FeesPaymentDetail.Status == TransectionMode._TransectionMode.CASH.ConvertEnumFieldToString())
            {
                feesCollectionVM._FeesPaymentDetail.ChequeDate = null;
                feesCollectionVM._FeesPaymentDetail.ChequeNo = string.Empty;
                feesCollectionVM._FeesPaymentDetail.PaymentByBankID = null;
            }
            #endregion

            Guid _PaymentId = Guid.NewGuid();
            //
            feesPaymentDetail = feesCollectionVM._FeesPaymentDetail;
            //fill
            feesPaymentDetail.PaymentId = _PaymentId;
            feesPaymentDetail.Amount = _CimpalsuryandCustomFees.Sum(s => s.PaidFees); //Total Amount
            feesPaymentDetail.PaymentMonth = strMonths;//If Its Monthly fees 


            #endregion

            //
            #region FeesPaymentDetailsSub 
            if (_CimpalsuryandCustomFees.IsValidList())
            {
                _CimpalsuryandCustomFees.ForEach(p =>
                   lstfeesPaymentDetailsSub.Add(new FeesPaymentDetailsSub
                   {
                       paymentId = _PaymentId,
                       FeesAmountId = p.FeesAmountId,
                       Amount = p.PaidFees,
                       Concession = p.Concession
                   })
                   );

            }
            #endregion

            //
            #region FeesBill Details -
            if (_CimpalsuryandCustomFees.IsValidList())
            {
                _CimpalsuryandCustomFees.ForEach(p =>
                   lstfeesBillDetails.Add(new FeesBillDetail
                   {
                       PaidFees = p.PaidFees,
                       DueFees = p.DueFees,
                       Concession = p.Concession,

                       FeesAmountId = p.FeesAmountId,
                       BillId = feesPaymentDetail.BillId
                   })
                   );

            }
            #endregion

            //Fill Value
            getSetValues.Item1 = feesPaymentDetail;
            getSetValues.Item2 = lstfeesPaymentDetailsSub;
            getSetValues.Item3 = lstfeesBillDetails;
            getSetValues.Item4 = lstMonths;//

            //Return
            return getSetValues;
        }

        //Show data
        [NonAction]
        public async Task<FeesCollectionVM> GetDueFeesDetails(string registerNo, Guid _BillId, List<string> lstMonth)
        {
            var isMonthly = (await feesConfigUtils.GetFeesConfig()).IsMonthly;
            var studentDetails = await studentTools.GetStudentListViewAsync(registerNo);
            var _ClassId = studentDetails.Std_Register_ClassID;
            FeesCollectionVM feesCollectionVM = new FeesCollectionVM();


            //StudentDetails
            feesCollectionVM._StudentListView = studentDetails;

            //_FeesPaymentDetail
            var billNo = (await feesPaymentDetailsTools.MaxFeesBillNo()) + 1;
            feesCollectionVM._FeesPaymentDetail = new FeesPaymentDetail
            {
                Date = DateTime.Now,
                BillNo = billNo,
                BillId = _BillId
            };


            //Compulsary Due Fees
            #region -==Compalsory==- 

            var compalsaryDueFees = await feesCollectionUtils.GetDueFees(_BillId, lstMonth, CustomeAndFineFees.WithOutCustomAndIsFine);
            List<FeesPaymentDetailVM> fedComplasory = new List<FeesPaymentDetailVM>();
            if (compalsaryDueFees.IsValidList())
            {
                compalsaryDueFees.ForEach(p =>
                   fedComplasory.Add(new FeesPaymentDetailVM
                   {
                       LedgerId = p.LedgerId,
                       FeesAmountId = p.FeesAmountID,
                       LedgerName = p.LedgerName,
                       Fees = p.FeesBillDetails_Fees,

                       PaidFees = p.DueFees,
                       PreviousDueFees = p.DueFees,
                       DueFees = 0,

                       Concession = p.Concession
                   })
                   );
            }
            feesCollectionVM._FeesPaymentDetailVMForCompulsory = fedComplasory;
            #endregion

            #region -==Custom==-
            List<FeesPaymentDetailVM> fedCustom = new List<FeesPaymentDetailVM>();
            var customDueFees = await feesCollectionUtils.GetDueFees(_BillId, lstMonth, CustomeAndFineFees.WithCustom_OR_IsFine);

            if (customDueFees.IsValidList())
            {
                customDueFees.ForEach(p =>
            fedCustom.Add(new FeesPaymentDetailVM
            {
                LedgerId = p.LedgerId,
                FeesAmountId = p.FeesAmountID,
                LedgerName = p.LedgerName,
                Fees = p.FeesBillDetails_Fees,

                PaidFees = p.DueFees,
                PreviousDueFees = p.DueFees,
                DueFees = 0,

                Concession = p.Concession

            })
            );
            }
            feesCollectionVM._FeesPaymentDetailVMForCustom = fedCustom;
            #endregion


            #region -=Month=-
            //Fill Month
            if (isMonthly)
            {
                List<FeesMonthsVM> feesMonthsVMs = new List<FeesMonthsVM>();
                var _months = await monthTools.GetMonthsAsync(_ClassId);//get
                if (_months.IsValidList())
                {
                    _months.ForEach(m =>
                             feesMonthsVMs.Add(new FeesMonthsVM
                             {
                                 IsSelected = false,
                                 MonthName = m.MonthName
                             })
                              );
                }
                feesCollectionVM._Months = feesMonthsVMs;
            }
            #endregion

            return feesCollectionVM;
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
