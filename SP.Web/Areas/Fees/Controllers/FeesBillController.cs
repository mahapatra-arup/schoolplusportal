﻿using SP.Data;
using SP.Data.Fees;
using SP.Data.Fees.ControllersTools;
using SP.Data.Fees.lib;
using SP.Data.Fees.Models;
using SP.Data.Models;
using SP.Data.Others.Tools;
using SP.Data.Student;
using SP.Web.Areas.Fees.Models;
using SP.Web.Components.Account;
using SP.Web.Infrastructure.ControllerStructure.Fees;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using static SP.Data.Enums.StudentsEnum;
using static SP.Data.Enums.StudentsFeesEnum;

namespace SP.Web.Areas.Fees.Controllers
{
    public class FeesBillController : Controller
    {
        #region Object
        private SP_DataEntities db = new SP_DataEntities();
        FeesBiilGenerateUtils feesBiilGenerateUtils = new FeesBiilGenerateUtils();
        LedgerComponent ledgerComponent = new LedgerComponent();
        StudentTools studentTools = new StudentTools();
        MonthTools monthTools = new MonthTools();
        FeesConfigUtils feesConfigUtils = new FeesConfigUtils();
        FeesStructureUtils feesStructureUtils = new FeesStructureUtils();
        FeesBillTools feesBillTools = new FeesBillTools();
        #endregion

        #region Bill Generate
        // GET: Fees/Index
        public async Task<ActionResult> Index()
        {
            var dueBillStudentList = await feesBiilGenerateUtils.DueBillStudentList();
            List<FeesBillVM> feesBillVM = dueBillStudentList.AsEnumerable().
                         Select(a => new FeesBillVM()
                         {
                             IsBillGenerate = false,

                             ClassId = a.Std_Register_ClassID,
                             StudentId = a.StudentID,
                             RegisterNo = a.RegisterNo,
                             StudentName = a.StudentName,
                             ClassName = a.ClassName,
                             SectionName = a.SectionName,
                             RollNo = a.RollNo
                         }).ToList();

            return View(feesBillVM);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(List<FeesBillVM> feesBillVMs)
        {
            if (ModelState.IsValid)
            {
                var data = feesBillVMs.Where(s => s.IsBillGenerate == true).ToList();
                if (data.IsValidList())
                {
                    foreach (var item in data)
                    {
                        await feesBiilGenerateUtils.GenerateFeesBill(item.RegisterNo, item.ClassId, _AdmissionType.Admission);
                    }
                    return RedirectToAction("Index");
                }
            }
            return View(feesBillVMs);
        }
        #endregion

        #region Cusstom Bill Generate
        // GET: Fees/FeesBillDetails
        public async Task<ActionResult> CustomFeesBill(string RegNo)
        {
            if (RegNo == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid RegisteNo Parameter");
            }

            var std_bill_details = await db.FeesBills.Where(s => s.RegisterNo == RegNo).FirstOrDefaultAsync();
            if (std_bill_details == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid Bill Details");
            }
            //obj with var
            #region obj
            CustomFeesBillVM customFeesBillVM = new CustomFeesBillVM();
            var _IsMonthly = (await feesConfigUtils.GetFeesConfig()).IsMonthly;
            var student_Data = (await studentTools.GetStudentListViewAsync(RegNo));
            var isRegMoreTime = await studentTools.IsRegNoMoreTime(student_Data.StudentID);

            #endregion

            //FeesBill
            customFeesBillVM._FeesBillDetails = new FeesBillDetail { BillId = std_bill_details.BillId };

            //Student Fill
            customFeesBillVM._StudentListView = student_Data;

            #region Fill CMB
            if (isRegMoreTime)
            {
                ViewBag.LedgerId = await ledgerComponent.CmbLedgerByClassIdsync(student_Data.Std_Register_ClassID, _AdmissionType.Readmission, CustomeAndFineFees.WithCustom_OR_IsFine);
            }
            else
            {
                ViewBag.LedgerId = await ledgerComponent.CmbLedgerByClassIdsync(student_Data.Std_Register_ClassID, _AdmissionType.Admission, CustomeAndFineFees.WithCustom_OR_IsFine);
            }
            #endregion

            //Monthly sets
            ViewBag.IsMonthly = _IsMonthly;

            //MonthFIll
            if (_IsMonthly)
            {
                List<FeesMonthsVM> lstMonths = new List<FeesMonthsVM>();
                var mnth = await monthTools.GetMonthsAsync(std_bill_details.BillId, student_Data.Std_Register_ClassID);
                mnth.ForEach(s =>
                   lstMonths.Add(new FeesMonthsVM
                   {
                       MonthName = s.MonthName,
                       IsSelected = false

                   }));
                customFeesBillVM._Months = lstMonths;
            }

            return PartialView(customFeesBillVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CustomFeesBill(CustomFeesBillVM customFeesBillVM)
        {

            if (ModelState.IsValid)
            {
                var result_data = await SetCustomFeesBill(customFeesBillVM);
                if (result_data.IsValidList())
                {
                    await feesBiilGenerateUtils.CustomBillGenerate(result_data);
                    return RedirectToAction(FeesIStruct.FeesCollection.ActionName, FeesIStruct.FeesCollection.ControllerName, new { _BillId = customFeesBillVM._FeesBillDetails.BillId });
                }
            }

            return RedirectToAction("CustomFeesBill", customFeesBillVM._StudentListView.RegisterNo);
        }
        #endregion

        #region NO-ACTION
        [NonAction]
        public async Task<List<FeesBillDetail>> SetCustomFeesBill(CustomFeesBillVM customFeesBillVM)
        {
            List<FeesBillDetail> feesBillDetails = new List<FeesBillDetail>();
            var isMonthly = (await feesConfigUtils.GetFeesConfig()).IsMonthly;

            //Fees Amount
            var feesStructure_Data = (await feesStructureUtils.GetFeesStructureView(customFeesBillVM._StudentListView.Std_Register_ClassID, customFeesBillVM.LedgerId.ConvertToGuid()));
            var feesAmount = feesStructure_Data.Fees;

            if (isMonthly)
            {
                #region is Months
                var months_result = customFeesBillVM._Months.Where(m => m.IsSelected).ToList();//check Month
                if (!months_result.IsValidList())
                { return feesBillDetails; }//return

                foreach (var month in months_result)
                {
                    var strMonth = month.MonthName;

                    feesBillDetails.Add(new FeesBillDetail
                    {
                        FeesAmountId = feesStructure_Data.FeesAmountID,
                        BillId = customFeesBillVM._FeesBillDetails.BillId,
                        BillType = FeesBillTypeModel.MONTHLY,
                        Month = strMonth,

                        //Amount
                        Fees = feesAmount,
                        DueFees = feesAmount,
                        PaidFees = 0,
                        Concession = 0
                    });

                } 
                #endregion
            }
            else
            {
                feesBillDetails.Add(new FeesBillDetail
                {
                    FeesAmountId = feesStructure_Data.FeesAmountID,
                    BillId = customFeesBillVM._FeesBillDetails.BillId,

                    //Amount
                    Fees = feesAmount,
                    DueFees = feesAmount,
                    PaidFees = 0,
                    Concession = 0
                });
            }
            return feesBillDetails;
        }
        #endregion

        [HttpGet]
        [AllowAnonymous]
        public async Task<double> GetFeesAmount(long ClassId, string LedgerId)
        {
            var data = await feesStructureUtils.GetFeesStructureView(ClassId, LedgerId.ConvertToGuid());
            return data.ISValidObject() ? data.Fees : 0;
        }


        #region StudentFeesDetails
        // GET: Fees/FeesBillDetails
        public async Task<ActionResult> StudentFeesDetails(string RegisterNo)
        {
            var isMonthly = (await feesConfigUtils.GetFeesConfig()).IsMonthly;
            ViewBag.IsMonthly = isMonthly;

            var dueBillList = await feesBillTools.GetFeesBillView(RegisterNo);
            return PartialView(dueBillList);
        }

        #endregion
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
