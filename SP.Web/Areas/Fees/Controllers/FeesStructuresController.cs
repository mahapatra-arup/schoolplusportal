﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SP.Data.Models;
using SP.Web.Areas.Fees.Models;
using System.Data.Entity.Validation;
using SP.Data;
using SP.Data.session;
using SP.Data.Academic;
using System.Web.Services;
using SP.Data.Account.Tools;
using SP.Data.Models.others;
using SP.Web.Components.Account;
using SP.Data.Account.Models;
using SP.Web.Components.Subject;
using SP.Data.Fees;
using SP.Data.Academic.Models;
using SP.Web.Components.Academic;

namespace SP.Web.Areas.Fees.Controllers
{
    public class FeesStructuresController : Controller
    {
        //Obj
        #region obj
        private SP_DataEntities db = new SP_DataEntities();
        FeesStructureUtils feesStructureUtils = new FeesStructureUtils();
        SessionUtils sessionUtils = new SessionUtils();
        ClassTools _classTools = new ClassTools();
        LedgerTools ledgerTools = new LedgerTools();
        LedgerComponent ledgerComponent = new LedgerComponent();
        SubjectComponent subjectComponent = new SubjectComponent();
        FeesConfigUtils feesConfigUtils = new FeesConfigUtils();
        ClassComponent classComponent = new ClassComponent();
        #endregion


        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public async Task<ActionResult> Index(long? ClassId)
        {
            var feesStructures =await feesStructureUtils.GetFeesStructureView(ClassId.ConvertObjectToLong());

            //Class
            var selectedClassId = ClassId.ISValidObject() ? ClassId.ToString() : "";
            Task.Run(async () =>
            {
                var data = await classComponent.CmbClassAsync(sessionUtils._gSessionIdVToX, sessionUtils._gSessionIdXIToXII, selectedClassId);
                ViewBag.ClassID = data;
            }).Wait();

            return View(feesStructures);
        }


        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public async Task<ActionResult> Create(Guid? LedgerId,long? SubjectId,string chkLab)
        {
            //var or obj
            #region ..var or obj
            List<FeesStructureVM> feesStructureVM = new List<FeesStructureVM>();
            bool isLabLedger = await ledgerTools.IsLabLedgerAsync(LedgerId.ConvertToGuid());
            var isMonthly = (await feesConfigUtils.GetFeesConfig()).IsMonthly;
            #endregion

            //View bag
            #region ..View bag
            @ViewBag.SubjectHideSHow = "none";
            ViewBag.IsMonthly = isMonthly; 
            #endregion

            if (LedgerId != null && LedgerId != default(Guid))
            {
                if (isLabLedger)
                {
                    @ViewBag.SubjectHideSHow = "block";//design Display:block
                }

                #region Query V-X
                var fa = (from FeesAmount in db.FeesAmounts
                          where
                            FeesAmount.FeesStructure.LedgerId == LedgerId
                          select new
                          {
                              FeesAmount.FeesStructure,
                              FeesAmount.ClassId,
                              FeesAmount.Class,

                              Fees = (double?)FeesAmount.Fees ?? 0,
                              FeesAmount.FeesAmountID,
                          });

                var FeesStructdata = (from cls in db.Classes
                                      join fs in fa
                                    on cls.Id equals fs.ClassId into fs_join
                                      from fs in fs_join.DefaultIfEmpty()

                                          //oreder by
                                      orderby cls.SlNo

                                      //Select
                                      select new
                                      {
                                          Id = (long?)cls.Id??0,
                                          SlNo = cls.SlNo,
                                          SessionId = cls.SessionId,
                                          ClassName = cls.ClassName,
                                          CategoryID = cls.CategoryID,

                                          FeesStructure = fs.FeesStructure,
                                          ClassId = fs.ClassId,
                                          Fees = (double?)fs.Fees ?? 0,
                                          FeesAmountID = fs.FeesAmountID
                                      });
                #endregion

                #region Query XI-XI 
                var hs_Category_str = ClassCategories.Higher_Secondary;

                #region for AllIn One
                var faHs = (from FeesAmount in db.FeesAmounts
                            where
                              FeesAmount.FeesStructure.LedgerId == LedgerId
                            select new
                            {
                                FeesAmount.FeesStructure,
                                FeesAmount.ClassId,
                                FeesAmount.Class,

                                Fees = (double?)FeesAmount.Fees ?? 0,
                                FeesAmount.FeesAmountID,
                            });

                var FeesStructdataHS = (from clsHs in db.Classes
                                            // Class Category
                                        join cls_Category in db.Class_Category on clsHs.CategoryID equals cls_Category.ID

                                        // fess STructure / amount
                                        join fs in faHs on clsHs.Id equals fs.ClassId into fs_join
                                        from fs in fs_join.DefaultIfEmpty()

                                            // where
                                        where cls_Category.CategoryName == hs_Category_str

                                        // oreder by
                                        orderby clsHs.SlNo

                                        // Select
                                        select new
                                        {
                                            Id = clsHs.Id,
                                            SlNo = clsHs.SlNo,
                                            SessionId = clsHs.SessionId,
                                            ClassName = clsHs.ClassName,
                                            CategoryID = clsHs.CategoryID,

                                            FeesStructure = fs.FeesStructure,
                                            ClassId = fs.ClassId,
                                            Fees = (double?)fs.Fees ?? 0,
                                            FeesAmountID = fs.FeesAmountID
                                        });
                #endregion

                #region ForIndivisual
                var faHs_Indivisual = (from FeesAmount in db.FeesAmounts
                                       where
                                         FeesAmount.FeesStructure.LedgerId == LedgerId &&
                                         FeesAmount.FeesStructure.SubjectId == SubjectId
                                       select new
                                       {
                                           FeesAmount.FeesStructure,
                                           FeesAmount.ClassId,
                                           FeesAmount.Class,

                                           Fees = (double?)FeesAmount.Fees ?? 0,
                                           FeesAmount.FeesAmountID,
                                       });

                var FeesStructdataHS_Indivisual = (from cls_Hs in db.Classes
                                                       //Class Category
                                                   join cls_Category in db.Class_Category on cls_Hs.CategoryID equals cls_Category.ID

                                                   //fess STructure /amount
                                                   join fs in faHs_Indivisual on cls_Hs.Id equals fs.ClassId into fs_join
                                                   from fs in fs_join.DefaultIfEmpty()

                                                       //where
                                                   where cls_Category.CategoryName == hs_Category_str

                                                   //oreder by
                                                   orderby cls_Hs.SlNo

                                                   //Select
                                                   select new
                                                   {
                                                       Id = cls_Hs.Id,
                                                       SlNo = cls_Hs.SlNo,
                                                       SessionId = cls_Hs.SessionId,
                                                       ClassName = cls_Hs.ClassName,
                                                       CategoryID = cls_Hs.CategoryID,

                                                       FeesStructure = fs.FeesStructure,
                                                       ClassId = fs.ClassId,
                                                       Fees = fs.Fees ,
                                                       FeesAmountID = fs.FeesAmountID
                                                   });
                #endregion

                #endregion

                var view_Data = !isLabLedger ? FeesStructdata : (chkLab == "Individual" ? FeesStructdataHS_Indivisual : FeesStructdataHS);


                #region Fill Data
                if (view_Data!=null)
                {
                    feesStructureVM = view_Data.Select(data => new FeesStructureVM
                    {
                        //Class
                        ClassId = (long?)data.Id ?? 0,
                        ClassName = data.ClassName,

                        //FeesAmount
                        FeesStructureID = data.FeesStructure.FeesStructureID != null ? data.FeesStructure.FeesStructureID : default(Guid),
                        FeesAmount = data.Fees,

                        //FeesStructer
                        IsAdmission = data.FeesStructure != null ? data.FeesStructure.IsAdmission : false,
                        IsReadmission = data.FeesStructure != null ? data.FeesStructure.IsReadmission : false,
                        IsFine = data.FeesStructure != null ? data.FeesStructure.IsFine : false,
                        IsCustom = data.FeesStructure != null ? data.FeesStructure.IsCustom : false,
                        IsMonthly = data.FeesStructure != null ? data.FeesStructure.IsMonthly : false
                    }).ToList(); 
                }
                #endregion

            }

           
            //selecter
            string ledgerid = LedgerId.ISValidObject() ? LedgerId.ConvertObjectToString() : "";
            string subjectid = SubjectId.ISValidObject() ? SubjectId.ConvertObjectToString() : "";

            //DropDownBox
            Task.Run(async () =>
            {
                ViewBag.SubjectId =await subjectComponent.CmbHsLabSubjectAsync(subjectid);
                ViewBag.LedgerId = await ledgerComponent.CmbLedgerByCategoryAsync(LedgerCategories.FEES,ledgerid);
            }).Wait();

            //Radion Button chek View Bag
           var a0=(chkLab == "Individual" ? ViewBag.Individual="checked" : ViewBag.AllInOne = "checked");


            return View(feesStructureVM);
        }

        #region ===Save Section===
        [HttpPost]
        [WebMethod]
        public async Task<JsonResult> SaveFeesStructure(FeesStructureVM feesStructureVM)
        {

            if (feesStructureVM!=null)
            {
                if (feesStructureVM.FeesStructureID != null && feesStructureVM.FeesStructureID != default(Guid))
                {
                    await UpdateFeesStructure(feesStructureVM);
                    //return new EmptyResult();
                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //for New
                    feesStructureVM.FeesStructureID = Guid.NewGuid();
                    await InsertFeesStructure(feesStructureVM);

                    //return new EmptyResult();
                    return Json(new { FeesStructureID = feesStructureVM.FeesStructureID }, JsonRequestBehavior.AllowGet);
                }
                
            }
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public async Task InsertFeesStructure(FeesStructureVM feesStructureVM)
        {
            var _SessionId = (await _classTools.GetClassAsync(feesStructureVM.ClassId)).SessionId;
            bool isLabLedger = await ledgerTools.IsLabLedgerAsync(feesStructureVM.LedgerId.ConvertToGuid());

            //Object
            FeesStructure fs = new FeesStructure();
            FeesAmount fa = new FeesAmount();


            //Fees Structure
            fs.FeesStructureID = feesStructureVM.FeesStructureID.ConvertToGuid();
            fs.Date = DateTime.Now;
            fs.LedgerId = feesStructureVM.LedgerId.ConvertToGuid();
            fs.SessionID = _SessionId;
            fs.IsAdmission = feesStructureVM.IsAdmission.ConvertObjectToBool();
            fs.IsReadmission = feesStructureVM.IsReadmission.ConvertObjectToBool();
            fs.IsFine = feesStructureVM.IsFine;
            fs.IsCustom = feesStructureVM.IsCustom;
            fs.IsMonthly = feesStructureVM.IsMonthly;

            //Subject
            fs.SubjectId = isLabLedger ? feesStructureVM.SubjectId : null;

            //FeesAmount
            fa.FeesStructureID = fs.FeesStructureID;
            fa.ClassId = feesStructureVM.ClassId.ConvertObjectToLong();
            fa.Fees = feesStructureVM.FeesAmount.ConvertObjectToDouble();

            using (db = new SP_DataEntities())
            {
                db.Database.Log = Console.Write;
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Fees Structure
                        db.FeesStructures.Add(fs);
                        await db.SaveChangesAsync();

                        //FeesAmount
                        db.FeesAmounts.Add(fa);
                        await db.SaveChangesAsync();


                        //Commit
                        transaction.Commit();

                        //return true;
                    }
                    catch (DbEntityValidationException e)
                    {
                        var newException = new FormattedDbEntityValidationException(e);
                        throw newException;
                    }
                }
            }

        }
        [NonAction]
        public async Task UpdateFeesStructure(FeesStructureVM feesStructureVM)
        {
            var _SessionId = (await _classTools.GetClassAsync(feesStructureVM.ClassId)).SessionId;
            using (db = new SP_DataEntities())
            {
                //Fees Structure
                FeesStructure fs = db.FeesStructures.Where(f => f.FeesStructureID == feesStructureVM.FeesStructureID).Where(s => s.SessionID == _SessionId).FirstOrDefault();
                fs.IsAdmission = feesStructureVM.IsAdmission.ConvertObjectToBool();
                fs.IsReadmission = feesStructureVM.IsReadmission.ConvertObjectToBool();
                fs.IsFine = feesStructureVM.IsFine;
                fs.IsCustom = feesStructureVM.IsCustom;
                fs.IsMonthly = feesStructureVM.IsMonthly;
                fs.Date = DateTime.Now;

                //FeesAmount
                FeesAmount fa = db.FeesAmounts.Where(f => f.FeesStructureID == feesStructureVM.FeesStructureID).Where(c => c.ClassId == feesStructureVM.ClassId).FirstOrDefault();
                fa.Fees = feesStructureVM.FeesAmount.ConvertObjectToDouble();

                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Fees Structure
                        db.Entry(fs).State = EntityState.Modified;
                        await db.SaveChangesAsync();

                        //FeesAmount
                        db.Entry(fa).State = EntityState.Modified;
                        await db.SaveChangesAsync();


                        //Commit
                        transaction.Commit();

                        //return true;
                    }
                    catch (DbEntityValidationException e)
                    {
                        var newException = new FormattedDbEntityValidationException(e);
                        throw newException;
                    }
                }

            }
        } 
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
