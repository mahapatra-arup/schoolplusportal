﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SP.Data.Models;

namespace SP.Web.Areas.Fees.Controllers
{
    public class FeesConfigsController : Controller
    {
        private SP_DataEntities db = new SP_DataEntities();
        List<string> lstPaperSize = new List<string> { "A3","A4","A5"};
       

        public async Task<ActionResult> Edit()
        {
            FeesConfig feesConfig = await db.FeesConfigs.FirstOrDefaultAsync();
            if (feesConfig == null)
            {
                return HttpNotFound();
            }

       //dropDownLists
              ViewBag.PaperSizes = new SelectList(lstPaperSize.Select(x => new { Value = x, Text = x })
              , "Value",
               "Text");

            return View(feesConfig);
        }

         [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,IsMonthly,PaperSize,IsAuditNonAudit,IsdateShow,IsPlaceShow,IsPageNoShow,IsFooterEndOfPage")] FeesConfig feesConfig)
        {
            if (ModelState.IsValid)
            {
                db.Entry(feesConfig).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }

            //dropDownLists
            ViewBag.PaperSizes = new SelectList(lstPaperSize.Select(x => new { Value = x, Text = x })
            , "Value",
             "Text");
            return View(feesConfig);
        }

       
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
