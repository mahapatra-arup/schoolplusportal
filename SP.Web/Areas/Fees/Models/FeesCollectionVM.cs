﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SP.Web.Areas.Fees.Models
{
    public class FeesCollectionVM
    {
        public StudentListView _StudentListView { get; set; }
        public FeesPaymentDetail _FeesPaymentDetail { get; set; }
        public List<FeesPaymentDetailVM> _FeesPaymentDetailVMForCompulsory { get; set; }
        public List<FeesPaymentDetailVM> _FeesPaymentDetailVMForCustom { get; set; }
        public List<FeesMonthsVM> _Months { get; set; }
        public string Narration { get; set; }
    }


    public class FeesPaymentDetailVM
    {
        public Guid LedgerId { get; set; }
        public long FeesAmountId { get; set; }

        [Display(Name = "Fees head")]
        public string LedgerName { get; set; }

        public double? Fees { get; set; } = 0d;//Due Total Fees

        
        [Required(ErrorMessage = "Invalid Amount.")]
        [Display(Name = "Paid Fees")]
        public double? PaidFees { get; set; } = 0d;

        
        [Display(Name = "Current Due")]
        public double? DueFees { get; set; } = 0d;

       
        [Display(Name = "Concession")]
        public double? Concession { get; set; } = 0d;

      
        [Display(Name = "Due")]//Previoud
        public double? PreviousDueFees { get; set; } = 0d;
    }

   
}