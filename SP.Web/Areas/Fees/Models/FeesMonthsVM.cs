﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SP.Web.Areas.Fees.Models
{
    public class FeesMonthsVM
    {
        public string MonthName { get; set; }
        public bool IsSelected { get; set; } = false;
    }
}