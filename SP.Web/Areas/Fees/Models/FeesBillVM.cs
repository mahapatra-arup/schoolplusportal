﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SP.Web.Areas.Fees.Models
{
    public class FeesBillVM
    {
        public bool IsBillGenerate { get; set; } = false;

        public long ClassId { get; set; }
        public Guid StudentId { get; set; }
        public string RegisterNo { get; set; }
        public string StudentName { get; set; }
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public long RollNo { get; set; }
    }

    /*
     * Custom Fees Bill View VM*
    */
    public class CustomFeesBillVM
    {
        public StudentListView _StudentListView { get; set; }
        public List<FeesMonthsVM> _Months { get; set; }
        public FeesBillDetail _FeesBillDetails { get; set; }
        [Required(ErrorMessage = "Invalid Selection")]
        public Guid? LedgerId { get; set; }
    }
}