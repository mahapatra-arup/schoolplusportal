﻿using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SP.Web.Areas.Fees.Models
{
    /// <summary>
    /// FeesStructure Custom View Model 
    /// </summary>
    public class FeesStructureVM
    {
        //Class
        public long? ClassId { get; set; }
        public string ClassName { get; set; }

        //FeesAMount
        [RegularExpression(@"^[0-9]{1,3}$", ErrorMessage = "Only number Acceptable")]
        [Range(1, 1000)]
        public double? FeesAmount { get; set; }

        //FeesStructure
        public Guid? FeesStructureID { get; set; }
        public long? SessionId { get; set; }
        public System.Guid? LedgerId { get; set; }
        public Nullable<long> SubjectId { get; set; }
        public Nullable<bool> IsAdmission { get; set; } = false;
        public Nullable<bool> IsReadmission { get; set; } = false;
        public Nullable<bool> IsCustom { get; set; } = false;
        public Nullable<bool> IsFine { get; set; } = false;
        public Nullable<bool> IsMonthly { get; set; } = false;
    }
}