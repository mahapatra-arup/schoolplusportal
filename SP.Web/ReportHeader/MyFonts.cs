﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace SP.Web.ReportHeader
{
   static public class MyFonts
    {
       public static List<string> GetAllFonts()
       {
           List<string> fonts = new List<string>();

           foreach (FontFamily font in System.Drawing.FontFamily.Families)
           {
               fonts.Add(font.Name);
           }
           return fonts;
       }
    }
}
