﻿using System.Collections.Generic;
using SchoolPlus.MyTools;
using System.Windows.Forms;

namespace SP.Web.ReportHeader
{
    public static class InsertHeaderConfigarationdetails
    {
        private static List<string> lstQuery = new List<string>();

        public static void AdmissionFormVHeaderReset()
        {
            lstQuery.Clear();

            string query = "";
            query = "delete from ReportHeaderConfigAdmissionFormV";
            if (SQLHelper.GetInstance().ExecuteQuery(query))
            {
                string contactDetails = SchoolUtils.gSchoolPhone + "                                                                                                                " + SchoolUtils.gSchoolEmail;
                query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormV] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                        "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor],  \r\n" +
                        "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                        "   [Visibility]) VALUES ( 0,'" + contactDetails + "', 4, 8, 21, 780, N'WindowText', N'White', N'Center', \r\n" +
                        "    N'Microsoft Sans Serif', N'Regular', 9.25, 1) \r\n";
                lstQuery.Add(query);

                string schoolName = SchoolUtils.gSchoolName.Trim().GetDBFormatString();
                query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormV] ( [SlNo], [HeaderText], [HeaderTop],  \r\n" +
                        " [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor],  \r\n" +
                        " [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                        "  [Visibility]) VALUES ( 1, '" + schoolName + "', 26, 6, 34, 780, \r\n" +
                        "   N'Black', N'White', N'Center', N'Microsoft Sans Serif', N'Bold', 21, 1) \r\n";
                lstQuery.Add(query);

                query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormV] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                         "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor], \r\n" +
                         "   [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                         "    [Visibility]) VALUES (2, 'HIGHER SECONDARY', 56, 4, 24, 780, N'Black', \r\n" +
                         "     N'White', N'Center', N'Microsoft Sans Serif', N'Bold', 10.25, 1) \r\n";
                lstQuery.Add(query);

                string estd = "ESTD- " + SchoolUtils.gSchoolESTD;
                query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormV] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                         "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor], \r\n" +
                         "   [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                         "    [Visibility]) VALUES ( 3, '" + estd + "', 77, 2, 24, 780, N'Blue', N'White', \r\n" +
                         "     N'Center', N'Microsoft Sans Serif', N'Bold', 10, 1) \r\n";
                lstQuery.Add(query);

                string indexHS = "INDEX NO.- " + SchoolUtils.gSchoolIndexNo + "  ::  H.S. CODE- " + SchoolUtils.gSchoolHSCode;
                query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormV] ( [SlNo], [HeaderText], [HeaderTop],  \r\n" +
                         " [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor], \r\n" +
                         "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                         "   [Visibility]) VALUES ( 4,'" + indexHS + "', 98, 7, \r\n" +
                         "    24, 780, N'Blue', N'White', N'Center', N'Microsoft Sans Serif', N'Bold', 8.25, 1) \r\n";
                lstQuery.Add(query);

                query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormV] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                         "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor], \r\n" +
                         "   [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                         "    [Visibility]) VALUES ( 5, N'', 49, 15, 61, 72, N'ControlText', N'InactiveBorder',  \r\n" +
                         "    N'MiddleCenter', N'Microsoft Sans Serif', N'Regular', 8.25, 1) \r\n";
                lstQuery.Add(query);

                if (SQLHelper.GetInstance().ExecuteTransection(lstQuery))
                {
                    MessageBox.Show("Reset SuccessFull", "Reset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        public static void AdmissionFormXIHeaderReset()
        {
            lstQuery.Clear();

            string query = "";
            query = "delete from ReportHeaderConfigAdmissionFormXI";
            if (SQLHelper.GetInstance().ExecuteQuery(query))
            {
                string contactDetails = SchoolUtils.gSchoolPhone + "                                                                                                                " + SchoolUtils.gSchoolEmail;
                query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormXI] ( [SlNo], [HeaderText], [HeaderTop],  \r\n" +
                         " [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor], \r\n" +
                         "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                         "   [Visibility]) VALUES (0, '" + contactDetails + "', 5, 8, 21, 780, N'WindowText', N'White', N'Center', \r\n" +
                         "    N'Microsoft Sans Serif', N'Regular', 9.25, 1) \r\n";
                lstQuery.Add(query);

                string schoolName = SchoolUtils.gSchoolName.Trim().GetDBFormatString();
                query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormXI] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                        "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor],  \r\n" +
                        "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize],  \r\n" +
                        "  [Visibility]) VALUES ( 1, '" + schoolName + "', 17, \r\n" +
                        "   10, 33, 780, N'Blue', N'White', N'Center', N'Old English Text MT', N'Bold', 28, 1) \r\n";
                lstQuery.Add(query);

                query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormXI] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                        "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor], \r\n" +
                        "   [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                        "    [Visibility]) VALUES (2,'HIGHER SECONDARY', 53, 7, 18, 780, N'Blue',  \r\n" +
                        "    N'White', N'Center', N'Microsoft Sans Serif', N'Bold', 10.25, 1) \r\n";
                lstQuery.Add(query);

                string estd = "ESTD- " + SchoolUtils.gSchoolESTD;
                query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormXI] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                        "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor],  \r\n" +
                        "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                        "   [Visibility]) VALUES ( 3, '" + estd + "', 71, 5, 20, 780, N'Blue', N'White',  \r\n" +
                        "   N'Center', N'Microsoft Sans Serif', N'Bold', 10, 1) \r\n";
                lstQuery.Add(query);

                string indexHS = "INDEX NO.- " + SchoolUtils.gSchoolIndexNo + "  ::  H.S. CODE- " + SchoolUtils.gSchoolHSCode;
                query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormXI] ( [SlNo], [HeaderText], [HeaderTop],  \r\n" +
                        " [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor], \r\n" +
                        "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                        "   [Visibility]) VALUES ( 4,'" + indexHS + "', 92, 7, \r\n" +
                        "    20, 780, N'Blue', N'White', N'Center', N'Microsoft Sans Serif', N'Bold', 8, 1) \r\n";
                lstQuery.Add(query);

                string address1 = "P.O.- " + SchoolUtils.gSchoolPO + "  ::  DIST.- " + SchoolUtils.gSchoolDist + "  ::  PIN- " + SchoolUtils.gSchoolPIN;
                query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormXI] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                        "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor],  \r\n" +
                        "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize],  \r\n" +
                        "  [Visibility]) VALUES ( 5, '" + address1.Trim().GetDBFormatString() + "', \r\n" +
                        "   111, 11, 24, 780, N'Blue', N'White', N'Center', N'Microsoft Sans Serif', N'Bold', 8.25, 1) \r\n";
                lstQuery.Add(query);

                query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormXI] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                        "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor],  \r\n" +
                        "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                        "   [Visibility]) VALUES ( 6, N'', 53, 28, 80, 80, N'ControlText', N'InactiveBorder', \r\n" +
                        "    N'MiddleCenter', N'Microsoft Sans Serif', N'Regular', 8.25, 1) \r\n";
                lstQuery.Add(query);

                if (SQLHelper.GetInstance().ExecuteTransection(lstQuery))
                {
                    MessageBox.Show("Reset SuccessFull", "Reset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }




        public static void SingleCertificateForFullPage()
        {
            lstQuery.Clear();

            string query = "";
            query = "delete from ReportHeaderConfig";
            if (SQLHelper.GetInstance().ExecuteQuery(query))
            {
                string contactDetails = SchoolUtils.gSchoolPhone + "                                                                                                                " + SchoolUtils.gSchoolEmail;
                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(0,'" + contactDetails + "',66,5,21,780,'WindowText','White','Center','Microsoft Sans Serif','Regular',9.25,'True')";
                lstQuery.Add(query);

                string schoolName = SchoolUtils.gSchoolName.Trim().GetDBFormatString();
                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(1,'" + schoolName + "',90,5,34,780,'Blue','White','Center','David','Bold',26,'True')";
                lstQuery.Add(query);

                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(2,'HIGHER SECONDARY',122,5,24,780,'Blue','White','Center','Microsoft Sans Serif','Bold',12.25,'True')";
                lstQuery.Add(query);

                string estd = "ESTD- " + SchoolUtils.gSchoolESTD;
                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(3,'" + estd + "',144,5,24,780,'Blue','White','Center','Microsoft Sans Serif','Bold',12,'True')";
                lstQuery.Add(query);

                string indexHS = "INDEX NO.- " + SchoolUtils.gSchoolIndexNo + "  ::  H.S. CODE- " + SchoolUtils.gSchoolHSCode;
                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(4,'" + indexHS + "',167,5,24,780,'Blue','White','Center','Microsoft Sans Serif','Bold',11.25,'True')";
                lstQuery.Add(query);

                string address1 = "P.O.- " + SchoolUtils.gSchoolPO + "  ::  DIST.- " + SchoolUtils.gSchoolDist + "  ::  PIN- " + SchoolUtils.gSchoolPIN;
                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(5,'" + address1.Trim().GetDBFormatString() + "',189,5,24,780,'Blue','White','Center','Microsoft Sans Serif','Bold',10.25,'True')";
                lstQuery.Add(query);

                string address2 = "BLOCK- " + SchoolUtils.gSchoolBlock + "  ::  SUB. DIV.- " + SchoolUtils.gSchoolSubDivision;
                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(6,'" + address2.Trim().GetDBFormatString() + "',210,5,20,780,'Blue','White','Center','Microsoft Sans Serif','Bold',10.25,'True')";
                lstQuery.Add(query);

                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(7,'',125,25,100,100,'Blue','White','Center','Microsoft Sans Serif','Bold',8.25,'True')";
                lstQuery.Add(query);

                if (SQLHelper.GetInstance().ExecuteTransection(lstQuery))
                {
                    MessageBox.Show("Reset SuccessFull", "Reset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        public static void SingleCertificateForHalfLegalPage()

        {
            lstQuery.Clear();

            string query = "";
            query = "delete from ReportHeaderConfig";
            if (SQLHelper.GetInstance().ExecuteQuery(query))
            {

                string contactDetails = SchoolUtils.gSchoolPhone + "                                                                                                                " + SchoolUtils.gSchoolEmail;
                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(0,'" + contactDetails + "',17,3,21,670,'WindowText','White','Center','Microsoft Sans Serif','Regular',9.25,'True')";
                lstQuery.Add(query);

                string schoolName = SchoolUtils.gSchoolName.Trim().GetDBFormatString();
                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(1,'" + schoolName + "',35,4,30,667,'Blue','White','Center','Microsoft Sans Serif','Bold',19,'True')";
                lstQuery.Add(query);

                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(2,'HIGHER SECONDARY',65,4,21,668,'Blue','White','Center','Microsoft Sans Serif','Bold',10.25,'True')";
                lstQuery.Add(query);

                string estd = "ESTD- " + SchoolUtils.gSchoolESTD;
                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(3,'" + estd + "',86,4,19,668,'Blue','White','Center','Microsoft Sans Serif','Bold',10,'True')";
                lstQuery.Add(query);

                string indexHS = "INDEX NO.- " + SchoolUtils.gSchoolIndexNo + "  ::  H.S. CODE- " + SchoolUtils.gSchoolHSCode;
                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(4,'" + indexHS + "',104,5,18,669,'Blue','White','Center','Microsoft Sans Serif','Bold',9.25,'True')";
                lstQuery.Add(query);

                string address1 = "P.O.- " + SchoolUtils.gSchoolPO + "  ::  DIST.- " + SchoolUtils.gSchoolDist + "  ::  PIN- " + SchoolUtils.gSchoolPIN;
                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(5,'" + address1.Trim().GetDBFormatString() + "',120,5,21,670,'Blue','White','Center','Microsoft Sans Serif','Bold',9.25,'True')";
                lstQuery.Add(query);

                string address2 = "BLOCK- " + SchoolUtils.gSchoolBlock + "  ::  SUB. DIV.- " + SchoolUtils.gSchoolSubDivision;
                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(6,'" + address2.Trim().GetDBFormatString() + "',137,4,18,669,'Blue','White','Center','Microsoft Sans Serif','Bold',9.25,'True')";
                lstQuery.Add(query);

                query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(7,'',69,28,75,80,'ControlText','InactiveBorder','MiddleCenter','Microsoft Sans Serif','Regular',8.25,'True')";
                lstQuery.Add(query);

                if (SQLHelper.GetInstance().ExecuteTransection(lstQuery))
                {
                    MessageBox.Show("Reset SuccessFull", "Reset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        public static void LandScapeDoubleCertificate()
        {
            lstQuery.Clear();
            string query = "";
            query = "delete from ReportHeaderCertificateLandscapDouble";
            if (SQLHelper.GetInstance().ExecuteQuery(query))
            {
                string schoolName = SchoolUtils.gSchoolName.Trim().GetDBFormatString();
                query = "Insert into ReportHeaderCertificateLandscapDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(1,'" + schoolName + "',50,40,30,496,'Blue','White','Center','David','Bold',22,'True')";
                lstQuery.Add(query);

                string estd = "ESTD- " + SchoolUtils.gSchoolESTD;
                query = "Insert into ReportHeaderCertificateLandscapDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(2,'" + estd + "',80,40,20,496,'Blue','White','Center','Microsoft Sans Serif','Bold',12,'True')";
                lstQuery.Add(query);

                string indexHS = "INDEX NO.- " + SchoolUtils.gSchoolIndexNo + "  ::  H.S. CODE- " + SchoolUtils.gSchoolHSCode;
                query = "Insert into ReportHeaderCertificateLandscapDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(3,'" + indexHS + "',97,40,20,496,'Blue','White','Center','Microsoft Sans Serif','Bold',11.25,'True')";
                lstQuery.Add(query);

                string address1 = "P.O.- " + SchoolUtils.gSchoolPO + "  ::  DIST.- " + SchoolUtils.gSchoolDist + "  ::  PIN- " + SchoolUtils.gSchoolPIN;
                query = "Insert into ReportHeaderCertificateLandscapDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(4,'" + address1.Trim().GetDBFormatString() + "',115,40,20,496,'Blue','White','Center','Microsoft Sans Serif','Bold',10.25,'True')";
                lstQuery.Add(query);

                query = "Insert into ReportHeaderCertificateLandscapDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(5,'',74,40,56,64,'Blue','White','Center','Microsoft Sans Serif','Bold',8.25,'True')";
                lstQuery.Add(query);

                if (SQLHelper.GetInstance().ExecuteTransection(lstQuery))
                {
                    MessageBox.Show("Reset SuccessFull", "Reset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        public static void ExamLandScap()
        {
            lstQuery.Clear();
            string query = "";
            query = "delete from ReportHeaderExamLandscap";
            if (SQLHelper.GetInstance().ExecuteQuery(query))
            {
                string schoolName = SchoolUtils.gSchoolName.Trim().GetDBFormatString();
                query = "Insert into ReportHeaderExamLandscap(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(1,'" + schoolName + "',56,208,34,720,'Blue','White','Center','David','Bold',26,'True')";
                lstQuery.Add(query);

                string address1 = "P.O.- " + SchoolUtils.gSchoolPO + "  ::  DIST.- " + SchoolUtils.gSchoolDist + "  ::  PIN- " + SchoolUtils.gSchoolPIN;
                query = "Insert into ReportHeaderExamLandscap(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(2,'" + address1.Trim().GetDBFormatString() + "',96,208,25,720,'Blue','White','Center','Microsoft Sans Serif','Bold',10.25,'True')";
                lstQuery.Add(query);

                query = "Insert into ReportHeaderExamLandscap(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(3,'',48,72,96,96,'Blue','White','Center','Microsoft Sans Serif','Bold',8.25,'True')";
                lstQuery.Add(query);

                if (SQLHelper.GetInstance().ExecuteTransection(lstQuery))
                {
                    MessageBox.Show("Reset SuccessFull", "Reset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        public static void ExamPortrait()
        {
            lstQuery.Clear();
            string query = "";
            query = "delete from ReportHeaderExamPortrait";
            if (SQLHelper.GetInstance().ExecuteQuery(query))
            {
                string schoolName = SchoolUtils.gSchoolName.Trim().GetDBFormatString();
                query = "Insert into ReportHeaderExamPortrait(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(1,'" + schoolName + "',48,120,30,640,'Blue','White','Center','David','Bold',22,'True')";
                lstQuery.Add(query);

                string address1 = "P.O.- " + SchoolUtils.gSchoolPO + "  ::  DIST.- " + SchoolUtils.gSchoolDist + "  ::  PIN- " + SchoolUtils.gSchoolPIN;
                query = "Insert into ReportHeaderExamPortrait(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(2,'" + address1.Trim().GetDBFormatString() + "',80,120,30,640,'Blue','White','Center','Microsoft Sans Serif','Bold',10.25,'True')";
                lstQuery.Add(query);

                query = "Insert into ReportHeaderExamPortrait(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(3,'',40,32,80,88,'Blue','White','Center','Microsoft Sans Serif','Bold',8.25,'True')";
                lstQuery.Add(query);

                if (SQLHelper.GetInstance().ExecuteTransection(lstQuery))
                {
                    MessageBox.Show("Reset SuccessFull", "Reset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        public static void FeesDouble()
        {
            lstQuery.Clear();
            string query = "";
            query = "delete from ReportHeaderFeesDouble";
            if (SQLHelper.GetInstance().ExecuteQuery(query))
            {
                string schoolName = SchoolUtils.gSchoolName.Trim().GetDBFormatString();
                query = "Insert into ReportHeaderFeesDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(1,'" + schoolName + "',12,2,29,380,'Blue','White','Center','David','Bold',13,'True')";
                lstQuery.Add(query);

                string estd = "ESTD- " + SchoolUtils.gSchoolESTD;
                query = "Insert into ReportHeaderFeesDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(2,'" + estd + "',36,2,21,380,'Blue','White','Center','Arial','Bold',8,'True')";
                lstQuery.Add(query);

                string address1 = "P.O.- " + SchoolUtils.gSchoolPO + "  ::  DIST.- " + SchoolUtils.gSchoolDist + "  ::  PIN- " + SchoolUtils.gSchoolPIN;
                query = "Insert into ReportHeaderFeesDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(3,'" + address1.Trim().GetDBFormatString() + "',53,6,24,380,'Blue','White','Center','Arial','Bold',8,'True')";
                lstQuery.Add(query);

                query = "Insert into ReportHeaderFeesDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                        "values(4,'',23,8,46,64,'Blue','White','Center','Microsoft Sans Serif','Bold',8.25,'True')";
                lstQuery.Add(query);

                if (SQLHelper.GetInstance().ExecuteTransection(lstQuery))
                {
                    MessageBox.Show("Reset SuccessFull", "Reset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
