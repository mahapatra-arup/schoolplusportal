﻿using System;
using CrystalDecisions.CrystalReports.Engine;
using System.Drawing;
using System.Data;

namespace SP.Web.ReportHeader
{
    public class HeaderConfigCertificateSingle
    {
        private static FontStyle mFontStyle;
        public enum _Purpose
        {
            SingleCertificate,
            AddmissionFormHeaderV,
            AddmissionFormHeaderXI,
        }
        //FullHeader
        public static void FullHeaderForSingleCertificate(ref TextObject txtHeader1, ref TextObject txtHeader2, ref TextObject txtHeader3, ref TextObject txtHeader4, ref TextObject txtHeader5, ref TextObject txtHeader6, ref TextObject txtHeader7, ref BlobFieldObject pictureBox)
        {
            try
            {
                HeaderPreviewForCerificateSingle(ref txtHeader1, 0, _Purpose.SingleCertificate);
                HeaderPreviewForCerificateSingle(ref txtHeader2, 1, _Purpose.SingleCertificate);
                HeaderPreviewForCerificateSingle(ref txtHeader3, 2, _Purpose.SingleCertificate);
                HeaderPreviewForCerificateSingle(ref txtHeader4, 3, _Purpose.SingleCertificate);
                HeaderPreviewForCerificateSingle(ref txtHeader5, 4, _Purpose.SingleCertificate);
                HeaderPreviewForCerificateSingle(ref txtHeader6, 5, _Purpose.SingleCertificate);
                HeaderPreviewForCerificateSingle(ref txtHeader7, 6, _Purpose.SingleCertificate);
                HeaderPreviewForCerificateSingleSchoolLogo(ref pictureBox, 7, _Purpose.SingleCertificate);
            }
            catch (Exception)
            {
            }
        }

        public static void FullHeaderForAddmissionFormV(ref TextObject txtHeader0, ref TextObject txtHeader1, ref TextObject txtHeader2, ref TextObject txtHeader3, ref TextObject txtHeader4, ref BlobFieldObject pictureBox)
        {
            try
            {
                HeaderPreviewForCerificateSingle(ref txtHeader0, 0, _Purpose.AddmissionFormHeaderV);
                HeaderPreviewForCerificateSingle(ref txtHeader1, 1, _Purpose.AddmissionFormHeaderV);
                HeaderPreviewForCerificateSingle(ref txtHeader2, 2, _Purpose.AddmissionFormHeaderV);
                HeaderPreviewForCerificateSingle(ref txtHeader3, 3, _Purpose.AddmissionFormHeaderV);
                HeaderPreviewForCerificateSingle(ref txtHeader4, 4, _Purpose.AddmissionFormHeaderV);
                HeaderPreviewForCerificateSingleSchoolLogo(ref pictureBox, 5, _Purpose.AddmissionFormHeaderV);
            }
            catch (Exception)
            {
            }
        }

        public static void FullHeaderForAddmissionFormXI(ref TextObject txtHeader0, ref TextObject txtHeader1, ref TextObject txtHeader2, ref TextObject txtHeader3, ref TextObject txtHeader4, ref TextObject txtHeader5, ref BlobFieldObject pictureBox)
        {
            try
            {
                HeaderPreviewForCerificateSingle(ref txtHeader0, 0, _Purpose.AddmissionFormHeaderXI);
                HeaderPreviewForCerificateSingle(ref txtHeader1, 1, _Purpose.AddmissionFormHeaderXI);
                HeaderPreviewForCerificateSingle(ref txtHeader2, 2, _Purpose.AddmissionFormHeaderXI);
                HeaderPreviewForCerificateSingle(ref txtHeader3, 3, _Purpose.AddmissionFormHeaderXI);
                HeaderPreviewForCerificateSingle(ref txtHeader4, 4, _Purpose.AddmissionFormHeaderXI);
                HeaderPreviewForCerificateSingle(ref txtHeader5, 5, _Purpose.AddmissionFormHeaderXI);
                HeaderPreviewForCerificateSingleSchoolLogo(ref pictureBox, 6, _Purpose.AddmissionFormHeaderXI);
            }
            catch (Exception)
            {
            }
        }



        public static void HeaderPreviewForCerificateSingle(ref TextObject mTxtHeader1, int slNo, _Purpose purpose)
        {
            string headerText = "", foreColor, backColor, align, fontName, fontStyle;
            int top, left, height, width;
            float fontSize;
            bool visibility;
            switch (purpose)
            {
                case _Purpose.SingleCertificate:
                    GetHeaderDetailsForSingleCertificate(slNo, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);
                    break;
                case _Purpose.AddmissionFormHeaderV:
                    GetHeaderDetailsForAddmissionForV(slNo, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);
                    break;
                case _Purpose.AddmissionFormHeaderXI:
                    GetHeaderDetailsForAddmissionForXI(slNo, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);
                    break;
                default:
                    GetHeaderDetailsForSingleCertificate(slNo, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);
                    break;
            }

            mTxtHeader1.Text = headerText;
            mTxtHeader1.Top = top * 15;
            mTxtHeader1.Width = width * 15;
            mTxtHeader1.Left = left * 15;
            mTxtHeader1.Height = height * 15;
            mTxtHeader1.Color = Color.FromName(foreColor);
            // mTxtHeader1.Border.BackgroundColor = Color.FromName(backColor);
            switch (align)
            {
                #region MyRegion
                case "Center":
                    mTxtHeader1.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.HorizontalCenterAlign;
                    break;
                case "Laft":
                    mTxtHeader1.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.LeftAlign;
                    break;
                case "Right":
                    mTxtHeader1.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.RightAlign;
                    break;
                default:
                    break;
                    #endregion
            }
            switch (fontStyle)
            {
                #region MyRegion
                case "Regular":
                    mFontStyle = FontStyle.Regular;
                    break;
                case "Bold":
                    mFontStyle = FontStyle.Bold;
                    break;
                case "Italic":
                    mFontStyle = FontStyle.Italic;
                    break;
                case "Bold,Italic":
                    mFontStyle = FontStyle.Bold | FontStyle.Italic;
                    break;
                default:
                    break;
                    #endregion
            }
            mTxtHeader1.ApplyFont(new Font(fontName, fontSize, mFontStyle));
        }

        /// <summary>
        /// Picture Object
        /// </summary>
        /// 
        public static void HeaderPreviewForCerificateSingleSchoolLogo(ref BlobFieldObject mPicObject, int slNo, _Purpose purpose)
        {
            string headerText = "", foreColor, backColor, align, fontName, fontStyle;
            int top, left, height, width;
            float fontSize;
            bool visibility;

            switch (purpose)
            {
                case _Purpose.SingleCertificate:
                    GetHeaderDetailsForSingleCertificate(slNo, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);
                    break;
                case _Purpose.AddmissionFormHeaderV:
                    GetHeaderDetailsForAddmissionForV(slNo, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);
                    break;
                case _Purpose.AddmissionFormHeaderXI:
                    GetHeaderDetailsForAddmissionForXI(slNo, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);
                    break;
                default:
                    GetHeaderDetailsForSingleCertificate(slNo, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);
                    break;
            }

            // mTxtHeader7.Text = headerText;
            mPicObject.Top = top * 15;
            mPicObject.Width = width * 15;
            mPicObject.Left = left * 15;
            mPicObject.Height = height * 15;
            mPicObject.Border.BorderColor = Color.Gainsboro;
            mPicObject.ObjectFormat.EnableSuppress = !visibility;
            //mPicObject.Picture
            //mTxtHeader7.Color = Color.FromName(foreColor);
            //mTxtHeader7.Border.BackgroundColor = Color.FromName(backColor);
        }


        private static void GetHeaderDetailsForSingleCertificate(int slno, out string headerText, out int top, out int left, out int height, out int width, out string foreColor, out string backColor, out string align, out string fontname, out string fontStyle, out float fontSize, out bool visibility)
        {
            visibility = false;
            headerText = ""; top = 0; left = 0; height = 0; width = 0;
            foreColor = ""; backColor = ""; align = ""; fontname = ""; fontStyle = ""; fontSize = 0f;
            string query = "Select * from ReportHeaderConfig where Slno=" + slno + "";
            DataTable dt = SQLHelper.GetInstance().ExecuteNonQuery(query);
            if (dt.IsValidDataTable())
            {
                headerText = dt.Rows[0]["HeaderText"].ToString();
                top = int.Parse(dt.Rows[0]["HeaderTop"].ToString());
                left = int.Parse(dt.Rows[0]["HeaderLeft"].ToString());
                height = int.Parse(dt.Rows[0]["HeaderHeight"].ToString());
                width = int.Parse(dt.Rows[0]["HeaderWidth"].ToString());
                foreColor = dt.Rows[0]["HeaderForColor"].ToString();
                backColor = dt.Rows[0]["HeaderBackColor"].ToString();
                align = dt.Rows[0]["HeaderHorizontalAlign"].ToString();
                fontname = dt.Rows[0]["HeaderFontName"].ToString();
                fontStyle = dt.Rows[0]["HeaderFontStyle"].ToString();
                fontSize = float.Parse(dt.Rows[0]["HeaderFontSize"].ToString());
                visibility = bool.Parse(dt.Rows[0]["Visibility"].ToString());
            }
        }

        private static void GetHeaderDetailsForAddmissionForV(int slno, out string headerText, out int top, out int left, out int height, out int width, out string foreColor, out string backColor, out string align, out string fontname, out string fontStyle, out float fontSize, out bool visibility)
        {
            visibility = false;
            headerText = ""; top = 0; left = 0; height = 0; width = 0;
            foreColor = ""; backColor = ""; align = ""; fontname = ""; fontStyle = ""; fontSize = 0f;
            string query = "Select * from ReportHeaderConfigAdmissionFormV where Slno=" + slno + "";
            DataTable dt = SQLHelper.GetInstance().ExecuteNonQuery(query);
            if (dt.IsValidDataTable())
            {
                headerText = dt.Rows[0]["HeaderText"].ToString();
                top = int.Parse(dt.Rows[0]["HeaderTop"].ToString());
                left = int.Parse(dt.Rows[0]["HeaderLeft"].ToString());
                height = int.Parse(dt.Rows[0]["HeaderHeight"].ToString());
                width = int.Parse(dt.Rows[0]["HeaderWidth"].ToString());
                foreColor = dt.Rows[0]["HeaderForColor"].ToString();
                backColor = dt.Rows[0]["HeaderBackColor"].ToString();
                align = dt.Rows[0]["HeaderHorizontalAlign"].ToString();
                fontname = dt.Rows[0]["HeaderFontName"].ToString();
                fontStyle = dt.Rows[0]["HeaderFontStyle"].ToString();
                fontSize = float.Parse(dt.Rows[0]["HeaderFontSize"].ToString());
                visibility = bool.Parse(dt.Rows[0]["Visibility"].ToString());
            }
        }

        private static void GetHeaderDetailsForAddmissionForXI(int slno, out string headerText, out int top, out int left, out int height, out int width, out string foreColor, out string backColor, out string align, out string fontname, out string fontStyle, out float fontSize, out bool visibility)
        {
            visibility = false;
            headerText = ""; top = 0; left = 0; height = 0; width = 0;
            foreColor = ""; backColor = ""; align = ""; fontname = ""; fontStyle = ""; fontSize = 0f;
            string query = "Select * from ReportHeaderConfigAdmissionFormXI where Slno=" + slno + "";
            DataTable dt = SQLHelper.GetInstance().ExecuteNonQuery(query);
            if (dt.IsValidDataTable())
            {
                headerText = dt.Rows[0]["HeaderText"].ToString();
                top = int.Parse(dt.Rows[0]["HeaderTop"].ToString());
                left = int.Parse(dt.Rows[0]["HeaderLeft"].ToString());
                height = int.Parse(dt.Rows[0]["HeaderHeight"].ToString());
                width = int.Parse(dt.Rows[0]["HeaderWidth"].ToString());
                foreColor = dt.Rows[0]["HeaderForColor"].ToString();
                backColor = dt.Rows[0]["HeaderBackColor"].ToString();
                align = dt.Rows[0]["HeaderHorizontalAlign"].ToString();
                fontname = dt.Rows[0]["HeaderFontName"].ToString();
                fontStyle = dt.Rows[0]["HeaderFontStyle"].ToString();
                fontSize = float.Parse(dt.Rows[0]["HeaderFontSize"].ToString());
                visibility = bool.Parse(dt.Rows[0]["Visibility"].ToString());
            }
        }

    }
}
