﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SP.Web.ReportHeader
{
    class GenerateReportHeaderConfigDB
    {
        public static void Execute()
        {
            GenerateCertificateDB();
            GenerateExamLandScapDB();
            GenerateExamPortraitDB();
            GenerateCertificateLandScapDoubleDB();
            GenerateCertificateTools();
            GenerateExamTools(); //1
            GenerateReportHeaderFeesDoubleDB();
            AddFathersNameAndStudentImageShoworNot();//2
            CreateTableAdmissionFormHeaderDesign();
        }

        private static void CreateTableAdmissionFormHeaderDesign()
        {
            CreateTableAdmissionFormHeaderDesignV_X();
            CreateTableAdmissionFormHeaderDesignXI();
        }



        private static void CreateTableAdmissionFormHeaderDesignV_X()
        {
            string query = " select top(1)Table_name from Information_SCHEMA.columns where Table_name='ReportHeaderConfigAdmissionFormV' \r\n";
            if (!SQLHelper.GetInstance().ExecuteScalar(query).ISValidObject())
            {
                query = " CREATE TABLE [dbo].[ReportHeaderConfigAdmissionFormV]( \r\n" +
                                 " 	[ID] [int] IDENTITY(1,1) NOT NULL, \r\n" +
                                 " 	[SlNo] [int] NOT NULL, \r\n" +
                                 " 	[HeaderText] [varchar](max) NOT NULL, \r\n" +
                                 " 	[HeaderTop] [int] NOT NULL, \r\n" +
                                 " 	[HeaderLeft] [int] NOT NULL, \r\n" +
                                 " 	[HeaderHeight] [int] NOT NULL, \r\n" +
                                 " 	[HeaderWidth] [int] NOT NULL, \r\n" +
                                 " 	[HeaderForColor] [varchar](50) NOT NULL, \r\n" +
                                 " 	[HeaderBackColor] [varchar](50) NULL, \r\n" +
                                 " 	[HeaderHorizontalAlign] [varchar](50) NULL, \r\n" +
                                 " 	[HeaderFontName] [varchar](50) NULL, \r\n" +
                                 " 	[HeaderFontStyle] [varchar](50) NULL, \r\n" +
                                 " 	[HeaderFontSize] [float] NULL, \r\n" +
                                 " 	[Visibility] [bit] NULL, \r\n" +
                                 "  CONSTRAINT [PK_ReportHeaderConfigAdmissionFormV] PRIMARY KEY CLUSTERED  \r\n" +
                                 " ( \r\n" +
                                 " 	[SlNo] ASC \r\n" +
                                 " )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] \r\n" +
                                 " ) ON [PRIMARY] \r\n";
                if (SQLHelper.GetInstance().ExecuteQuery(query))
                {
                    DBUpdate._mTotalTableUpdate++;
                    InsertDataReportHeaderConfigAdmissionFormV();
                }
                else
                {
                    Logs.ShowErrorAsMessageBox();
                }

            }


        }
        private static void InsertDataReportHeaderConfigAdmissionFormV()
        {
            List<string> lstQuery = new List<string>();
            string contactDetails = SchoolUtils.gSchoolPhone + "                                                                                                                " + SchoolUtils.gSchoolEmail;
            string query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormV] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                    "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor],  \r\n" +
                    "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                    "   [Visibility]) VALUES ( 0,'" + contactDetails + "', 4, 8, 21, 780, N'WindowText', N'White', N'Center', \r\n" +
                    "    N'Microsoft Sans Serif', N'Regular', 9.25, 1) \r\n";
            lstQuery.Add(query);

            string schoolName = SchoolUtils.gSchoolName.Trim().GetDBFormatString();
            query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormV] ( [SlNo], [HeaderText], [HeaderTop],  \r\n" +
                    " [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor],  \r\n" +
                    " [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                    "  [Visibility]) VALUES ( 1, '" + schoolName + "', 26, 6, 34, 780, \r\n" +
                    "   N'Black', N'White', N'Center', N'Microsoft Sans Serif', N'Bold', 21, 1) \r\n";
            lstQuery.Add(query);

            query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormV] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                     "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor], \r\n" +
                     "   [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                     "    [Visibility]) VALUES (2, 'HIGHER SECONDARY', 56, 4, 24, 780, N'Black', \r\n" +
                     "     N'White', N'Center', N'Microsoft Sans Serif', N'Bold', 10.25, 1) \r\n";
            lstQuery.Add(query);

            string estd = "ESTD- " + SchoolUtils.gSchoolESTD;
            query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormV] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                     "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor], \r\n" +
                     "   [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                     "    [Visibility]) VALUES ( 3, '" + estd + "', 77, 2, 24, 780, N'Blue', N'White', \r\n" +
                     "     N'Center', N'Microsoft Sans Serif', N'Bold', 10, 1) \r\n";
            lstQuery.Add(query);

            string indexHS = "INDEX NO.- " + SchoolUtils.gSchoolIndexNo + "  ::  H.S. CODE- " + SchoolUtils.gSchoolHSCode;
            query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormV] ( [SlNo], [HeaderText], [HeaderTop],  \r\n" +
                     " [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor], \r\n" +
                     "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                     "   [Visibility]) VALUES ( 4,'" + indexHS + "', 98, 7, \r\n" +
                     "    24, 780, N'Blue', N'White', N'Center', N'Microsoft Sans Serif', N'Bold', 8.25, 1) \r\n";
            lstQuery.Add(query);

            query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormV] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                     "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor], \r\n" +
                     "   [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                     "    [Visibility]) VALUES ( 5, N'', 49, 15, 61, 72, N'ControlText', N'InactiveBorder',  \r\n" +
                     "    N'MiddleCenter', N'Microsoft Sans Serif', N'Regular', 8.25, 1) \r\n";
            lstQuery.Add(query);

            DBUpdate._mTotalTableUpdate += SQLHelper.GetInstance().ExecuteTransection(lstQuery) ? lstQuery.Count : 0;
        }

        private static void CreateTableAdmissionFormHeaderDesignXI()
        {
            string query = " select top(1)Table_name from Information_SCHEMA.columns where Table_name='ReportHeaderConfigAdmissionFormXI' \r\n";
            if (!SQLHelper.GetInstance().ExecuteScalar(query).ISValidObject())
            {
                query = " CREATE TABLE [dbo].[ReportHeaderConfigAdmissionFormXI]( \r\n" +
                                 " 	[ID] [int] IDENTITY(1,1) NOT NULL, \r\n" +
                                 " 	[SlNo] [int] NOT NULL, \r\n" +
                                 " 	[HeaderText] [varchar](max) NOT NULL, \r\n" +
                                 " 	[HeaderTop] [int] NOT NULL, \r\n" +
                                 " 	[HeaderLeft] [int] NOT NULL, \r\n" +
                                 " 	[HeaderHeight] [int] NOT NULL, \r\n" +
                                 " 	[HeaderWidth] [int] NOT NULL, \r\n" +
                                 " 	[HeaderForColor] [varchar](50) NOT NULL, \r\n" +
                                 " 	[HeaderBackColor] [varchar](50) NULL, \r\n" +
                                 " 	[HeaderHorizontalAlign] [varchar](50) NULL, \r\n" +
                                 " 	[HeaderFontName] [varchar](50) NULL, \r\n" +
                                 " 	[HeaderFontStyle] [varchar](50) NULL, \r\n" +
                                 " 	[HeaderFontSize] [float] NULL, \r\n" +
                                 " 	[Visibility] [bit] NULL, \r\n" +
                                 "  CONSTRAINT [PK_ReportHeaderConfigAdmissionFormXI] PRIMARY KEY CLUSTERED  \r\n" +
                                 " ( \r\n" +
                                 " 	[SlNo] ASC \r\n" +
                                 " )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] \r\n" +
                                 " ) ON [PRIMARY] \r\n";
                if (SQLHelper.GetInstance().ExecuteQuery(query))
                {
                    DBUpdate._mTotalTableUpdate++;
                    InsertDataReportHeaderConfigAdmissionFormXI();
                }
                else
                {
                    Logs.ShowErrorAsMessageBox();
                }

            }


        }
        private static void InsertDataReportHeaderConfigAdmissionFormXI()
        {
            List<string> lstQuery = new List<string>();
            string contactDetails = SchoolUtils.gSchoolPhone + "                                                                                                                " + SchoolUtils.gSchoolEmail;
            string query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormXI] ( [SlNo], [HeaderText], [HeaderTop],  \r\n" +
                     " [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor], \r\n" +
                     "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                     "   [Visibility]) VALUES (0, '" + contactDetails + "', 5, 8, 21, 780, N'WindowText', N'White', N'Center', \r\n" +
                     "    N'Microsoft Sans Serif', N'Regular', 9.25, 1) \r\n";
            lstQuery.Add(query);

            string schoolName = SchoolUtils.gSchoolName.Trim().GetDBFormatString();
            query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormXI] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                    "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor],  \r\n" +
                    "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize],  \r\n" +
                    "  [Visibility]) VALUES ( 1, '" + schoolName + "', 17, \r\n" +
                    "   10, 33, 780, N'Blue', N'White', N'Center', N'Old English Text MT', N'Bold', 28, 1) \r\n";
            lstQuery.Add(query);

            query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormXI] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                    "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor], \r\n" +
                    "   [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                    "    [Visibility]) VALUES (2,'HIGHER SECONDARY', 53, 7, 18, 780, N'Blue',  \r\n" +
                    "    N'White', N'Center', N'Microsoft Sans Serif', N'Bold', 10.25, 1) \r\n";
            lstQuery.Add(query);

            string estd = "ESTD- " + SchoolUtils.gSchoolESTD;
            query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormXI] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                    "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor],  \r\n" +
                    "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                    "   [Visibility]) VALUES ( 3, '" + estd + "', 71, 5, 20, 780, N'Blue', N'White',  \r\n" +
                    "   N'Center', N'Microsoft Sans Serif', N'Bold', 10, 1) \r\n";
            lstQuery.Add(query);

            string indexHS = "INDEX NO.- " + SchoolUtils.gSchoolIndexNo + "  ::  H.S. CODE- " + SchoolUtils.gSchoolHSCode;
            query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormXI] ( [SlNo], [HeaderText], [HeaderTop],  \r\n" +
                    " [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor], \r\n" +
                    "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                    "   [Visibility]) VALUES ( 4,'" + indexHS + "', 92, 7, \r\n" +
                    "    20, 780, N'Blue', N'White', N'Center', N'Microsoft Sans Serif', N'Bold', 8, 1) \r\n";
            lstQuery.Add(query);

            string address1 = "P.O.- " + SchoolUtils.gSchoolPO + "  ::  DIST.- " + SchoolUtils.gSchoolDist + "  ::  PIN- " + SchoolUtils.gSchoolPIN;
            query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormXI] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                    "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor],  \r\n" +
                    "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize],  \r\n" +
                    "  [Visibility]) VALUES ( 5, '" + address1.Trim().GetDBFormatString() + "', \r\n" +
                    "   111, 11, 24, 780, N'Blue', N'White', N'Center', N'Microsoft Sans Serif', N'Bold', 8.25, 1) \r\n";
            lstQuery.Add(query);

            query = " INSERT [dbo].[ReportHeaderConfigAdmissionFormXI] ( [SlNo], [HeaderText], [HeaderTop], \r\n" +
                    "  [HeaderLeft], [HeaderHeight], [HeaderWidth], [HeaderForColor], [HeaderBackColor],  \r\n" +
                    "  [HeaderHorizontalAlign], [HeaderFontName], [HeaderFontStyle], [HeaderFontSize], \r\n" +
                    "   [Visibility]) VALUES ( 6, N'', 53, 28, 80, 80, N'ControlText', N'InactiveBorder', \r\n" +
                    "    N'MiddleCenter', N'Microsoft Sans Serif', N'Regular', 8.25, 1) \r\n";
            lstQuery.Add(query);
            DBUpdate._mTotalTableUpdate += SQLHelper.GetInstance().ExecuteTransection(lstQuery) ? lstQuery.Count : 0;
        }





        public static void GenerateCertificateDB()
        {
            string query = " select top(1)Table_name from Information_SCHEMA.columns where Table_name='ReportHeaderConfig' \r\n";
            if (!SQLHelper.GetInstance().ExecuteScalar(query).ISValidObject())
            {
                query = "CREATE TABLE [dbo].[ReportHeaderConfig]( " +
                          "[ID] [int] IDENTITY(1,1) NOT NULL, " +
                          "[SlNo] [int] NOT NULL, " +
                          "[HeaderText] [varchar](max) NOT NULL, " +
                          "[HeaderTop] [int] NOT NULL, " +
                          "[HeaderLeft] [int] NOT NULL, " +
                          "[HeaderHeight] [int] NOT NULL, " +
                          "[HeaderWidth] [int] NOT NULL, " +
                          "[HeaderForColor] [varchar](50) NOT NULL, " +
                          "[HeaderBackColor] [varchar](50) NULL, " +
                          "[HeaderHorizontalAlign] [varchar](50) NULL, " +
                          "[HeaderFontName] [varchar](50) NULL, " +
                          "[HeaderFontStyle] [varchar](50) NULL, " +
                          "[HeaderFontSize] [float] NULL, " +
                          "[Visibility] [bit] NULL, " +
                          "CONSTRAINT [PK_ReportHeaderConfig] PRIMARY KEY CLUSTERED (  " +
                          "[SlNo] ASC " +
                          ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " +
                          ") ON [PRIMARY] ";
                if (SQLHelper.GetInstance().ExecuteQuery(query))
                {
                    DBUpdate._mTotalTableUpdate++;
                    InsertDataReportHeaderConfig();
                }

            }
            else
            {
                //////////for reportheaderconfig table update visibility column 
                string qry = "select * from Information_SCHEMA.columns where Table_name='ReportHeaderConfig' and column_name='Visibility'";
                object o1 = SQLHelper.GetInstance().ExecuteScalar(qry);
                if (o1 == null)
                {
                    string qury = "ALTER TABLE ReportHeaderConfig Add Visibility bit NULL";
                    if (SQLHelper.GetInstance().ExecuteQuery(qury))
                    {
                        DBUpdate._mTotalTableUpdate++;
                        qury = "update ReportHeaderConfig set Visibility='True'";
                        DBUpdate._mTotalTableUpdate += SQLHelper.GetInstance().ExecuteQuery(qury) ? 1 : 0;

                    }
                }
            }

        }
        private static void InsertDataReportHeaderConfig()
        {
            List<string> lstQuery = new List<string>();
            string contactDetails = SchoolUtils.gSchoolPhone + "                                                                                                                " + SchoolUtils.gSchoolEmail;
            string query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                     "values(0,'" + contactDetails + "',66,5,21,780,'WindowText','White','Center','Microsoft Sans Serif','Regular',9.25,'True')";
            lstQuery.Add(query);

            string schoolName = SchoolUtils.gSchoolName;
            query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(1,'" + schoolName + "',90,5,34,780,'Blue','White','Center','David','Bold',26,'True')";
            lstQuery.Add(query);

            query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(2,'HIGHER SECONDARY',122,5,24,780,'Blue','White','Center','Microsoft Sans Serif','Bold',12.25,'True')";
            lstQuery.Add(query);

            string estd = "ESTD- " + SchoolUtils.gSchoolESTD;
            query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(3,'" + estd + "',144,5,24,780,'Blue','White','Center','Microsoft Sans Serif','Bold',12,'True')";
            lstQuery.Add(query);

            string indexHS = "INDEX NO.- " + SchoolUtils.gSchoolIndexNo + "  ::  H.S. CODE- " + SchoolUtils.gSchoolHSCode;
            query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(4,'" + indexHS + "',167,5,24,780,'Blue','White','Center','Microsoft Sans Serif','Bold',11.25,'True')";
            lstQuery.Add(query);

            string address1 = "P.O.- " + SchoolUtils.gSchoolPO + "  ::  DIST.- " + SchoolUtils.gSchoolDist + "  ::  PIN- " + SchoolUtils.gSchoolPIN;
            query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(5,'" + address1 + "',189,5,24,780,'Blue','White','Center','Microsoft Sans Serif','Bold',10.25,'True')";
            lstQuery.Add(query);

            string address2 = "BLOCK- " + SchoolUtils.gSchoolBlock + "  ::  SUB. DIV.- " + SchoolUtils.gSchoolSubDivision;
            query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(6,'" + address2 + "',210,5,20,780,'Blue','White','Center','Microsoft Sans Serif','Bold',10.25,'True')";
            lstQuery.Add(query);

            query = "Insert into ReportHeaderConfig(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(7,'',125,25,100,100,'Blue','White','Center','Microsoft Sans Serif','Bold',8.25,'True')";
            lstQuery.Add(query);

            DBUpdate._mTotalTableUpdate += SQLHelper.GetInstance().ExecuteTransection(lstQuery) ? lstQuery.Count : 0;
        }

        public static void GenerateExamLandScapDB()
        {
            string query = " select top(1)Table_name from Information_SCHEMA.columns where Table_name='ReportHeaderExamLandscap' \r\n";
            if (!SQLHelper.GetInstance().ExecuteScalar(query).ISValidObject())
            {
                query = "CREATE TABLE [dbo].[ReportHeaderExamLandscap]( " +
                          "[ID] [int] IDENTITY(1,1) NOT NULL, " +
                          "[SlNo] [int] NOT NULL, " +
                          "[HeaderText] [varchar](max) NOT NULL, " +
                          "[HeaderTop] [int] NOT NULL, " +
                          "[HeaderLeft] [int] NOT NULL, " +
                          "[HeaderHeight] [int] NOT NULL, " +
                          "[HeaderWidth] [int] NOT NULL, " +
                          "[HeaderForColor] [varchar](50) NOT NULL, " +
                          "[HeaderBackColor] [varchar](50) NULL, " +
                          "[HeaderHorizontalAlign] [varchar](50) NULL, " +
                          "[HeaderFontName] [varchar](50) NULL, " +
                          "[HeaderFontStyle] [varchar](50) NULL, " +
                          "[HeaderFontSize] [float] NULL, " +
                            "[Visibility] [bit] NULL, " +
                          "CONSTRAINT [PK_ReportHeaderExamLandscap] PRIMARY KEY CLUSTERED (  " +
                          "[SlNo] ASC " +
                          ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " +
                          ") ON [PRIMARY] ";
                if (SQLHelper.GetInstance().ExecuteQuery(query))
                {
                    DBUpdate._mTotalTableUpdate++;
                    InsertDataReportHeaderExamLandscap();
                }
            }
        }
        private static void InsertDataReportHeaderExamLandscap()
        {
            List<string> lstQuery = new List<string>();
            string schoolName = SchoolUtils.gSchoolName;
            string query = "Insert into ReportHeaderExamLandscap(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                      "values(1,'" + schoolName + "',56,208,34,720,'Blue','White','Center','David','Bold',26,'True')";
            lstQuery.Add(query);

            string address1 = "P.O.- " + SchoolUtils.gSchoolPO + "  ::  DIST.- " + SchoolUtils.gSchoolDist + "  ::  PIN- " + SchoolUtils.gSchoolPIN;
            query = "Insert into ReportHeaderExamLandscap(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(2,'" + address1 + "',96,208,25,720,'Blue','White','Center','Microsoft Sans Serif','Bold',10.25,'True')";
            lstQuery.Add(query);

            query = "Insert into ReportHeaderExamLandscap(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(3,'',48,72,96,96,'Blue','White','Center','Microsoft Sans Serif','Bold',8.25,'True')";
            lstQuery.Add(query);

            DBUpdate._mTotalTableUpdate += SQLHelper.GetInstance().ExecuteTransection(lstQuery) ? lstQuery.Count() : 0;
        }

        public static void GenerateExamPortraitDB()
        {
            string query = " select top(1)Table_name from Information_SCHEMA.columns where Table_name='ReportHeaderExamPortrait' \r\n";
            if (!SQLHelper.GetInstance().ExecuteScalar(query).ISValidObject())
            {
                query = "CREATE TABLE [dbo].[ReportHeaderExamPortrait]( " +
                          "[ID] [int] IDENTITY(1,1) NOT NULL, " +
                          "[SlNo] [int] NOT NULL, " +
                          "[HeaderText] [varchar](max) NOT NULL, " +
                          "[HeaderTop] [int] NOT NULL, " +
                          "[HeaderLeft] [int] NOT NULL, " +
                          "[HeaderHeight] [int] NOT NULL, " +
                          "[HeaderWidth] [int] NOT NULL, " +
                          "[HeaderForColor] [varchar](50) NOT NULL, " +
                          "[HeaderBackColor] [varchar](50) NULL, " +
                          "[HeaderHorizontalAlign] [varchar](50) NULL, " +
                          "[HeaderFontName] [varchar](50) NULL, " +
                          "[HeaderFontStyle] [varchar](50) NULL, " +
                          "[HeaderFontSize] [float] NULL, " +
                            "[Visibility] [bit] NULL, " +
                          "CONSTRAINT [PK_ReportHeaderExamPortrait] PRIMARY KEY CLUSTERED (  " +
                          "[SlNo] ASC " +
                          ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " +
                          ") ON [PRIMARY] ";
                if (SQLHelper.GetInstance().ExecuteQuery(query))
                {
                    DBUpdate._mTotalTableUpdate++;
                    InsertDataReportHeaderExamPortrait();
                }
            }
        }
        private static void InsertDataReportHeaderExamPortrait()
        {
            List<string> lstQuery = new List<string>();

            string schoolName = SchoolUtils.gSchoolName;
            string query = "Insert into ReportHeaderExamPortrait(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                       "values(1,'" + schoolName + "',48,120,30,640,'Blue','White','Center','David','Bold',22,'True')";
            lstQuery.Add(query);

            string address1 = "P.O.- " + SchoolUtils.gSchoolPO + "  ::  DIST.- " + SchoolUtils.gSchoolDist + "  ::  PIN- " + SchoolUtils.gSchoolPIN;
            query = "Insert into ReportHeaderExamPortrait(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(2,'" + address1 + "',80,120,30,640,'Blue','White','Center','Microsoft Sans Serif','Bold',10.25,'True')";
            lstQuery.Add(query);

            query = "Insert into ReportHeaderExamPortrait(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(3,'',40,32,80,88,'Blue','White','Center','Microsoft Sans Serif','Bold',8.25,'True')";
            lstQuery.Add(query);

            DBUpdate._mTotalTableUpdate += SQLHelper.GetInstance().ExecuteTransection(lstQuery) ? lstQuery.Count() : 0;
        }


        public static void GenerateCertificateLandScapDoubleDB()
        {
            string query = " select top(1)Table_name from Information_SCHEMA.columns where Table_name='ReportHeaderCertificateLandscapDouble' \r\n";
            if (!SQLHelper.GetInstance().ExecuteScalar(query).ISValidObject())
            {
                query = "CREATE TABLE [dbo].[ReportHeaderCertificateLandscapDouble]( " +
                          "[ID] [int] IDENTITY(1,1) NOT NULL, " +
                          "[SlNo] [int] NOT NULL, " +
                          "[HeaderText] [varchar](max) NOT NULL, " +
                          "[HeaderTop] [int] NOT NULL, " +
                          "[HeaderLeft] [int] NOT NULL, " +
                          "[HeaderHeight] [int] NOT NULL, " +
                          "[HeaderWidth] [int] NOT NULL, " +
                          "[HeaderForColor] [varchar](50) NOT NULL, " +
                          "[HeaderBackColor] [varchar](50) NULL, " +
                          "[HeaderHorizontalAlign] [varchar](50) NULL, " +
                          "[HeaderFontName] [varchar](50) NULL, " +
                          "[HeaderFontStyle] [varchar](50) NULL, " +
                          "[HeaderFontSize] [float] NULL, " +
                            "[Visibility] [bit] NULL, " +
                          "CONSTRAINT [PK_ReportHeaderCertificateLandscapDouble] PRIMARY KEY CLUSTERED (  " +
                          "[SlNo] ASC " +
                          ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " +
                          ") ON [PRIMARY] ";
                if (SQLHelper.GetInstance().ExecuteQuery(query))
                {
                    DBUpdate._mTotalTableUpdate++;
                    InsertDataReportHeaderCertificateLandscapDouble();
                }
            }
        }
        private static void InsertDataReportHeaderCertificateLandscapDouble()
        {
            List<string> lstQuery = new List<string>();

            string schoolName = SchoolUtils.gSchoolName;
            string query = "Insert into ReportHeaderCertificateLandscapDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                      "values(1,'" + schoolName + "',50,40,30,496,'Blue','White','Center','David','Bold',22,'True')";
            lstQuery.Add(query);

            string estd = "ESTD- " + SchoolUtils.gSchoolESTD;
            query = "Insert into ReportHeaderCertificateLandscapDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(2,'" + estd + "',80,40,20,496,'Blue','White','Center','Microsoft Sans Serif','Bold',12,'True')";
            lstQuery.Add(query);

            string indexHS = "INDEX NO.- " + SchoolUtils.gSchoolIndexNo + "  ::  H.S. CODE- " + SchoolUtils.gSchoolHSCode;
            query = "Insert into ReportHeaderCertificateLandscapDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(3,'" + indexHS + "',97,40,20,496,'Blue','White','Center','Microsoft Sans Serif','Bold',11.25,'True')";
            lstQuery.Add(query);

            string address1 = "P.O.- " + SchoolUtils.gSchoolPO + "  ::  DIST.- " + SchoolUtils.gSchoolDist + "  ::  PIN- " + SchoolUtils.gSchoolPIN;
            query = "Insert into ReportHeaderCertificateLandscapDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(4,'" + address1 + "',115,40,20,496,'Blue','White','Center','Microsoft Sans Serif','Bold',10.25,'True')";
            lstQuery.Add(query);

            query = "Insert into ReportHeaderCertificateLandscapDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(5,'',74,40,56,64,'Blue','White','Center','Microsoft Sans Serif','Bold',8.25,'True')";
            lstQuery.Add(query);

            DBUpdate._mTotalTableUpdate += SQLHelper.GetInstance().ExecuteTransection(lstQuery) ? lstQuery.Count() : 0;
        }

        public static void GenerateCertificateTools()
        {
            string query = " select top(1)Table_name from Information_SCHEMA.columns where Table_name='ReportConfigCertificates' \r\n";
            if (!SQLHelper.GetInstance().ExecuteScalar(query).ISValidObject())
            {
                query = "CREATE TABLE [dbo].[ReportConfigCertificates]( " +
                          "[ID] [int] IDENTITY(1,1) NOT NULL, " +
                          "[ReportName] [varchar](50) NULL, " +
                          "[WaterMark] [bit] NULL, " +
                          "[Signature] [bit] NULL, " +
                          "[Subject1] [varchar](500) NULL, " +
                          "[Subject2] [varchar](500) NULL, " +
                          "[StudentPhoto] [bit] NULL, " +
                          "CONSTRAINT [PK_ReportConfigCertificates] PRIMARY KEY CLUSTERED (  " +
                          "[ID] ASC " +
                          ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " +
                          ") ON [PRIMARY] ";
                if (SQLHelper.GetInstance().ExecuteQuery(query))
                {
                    DBUpdate._mTotalTableUpdate++;
                    InsertDataReportConfigCertificates();
                }

            }
            else
            {
                ///// for fees
                query = "SELECT  ReportName FROM  ReportConfigCertificates WHERE ReportName = 'Fees'";
                object o1 = SQLHelper.GetInstance().ExecuteScalar(query);
                if (o1 == null)
                {
                    query = "Insert into ReportConfigCertificates(ReportName,WaterMark,Signature,Subject1,Subject2,StudentPhoto) " +
                         "values('Fees','False','False','','','True')";
                    DBUpdate._mTotalTableUpdate += SQLHelper.GetInstance().ExecuteQuery(query) ? 1 : 0;
                }
            }
        }
        private static void InsertDataReportConfigCertificates()
        {
            List<string> lstQuery = new List<string>();

            string query = "Insert into ReportConfigCertificates(ReportName,WaterMark,Signature,Subject1,Subject2,StudentPhoto) " +
                     "values('CharacterCertificate','False','False','','','True')";
            lstQuery.Add(query);

            query = "Insert into ReportConfigCertificates(ReportName,WaterMark,Signature,Subject1,Subject2,StudentPhoto) " +
                   "values('TransferCertificate','False','False','','','True')";
            lstQuery.Add(query);

            query = "Insert into ReportConfigCertificates(ReportName,WaterMark,Signature,Subject1,Subject2,StudentPhoto) " +
              "values('Exam','False','False','','','True')";
            lstQuery.Add(query);

            query = "Insert into ReportConfigCertificates(ReportName,WaterMark,Signature,Subject1,Subject2,StudentPhoto) " +
                        "values('Fees','False','False','','','True')";
            lstQuery.Add(query);

            DBUpdate._mTotalTableUpdate += SQLHelper.GetInstance().ExecuteTransection(lstQuery) ? lstQuery.Count() : 0;
        }

        public static void GenerateExamTools()
        {
            string query = " select top(1)Table_name from Information_SCHEMA.columns where Table_name='ReportConfigExam' \r\n";
            if (!SQLHelper.GetInstance().ExecuteScalar(query).ISValidObject())
            {
                query = "CREATE TABLE [dbo].[ReportConfigExam]( " +
                          "[ID] [int] IDENTITY(1,1) NOT NULL, " +
                          "[Class] [varchar](50) NULL, " +
                          "[Result] [bit] NULL, " +
                          "[GrandTotal] [bit] NULL, " +
                          "[Percentage] [bit] NULL, " +
                          "[Grade] [bit] NULL, " +
                          "[Rank] [bit] NULL, " +
                          "CONSTRAINT [PK_ReportConfigExam] PRIMARY KEY CLUSTERED (  " +
                          "[ID] ASC " +
                          ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " +
                          ") ON [PRIMARY] ";
                if (SQLHelper.GetInstance().ExecuteQuery(query))
                {
                    DBUpdate._mTotalTableUpdate++;
                    List<string> lstQuery = new List<string>();

                    query = "Insert into ReportConfigExam(Class,Result,GrandTotal,Percentage,Grade,Rank) " +
                            "values('All','True','True','True','True','True')";
                    lstQuery.Add(query);

                    DBUpdate._mTotalTableUpdate += SQLHelper.GetInstance().ExecuteTransection(lstQuery) ? lstQuery.Count : 0;
                }
            }
        }

        public static void GenerateReportHeaderFeesDoubleDB()
        {
            string query = " select top(1)Table_name from Information_SCHEMA.columns where Table_name='ReportHeaderFeesDouble' \r\n";
            if (!SQLHelper.GetInstance().ExecuteScalar(query).ISValidObject())
            {
                query = "CREATE TABLE [dbo].[ReportHeaderFeesDouble]( " +
                          "[ID] [int] IDENTITY(1,1) NOT NULL, " +
                          "[SlNo] [int] NOT NULL, " +
                          "[HeaderText] [varchar](max) NOT NULL, " +
                          "[HeaderTop] [int] NOT NULL, " +
                          "[HeaderLeft] [int] NOT NULL, " +
                          "[HeaderHeight] [int] NOT NULL, " +
                          "[HeaderWidth] [int] NOT NULL, " +
                          "[HeaderForColor] [varchar](50) NOT NULL, " +
                          "[HeaderBackColor] [varchar](50) NULL, " +
                          "[HeaderHorizontalAlign] [varchar](50) NULL, " +
                          "[HeaderFontName] [varchar](50) NULL, " +
                          "[HeaderFontStyle] [varchar](50) NULL, " +
                          "[HeaderFontSize] [float] NULL, " +
                            "[Visibility] [bit] NULL, " +
                          "CONSTRAINT [PK_ReportHeaderFeesDouble] PRIMARY KEY CLUSTERED (  " +
                          "[SlNo] ASC " +
                          ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " +
                          ") ON [PRIMARY] ";
                if (SQLHelper.GetInstance().ExecuteQuery(query))
                {
                    DBUpdate._mTotalTableUpdate++;
                    InsertDataReportHeaderFeesDouble();
                }
            }
        }
        private static void InsertDataReportHeaderFeesDouble()
        {
            List<string> lstQuery = new List<string>();

            string schoolName = SchoolUtils.gSchoolName.Trim().GetDBFormatString();
            string query = "Insert into ReportHeaderFeesDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                      "values(1,'" + schoolName + "',12,2,29,380,'Blue','White','Center','David','Bold',13,'True')";
            lstQuery.Add(query);

            string estd = "ESTD- " + SchoolUtils.gSchoolESTD;
            query = "Insert into ReportHeaderFeesDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(2,'" + estd + "',36,2,21,380,'Blue','White','Center','Arial','Bold',8,'True')";
            lstQuery.Add(query);

            string address1 = "P.O.- " + SchoolUtils.gSchoolPO + "  ::  DIST.- " + SchoolUtils.gSchoolDist + "  ::  PIN- " + SchoolUtils.gSchoolPIN;
            query = "Insert into ReportHeaderFeesDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(3,'" + address1.Trim().GetDBFormatString() + "',53,6,24,380,'Blue','White','Center','Arial','Bold',8,'True')";
            lstQuery.Add(query);

            query = "Insert into ReportHeaderFeesDouble(SlNo,HeaderText,HeaderTop,HeaderLeft,HeaderHeight,HeaderWidth,HeaderForColor,HeaderBackColor,HeaderHorizontalAlign,HeaderFontName,HeaderFontStyle,HeaderFontSize,Visibility) " +
                    "values(4,'',23,8,46,64,'Blue','White','Center','Microsoft Sans Serif','Bold',8.25,'True')";
            lstQuery.Add(query);

            DBUpdate._mTotalTableUpdate += SQLHelper.GetInstance().ExecuteTransection(lstQuery) ? lstQuery.Count : 0;
        }

        public static void AddFathersNameAndStudentImageShoworNot()
        {
            string query = "Select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS \r\n" +
"                     where  TABLE_NAME='ReportConfigExam' and COLUMN_NAME='IsShowFathersName' \r\n";
            if (!SQLHelper.GetInstance().ExecuteScalar(query).ISValidObject())
            {
                query = "ALTER TABLE ReportConfigExam ADD IsShowFathersName bit null,IsShowStudentImage bit null";
                if (!SQLHelper.GetInstance().ExecuteQuery(query))
                {
                    MessageBox.Show("Error At add IsShowFathersName.");

                }
                else
                {
                    DBUpdate._mTotalTableUpdate++;
                    query = "  Update ReportConfigExam set IsShowStudentImage='True',IsShowFathersName='False' \r\n";
                    if (!SQLHelper.GetInstance().ExecuteQuery(query))
                    {
                        MessageBox.Show("Error At updte IsShowStudentImage.");

                    }
                    else
                    {
                        DBUpdate._mTotalTableUpdate++;
                    }
                }
            }
        }


    }
}
