﻿using System;
using System.Data;
using System.IO;

namespace SP.Web.ReportHeader
{
    public static class ReportTools
    {
        public static bool gWaterMarkOnCertificate,
        gSignatureOnCertificate;

        public static bool gWaterMarkOnTransferCertificate,
        gSignatureOnTransferCertificate;

        public static bool gWaterMarkOnMarkSheet,
        gSignatureOnMarkSheet;

        public static bool gWaterMarkOnFees,
        gSignatureOnFees;

        public static bool CertificateLogoVisibility;

        public static bool gShowResult,
        gShowGrandTotal,
        gShowPercentage,
        gShowGrade,
        gShowRank;

        public static bool IsSowAttendenceForLandscape = false;
        public static bool IsSowAttendenceForPortrait = false;
        public static bool IsShowFathersNameLandscape = false;
        public static bool IsShowFathersNamePortrait = false;
        public static bool IsShowStudentImageLandscape = true;
        public static bool IsShowStudentImagePortrait = false;


        /// <summary>
        /// Report Item Visibility And UnVisibility declaration
        /// </summary>
        public static void GetReportConfiguretion()
        {
            GetReportConfigForCertificate();
            GetReportConfigForTransferCertificate();
            GetReportConfigForExam();
            ExamReportConfig();
            GetReportConfigForFees();

            //for School logo
            GetCertificateLogoVisibility();
        }
        /// <summary>
        /// Certificate Item Visibility And UnVisibility declaration
        /// </summary>
        private static void GetReportConfigForCertificate()
        {
            DataTable dt = new DataTable();
            string query = "Select * from ReportConfigCertificates WHERE ReportName='CharacterCertificate'";
            dt = SQLHelper.GetInstance().ExecuteNonQuery(query);
            if (dt.IsValidDataTable())
            {
                try
                {
                    gWaterMarkOnCertificate = bool.Parse(dt.Rows[0]["WaterMark"].ToString());
                    gSignatureOnMarkSheet = bool.Parse(dt.Rows[0]["Signature"].ToString());
                }
                catch (Exception)
                {
                }
            }
        }
        /// <summary>
        /// TransferCertificate Item Visibility And UnVisibility declaration
        /// </summary>
        private static void GetReportConfigForTransferCertificate()
        {
            DataTable dt = new DataTable();
            string query = "Select * from ReportConfigCertificates WHERE ReportName='TransferCertificate'";
            dt = SQLHelper.GetInstance().ExecuteNonQuery(query);
            if (dt.IsValidDataTable())
            {
                try
                {
                    gWaterMarkOnTransferCertificate = bool.Parse(dt.Rows[0]["WaterMark"].ToString());
                    gSignatureOnTransferCertificate = bool.Parse(dt.Rows[0]["Signature"].ToString());
                }
                catch (Exception)
                {
                }
            }
        }
        /// <summary>
        /// ForExam WaterMark And Signature Visibility And UnVisibility declaration
        /// </summary>
        private static void GetReportConfigForExam()
        {
            DataTable dt = new DataTable();
            string query = "Select * from ReportConfigCertificates WHERE ReportName='Exam'";
            dt = SQLHelper.GetInstance().ExecuteNonQuery(query);
            if (dt.IsValidDataTable())
            {
                try
                {
                    gWaterMarkOnMarkSheet = bool.Parse(dt.Rows[0]["WaterMark"].ToString());
                    gSignatureOnMarkSheet = bool.Parse(dt.Rows[0]["Signature"].ToString());
                }
                catch (Exception)
                {
                }
            }
        }
        /// <summary>
        /// ExamReport Other Item Visibility And UnVisibility declaration (EX:Result,Total,Rank etc)
        /// </summary>
        public static void ExamReportConfig()
        {
            DataTable dt = new DataTable();
            string query = "Select * from ReportConfigExam ";
            dt = SQLHelper.GetInstance().ExecuteNonQuery(query);
            if (dt.IsValidDataTable())
            {
                try
                {
                    gShowResult = bool.Parse(dt.Rows[0]["Result"].ToString());
                    gShowGrandTotal = bool.Parse(dt.Rows[0]["GrandTotal"].ToString());
                    gShowPercentage = bool.Parse(dt.Rows[0]["Percentage"].ToString());
                    gShowGrade = bool.Parse(dt.Rows[0]["Grade"].ToString());
                    gShowRank = bool.Parse(dt.Rows[0]["Rank"].ToString());
                    IsSowAttendenceForLandscape = bool.Parse(dt.Rows[0]["IsShowAttendence"].ToString());
                    IsSowAttendenceForPortrait = bool.Parse(dt.Rows[0]["IsShowAttendence"].ToString());

                    IsShowFathersNameLandscape = bool.Parse(dt.Rows[0]["IsShowFathersName"].ToString());
                    IsShowFathersNamePortrait = bool.Parse(dt.Rows[0]["IsShowFathersName"].ToString());

                    IsShowStudentImageLandscape= bool.Parse(dt.Rows[0]["IsShowStudentImage"].ToString());
                    IsShowStudentImagePortrait = bool.Parse(dt.Rows[0]["IsShowStudentImage"].ToString());
                }
                catch (Exception)
                {
                }
            }
        }
        /// <summary>
        /// Fees Item Visibility And UnVisibility declaration
        /// </summary>
        private static void GetReportConfigForFees()
        {
            DataTable dt = new DataTable();
            string query = "Select * from ReportConfigCertificates WHERE ReportName='Fees'";
            dt = SQLHelper.GetInstance().ExecuteNonQuery(query);
            if (dt.IsValidDataTable())
            {
                try
                {
                    gWaterMarkOnFees = bool.Parse(dt.Rows[0]["WaterMark"].ToString());
                    gSignatureOnFees = bool.Parse(dt.Rows[0]["Signature"].ToString());
                }
                catch (Exception)
                {
                }
            }

            ///for Singel or double print page config
            ///
            string path = Path.GetDirectoryName(Application.StartupPath) + "\\Config\\FeesPrint.txt";
            TextFileTools.ReadTextFile(path);
        }
        /// <summary>
        /// Certificate Logo  Visibility And UnVisibility declaration
        /// </summary>
        private static void GetCertificateLogoVisibility()
        {
            CertificateLogoVisibility = false;
            string query = "Select Visibility from ReportHeaderConfig where Slno='7'";
            object obj = SQLHelper.GetInstance().ExecuteScalar(query);
            if (obj != null)
            {
                CertificateLogoVisibility = Convert.ToBoolean((obj.ToString()).ToLower());
            }
        }
    }
}
