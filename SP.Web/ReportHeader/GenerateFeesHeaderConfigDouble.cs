﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using CrystalDecisions.CrystalReports.Engine;
using System.Data;

namespace SP.Web.ReportHeader
{
    class GenerateFeesHeaderConfigDouble
    {
        private static FontStyle mFontStyle;
        public static void SetHeader(ref TextObject txtHeader1, ref TextObject txtHeader2, ref TextObject txtHeader3,  ref BlobFieldObject pictureBox1, ref TextObject txtHeader5, ref TextObject txtHeader6, ref TextObject txtHeader7, ref BlobFieldObject pictureBox2)
        {
            try
            {
                Header1Preview(ref txtHeader1);
                Header2Preview(ref txtHeader2);
                Header3Preview(ref txtHeader3);
                if (pictureBox1 != null)
                {
                    Logo1Preview(ref pictureBox1);
                }

                Header5Preview(ref txtHeader5);
                Header6Preview(ref txtHeader6);
                Header7Preview(ref txtHeader7);
                Logo2Preview(ref pictureBox2);
            }
            catch (Exception)
            {
            }
        }

        private static void Header1Preview(ref TextObject mTxtHeader1)
        {
            //PictureObject picture1 = (PictureObject)cr.Section1.ReportObjects["picture1"];
            //picture1.ObjectFormat.EnableSuppress = false;
            //picture1.Left = 10000;
            string headerText = "", foreColor, backColor, align, fontName, fontStyle;
            int top, left, height, width;
            float fontSize;
            bool visibility;
            GetHeaderDetails(1, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);

            mTxtHeader1.Text = headerText;
            mTxtHeader1.Top = top * 15;
            mTxtHeader1.Width = width * 15;
            mTxtHeader1.Left = left * 15;
            mTxtHeader1.Height = height * 15;
            mTxtHeader1.Color = Color.FromName(foreColor);
            // mTxtHeader1.Border.BackgroundColor = Color.FromName(backColor);
            switch (align)
            {
                #region MyRegion
                case "Center":
                    mTxtHeader1.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.HorizontalCenterAlign;
                    break;
                case "Laft":
                    mTxtHeader1.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.LeftAlign;
                    break;
                case "Right":
                    mTxtHeader1.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.RightAlign;
                    break;
                default:
                    break;
                #endregion
            }
            switch (fontStyle)
            {
                #region MyRegion
                case "Regular":
                    mFontStyle = FontStyle.Regular;
                    break;
                case "Bold":
                    mFontStyle = FontStyle.Bold;
                    break;
                case "Italic":
                    mFontStyle = FontStyle.Italic;
                    break;
                case "Bold,Italic":
                    mFontStyle = FontStyle.Bold | FontStyle.Italic;
                    break;
                default:
                    break;
                #endregion
            }
            mTxtHeader1.ApplyFont(new Font(fontName, fontSize, mFontStyle));
        }

        private static void Header2Preview(ref TextObject mTxtHeader2)
        {
            string headerText = "", foreColor, backColor, align, fontName, fontStyle;
            int top, left, height, width;
            float fontSize;
            bool visibility;
            GetHeaderDetails(2, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);

            mTxtHeader2.Text = headerText;
            mTxtHeader2.Top = top * 15;
            mTxtHeader2.Width = width * 15;
            mTxtHeader2.Left = left * 15;
            mTxtHeader2.Height = height * 15;
            mTxtHeader2.Color = Color.FromName(foreColor);
            // mTxtHeader2.Border.BackgroundColor = Color.FromName(backColor);
            switch (align)
            {
                #region Align
                case "Center":
                    mTxtHeader2.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.HorizontalCenterAlign;
                    break;
                case "Laft":
                    mTxtHeader2.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.LeftAlign;
                    break;
                case "Right":
                    mTxtHeader2.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.RightAlign;
                    break;
                default:
                    break;
                #endregion
            }
            switch (fontStyle)
            {
                #region MyRegion
                case "Regular":
                    mFontStyle = FontStyle.Regular;
                    break;
                case "Bold":
                    mFontStyle = FontStyle.Bold;
                    break;
                case "Italic":
                    mFontStyle = FontStyle.Italic;
                    break;
                case "Bold,Italic":
                    mFontStyle = FontStyle.Bold | FontStyle.Italic;
                    break;
                default:
                    break;
                #endregion
            }
            mTxtHeader2.ApplyFont(new Font(fontName, fontSize, mFontStyle));
        }

        private static void Header3Preview(ref TextObject mTxtHeader3)
        {
            string headerText = "", foreColor, backColor, align, fontName, fontStyle;
            int top, left, height, width;
            float fontSize;

            bool visibility;
            GetHeaderDetails(3, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);

            mTxtHeader3.Text = headerText;
            mTxtHeader3.Top = top * 15;
            mTxtHeader3.Width = width * 15;
            mTxtHeader3.Left = left * 15;
            mTxtHeader3.Height = height * 15;
            mTxtHeader3.Color = Color.FromName(foreColor);
            //  mTxtHeader3.Border.BackgroundColor = Color.FromName(backColor);
            switch (align)
            {
                case "Center":
                    mTxtHeader3.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.HorizontalCenterAlign;
                    break;
                case "Laft":
                    mTxtHeader3.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.LeftAlign;
                    break;
                case "Right":
                    mTxtHeader3.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.RightAlign;
                    break;
                default:
                    break;
            }
            switch (fontStyle)
            {
                #region MyRegion
                case "Regular":
                    mFontStyle = FontStyle.Regular;
                    break;
                case "Bold":
                    mFontStyle = FontStyle.Bold;
                    break;
                case "Italic":
                    mFontStyle = FontStyle.Italic;
                    break;
                case "Bold,Italic":
                    mFontStyle = FontStyle.Bold | FontStyle.Italic;
                    break;
                default:
                    break;
                #endregion
            }
            mTxtHeader3.ApplyFont(new Font(fontName, fontSize, mFontStyle));
        }

        /// <summary>
        /// Picture Object
        /// </summary>
        /// 
        private static void Logo1Preview(ref BlobFieldObject mPicObject)
        {
            string headerText = "", foreColor, backColor, align, fontName, fontStyle;
            int top, left, height, width;
            float fontSize;
            bool visibility;
            GetHeaderDetails(5, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);

            // mTxtHeader7.Text = headerText;
            mPicObject.Top = top * 15;
            mPicObject.Width = width * 15;
            mPicObject.Left = left * 15;
            mPicObject.Height = height * 15;
            mPicObject.Border.BorderColor = Color.Gainsboro;
            mPicObject.ObjectFormat.EnableSuppress = !visibility;
            //mPicObject.Picture
            //mTxtHeader7.Color = Color.FromName(foreColor);
            //mTxtHeader7.Border.BackgroundColor = Color.FromName(backColor);
        }

        private static void Header5Preview(ref TextObject mTxtHeader1)
        {
            //PictureObject picture1 = (PictureObject)cr.Section1.ReportObjects["picture1"];
            //picture1.ObjectFormat.EnableSuppress = false;
            //picture1.Left = 10000;
            string headerText = "", foreColor, backColor, align, fontName, fontStyle;
            int top, left, height, width;
            float fontSize;
            bool visibility;
            GetHeaderDetails(1, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);

            mTxtHeader1.Text = headerText;
            mTxtHeader1.Top = top * 15;
            mTxtHeader1.Width = width * 15;
            mTxtHeader1.Left = (left * 15) + (400 * 15);
            mTxtHeader1.Height = height * 15;
            mTxtHeader1.Color = Color.FromName(foreColor);
            //mTxtHeader1.Border.BackgroundColor = Color.FromName(backColor);
            switch (align)
            {
                #region MyRegion
                case "Center":
                    mTxtHeader1.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.HorizontalCenterAlign;
                    break;
                case "Laft":
                    mTxtHeader1.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.LeftAlign;
                    break;
                case "Right":
                    mTxtHeader1.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.RightAlign;
                    break;
                default:
                    break;
                #endregion
            }
            switch (fontStyle)
            {
                #region MyRegion
                case "Regular":
                    mFontStyle = FontStyle.Regular;
                    break;
                case "Bold":
                    mFontStyle = FontStyle.Bold;
                    break;
                case "Italic":
                    mFontStyle = FontStyle.Italic;
                    break;
                case "Bold,Italic":
                    mFontStyle = FontStyle.Bold | FontStyle.Italic;
                    break;
                default:
                    break;
                #endregion
            }
            mTxtHeader1.ApplyFont(new Font(fontName, fontSize, mFontStyle));
        }

        private static void Header6Preview(ref TextObject mTxtHeader2)
        {
            string headerText = "", foreColor, backColor, align, fontName, fontStyle;
            int top, left, height, width;
            float fontSize;
            bool visibility;
            GetHeaderDetails(2, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);

            mTxtHeader2.Text = headerText;
            mTxtHeader2.Top = top * 15;
            mTxtHeader2.Width = width * 15;
            mTxtHeader2.Left = (left * 15) + (400 * 15);
            mTxtHeader2.Height = height * 15;
            mTxtHeader2.Color = Color.FromName(foreColor);
            //mTxtHeader2.Border.BackgroundColor = Color.FromName(backColor);
            switch (align)
            {
                #region Align
                case "Center":
                    mTxtHeader2.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.HorizontalCenterAlign;
                    break;
                case "Laft":
                    mTxtHeader2.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.LeftAlign;
                    break;
                case "Right":
                    mTxtHeader2.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.RightAlign;
                    break;
                default:
                    break;
                #endregion
            }
            switch (fontStyle)
            {
                #region MyRegion
                case "Regular":
                    mFontStyle = FontStyle.Regular;
                    break;
                case "Bold":
                    mFontStyle = FontStyle.Bold;
                    break;
                case "Italic":
                    mFontStyle = FontStyle.Italic;
                    break;
                case "Bold,Italic":
                    mFontStyle = FontStyle.Bold | FontStyle.Italic;
                    break;
                default:
                    break;
                #endregion
            }
            mTxtHeader2.ApplyFont(new Font(fontName, fontSize, mFontStyle));
        }

        private static void Header7Preview(ref TextObject mTxtHeader3)
        {
            string headerText = "", foreColor, backColor, align, fontName, fontStyle;
            int top, left, height, width;
            float fontSize;
            bool visibility;
            GetHeaderDetails(3, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);

            mTxtHeader3.Text = headerText;
            mTxtHeader3.Top = top * 15;
            mTxtHeader3.Width = width * 15;
            mTxtHeader3.Left = (left * 15) + (400 * 15);
            mTxtHeader3.Height = height * 15;
            mTxtHeader3.Color = Color.FromName(foreColor);
            // mTxtHeader3.Border.BackgroundColor = Color.FromName(backColor);
            switch (align)
            {
                case "Center":
                    mTxtHeader3.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.HorizontalCenterAlign;
                    break;
                case "Laft":
                    mTxtHeader3.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.LeftAlign;
                    break;
                case "Right":
                    mTxtHeader3.ObjectFormat.HorizontalAlignment = CrystalDecisions.Shared.Alignment.RightAlign;
                    break;
                default:
                    break;
            }
            switch (fontStyle)
            {
                #region MyRegion
                case "Regular":
                    mFontStyle = FontStyle.Regular;
                    break;
                case "Bold":
                    mFontStyle = FontStyle.Bold;
                    break;
                case "Italic":
                    mFontStyle = FontStyle.Italic;
                    break;
                case "Bold,Italic":
                    mFontStyle = FontStyle.Bold | FontStyle.Italic;
                    break;
                default:
                    break;
                #endregion
            }
            mTxtHeader3.ApplyFont(new Font(fontName, fontSize, mFontStyle));
        }

        /// <summary>
        /// Picture Object
        /// </summary>
        /// 
        private static void Logo2Preview(ref BlobFieldObject mPicObject)
        {
            string headerText = "", foreColor, backColor, align, fontName, fontStyle;
            int top, left, height, width;
            float fontSize;
            bool visibility;
            GetHeaderDetails(5, out headerText, out top, out left, out height, out width, out foreColor, out backColor, out align, out fontName, out fontStyle, out fontSize, out visibility);

            // mTxtHeader7.Text = headerText;
            mPicObject.Top = top * 15;
            mPicObject.Width = width * 15;
            mPicObject.Left = (left * 15) + (400 * 15);
            mPicObject.Height = height * 15;
            mPicObject.Border.BorderColor = Color.Gainsboro;
            mPicObject.ObjectFormat.EnableSuppress = !visibility;
            //mPicObject.Picture
            //mTxtHeader7.Color = Color.FromName(foreColor);
            //mTxtHeader7.Border.BackgroundColor = Color.FromName(backColor);
        }

        private static void GetHeaderDetails(int slno, out string headerText, out int top, out int left, out int height, out int width, out string foreColor, out string backColor, out string align, out string fontname, out string fontStyle, out float fontSize, out bool visibility)
        {
            headerText = ""; top = 0; left = 0; height = 0; width = 0;
            foreColor = ""; backColor = ""; align = ""; fontname = ""; fontStyle = ""; fontSize = 0f;
            visibility = false;
            string query = "Select * from ReportHeaderFeesDouble where Slno=" + slno + "";
            DataTable dt = SQLHelper.GetInstance().ExecuteNonQuery(query);
            if (dt.IsValidDataTable())
            {
                headerText = dt.Rows[0]["HeaderText"].ToString();
                top = int.Parse(dt.Rows[0]["HeaderTop"].ToString());
                left = int.Parse(dt.Rows[0]["HeaderLeft"].ToString());
                height = int.Parse(dt.Rows[0]["HeaderHeight"].ToString());
                width = int.Parse(dt.Rows[0]["HeaderWidth"].ToString());
                foreColor = dt.Rows[0]["HeaderForColor"].ToString();
                backColor = dt.Rows[0]["HeaderBackColor"].ToString();
                align = dt.Rows[0]["HeaderHorizontalAlign"].ToString();
                fontname = dt.Rows[0]["HeaderFontName"].ToString();
                fontStyle = dt.Rows[0]["HeaderFontStyle"].ToString();
                fontSize = float.Parse(dt.Rows[0]["HeaderFontSize"].ToString());
                visibility = bool.Parse(dt.Rows[0]["Visibility"].ToString());
            }
        }
    }
}
