﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SP.Web.Infrastructure.AjaxStructure
{
    public class AjaxFormIStruct
    {
        public static string AjaxBeginFormLoading { get => "Ajax_BeginForm_Loading"; }
        public static string AjaxActionLinkLoading { get => "Ajax_BeginForm_Loading"; }
    }
}