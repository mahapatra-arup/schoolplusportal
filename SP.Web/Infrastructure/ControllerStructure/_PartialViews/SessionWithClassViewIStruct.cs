﻿using SP.Data.Models.others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SP.Web.Infrastructure.ControllerStructure._PartialViews
{
    public class SessionWithClassViewIStruct
    {
        //Partial View
        public static ControllerStructureModel SessionWithClassAndSecView
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "SessionWithClassViews",
                ActionName = "SessionWithClassAndSecView",
                AreaName = "_PartialViews",
                ViewName = "SessionWithClassAndSecView",
                ViewPath = "~/Areas/_PartialViews/Views/SessionWithClassViews",
                Layout = ""
            };
        }
    }
}