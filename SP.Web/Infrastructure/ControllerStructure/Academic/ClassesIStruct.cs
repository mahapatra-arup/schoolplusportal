﻿using SP.Data.Models.others;
using SP.Web.Infrastructure.Layout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SP.Web.Infrastructure.ControllerStructure.Academic
{
    public class ClassesIStruct
    {
        
        public static ControllerStructureModel CreateAndEdit
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "Classes",
                ActionName = "CreateAndEdit",
                AreaName = "Academic",
                ViewName = "CreateAndEdit",
                ViewPath = "~/Areas/Academic/Views/Classes",
                Layout = "~/Areas/Academic/Views/Shared/_ClassIndex.cshtml"
            };
        }
    }
}