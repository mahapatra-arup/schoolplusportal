﻿using SP.Data.Models.others;
using SP.Web.Infrastructure.Layout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SP.Web.Infrastructure.ControllerStructure.Academic
{
    /// <summary>
    /// SectionInfraStructure
    /// </summary>
    public class SectionIStruct
    {
        public static ControllerStructureModel SectionView
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "Sections",
                ActionName = "SectionView",
                AreaName = "Academic",
                ViewName = "SectionView",
                ViewPath = "~/Areas/Academic/Views/Sections",
                Layout = "~/Areas/Academic/Views/Shared/_SectionsIndex.cshtml"
            };
        }
        public static ControllerStructureModel CreateAndEdit
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "Sections",
                ActionName = "CreateAndEdit",
                AreaName = "Academic",
                ViewName = "CreateAndEdit",
                ViewPath = "~/Areas/Academic/Views/Sections",
                Layout = "~/Areas/Academic/Views/Shared/_SectionsIndex.cshtml"
            };
        }
    }
}