﻿using SP.Data.Models.others;
using SP.Web.Infrastructure.Layout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SP.Web.Infrastructure.ControllerStructure.Account
{
    public class LedgerIStruct
    {
        public static ControllerStructureModel Index
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "Ledgers",
                ActionName = "Index",
                AreaName = "Account",
                ViewName = "Index",
                ViewPath = "~/Areas/Account/Views/Index",
                Layout = LayoutIStruct._AdminPanelLayout
            };
        }
        public static ControllerStructureModel LedgerAddEdit
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "Ledgers",
                ActionName = "LedgerAddEdit",
                AreaName = "Account",
                ViewName = "LedgerAddEdit",
                ViewPath = "~/Areas/Account/Views/Ledgers",
                Layout = LayoutIStruct._AdminPanelLayout
            };
        }
    }
}