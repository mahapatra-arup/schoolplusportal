﻿using SP.Data.Models.others;
using SP.Web.Infrastructure.Layout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SP.Web.Infrastructure.ControllerStructure.Fees
{
    public class FeesIStruct
    {
        #region ==FeesCollection==
        public static ControllerStructureModel Index
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "FeesCollect",
                ActionName = "Index",
                AreaName = "Fees",
                ViewName = "Index",
                ViewPath = "~/Areas/Fees/Views/FeesCollect",
                Layout = LayoutIStruct._FeesListLayout
            };
        }

        public static ControllerStructureModel PartialIndex
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "FeesCollect",
                ActionName = "PartialIndex",
                AreaName = "Fees",
                ViewName = "PartialIndex",
                ViewPath = "~/Areas/Fees/Views/FeesCollect",
            };
        }


        public static ControllerStructureModel FeesCollection
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "FeesCollect",
                ActionName = "FeesCollection",
                AreaName = "Fees",
                ViewName = "FeesCollection",
                ViewPath = "~/Areas/Fees/Views/FeesCollect",
                Layout = LayoutIStruct._AdminPanelLayout
            };
        }
        #endregion
       
    }
}