﻿using SP.Data.Models.others;
using SP.Web.Infrastructure.Layout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SP.Web.Infrastructure.ControllerStructure.Fees
{
    public class FeesBillIStruct
    {
        public static ControllerStructureModel Index
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "FeesBill",
                ActionName = "Index",
                AreaName = "Fees",
                ViewName = "Index",
                ViewPath = "~/Areas/Fees/Views/FeesBill",
                Layout = LayoutIStruct._AdminPanelLayout
            };
        }

        public static ControllerStructureModel CustomFeesBill
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "FeesBill",
                ActionName = "CustomFeesBill",
                AreaName = "Fees",
                ViewName = "CustomFeesBill",
                ViewPath = "~/Areas/Fees/Views/FeesBill",
                Layout = LayoutIStruct._AdminPanelLayout
            };
        }

        public static ControllerStructureModel StudentFeesDetails
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "FeesBill",
                ActionName = "StudentFeesDetails",
                AreaName = "Fees",
                ViewName = "StudentFeesDetails",
                ViewPath = "~/Areas/Fees/Views/FeesBill",
                Layout = null
            };
        }
    }
}