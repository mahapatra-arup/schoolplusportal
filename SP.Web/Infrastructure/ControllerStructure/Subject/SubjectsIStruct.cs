﻿using SP.Data.Models.others;
using SP.Web.Infrastructure.Layout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SP.Web.Infrastructure.ControllerStructure.Subject
{
    public class SubjectsIStruct
    {

        #region Mp
        public static ControllerStructureModel AddEditMpSubjects
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "Subjects",
                ActionName = "AddEditMpSubjects",
                AreaName = "Subject",
                ViewName = "AddEditMpSubjects",
                ViewPath = "~/Areas/Subject/Views/Subjects",
                Layout = LayoutIStruct._SubjectMenuLayout
            };
        }

        public static ControllerStructureModel DeleteMpSubjects
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "Subjects",
                ActionName = "DeleteMpSubjects",
                AreaName = "Subject",
                ViewName = "DeleteMpSubjects",
                ViewPath = "~/Areas/Subject/Views/Subjects",
                Layout = LayoutIStruct._SubjectMenuLayout
            };
        }

        public static ControllerStructureModel MpIndex
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "Subjects",
                ActionName = "MpIndex",
                AreaName = "Subject",
                ViewName = "MpIndex",
                ViewPath = "~/Areas/Subject/Views/Subjects",
                Layout = LayoutIStruct._SubjectMenuLayout
            };
        }
        #endregion

        #region Hs
        public static ControllerStructureModel AddEditHsSubjects
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "Subjects",
                ActionName = "AddEditHsSubjects",
                AreaName = "Subject",
                ViewName = "AddEditHsSubjects",
                ViewPath = "~/Areas/Subject/Views/Subjects",
                Layout = LayoutIStruct._SubjectMenuLayout
            };
        }

        public static ControllerStructureModel DeleteHsSubjects
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "Subjects",
                ActionName = "DeleteHsSubjects",
                AreaName = "Subject",
                ViewName = "DeleteHsSubjects",
                ViewPath = "~/Areas/Subject/Views/Subjects",
                Layout = LayoutIStruct._SubjectMenuLayout
            };
        }

        public static ControllerStructureModel HsIndex
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "Subjects",
                ActionName = "HsIndex",
                AreaName = "Subject",
                ViewName = "HsIndex",
                ViewPath = "~/Areas/Subject/Views/Subjects",
                Layout = LayoutIStruct._SubjectMenuLayout
            };
        }
        #endregion
    }
}
