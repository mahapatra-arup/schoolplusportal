﻿using SP.Data.Models.others;
using SP.Web.Infrastructure.Layout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SP.Web.Infrastructure.ControllerStructure.Student
{
    public class StdAdmissionIStruct
    {

        #region ==Create==
        public static ControllerStructureModel StdDetailsCreate
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "StdAdmission",
                ActionName = "StdDetailsCreate",
                AreaName = "Student",
                ViewName = "StdDetailsCreate",
                ViewPath = "~/Areas/Student/Views/StdAdmission",
                Layout = LayoutIStruct._AdminPanelLayout
            };
        }


        public static ControllerStructureModel StdBankCreate
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "StdAdmission",
                ActionName = "StdBankCreate",
                AreaName = "Student",
                ViewName = "StdBankCreate",
                ViewPath = "~/Areas/Student/Views/StdAdmission",
                Layout = ""
            };
        } 
        #endregion
    }
}

