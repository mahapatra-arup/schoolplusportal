﻿using SP.Data.Models.others;
using SP.Web.Infrastructure.Layout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SP.Web.Infrastructure.ControllerStructure.Student
{
    public class StdReAdmissionIStruct
    {
        public static ControllerStructureModel StdReadmissionManually
        {
            get => new ControllerStructureModel()
            {
                ControllerName = "StdReadmission",
                ActionName = "StdReadmissionManually",
                AreaName = "Student",
                ViewName = "StdReadmissionManually",
                ViewPath = "~/Areas/Student/Views/StdReadmission",
                Layout = LayoutIStruct._AdminPanelLayout
            };
        }
    }
}