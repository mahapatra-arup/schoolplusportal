﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SP.Web.Infrastructure.Layout
{
    public class LayoutIStruct
    {
        public static string _AdminPanelLayout { get => "~/Views/Shared/_Layout.cshtml"; }
        public static string _StudentEditLayout { get => "~/Areas/Student/Views/Shared/_StudentEditLayout.cshtml"; }
        public static string _SubjectMenuLayout { get => "~/Areas/Subject/Views/Shared/_SubjectMenuLayout.cshtml"; }
        public static string _FeesListLayout { get => "~/Areas/Fees/Views/Shared/_FeesListLayout.cshtml"; }
    }
}