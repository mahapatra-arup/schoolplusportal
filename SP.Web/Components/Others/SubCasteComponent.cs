﻿using SP.Data;
using SP.Data.Models;
using SP.Data.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SP.Web.Components.Others
{
    public class SubCasteComponent
    {
        SubCasteTools subCasteTools = new SubCasteTools();

        public async Task<SelectList> CmbSubCastesAsync(string selectedValue = "")
        {
            return new SelectList(await subCasteTools.GetSubCasteAsync(),
                FieldPropertiesExtension.GetMemberName((SubCaste c) => c.Id),
                FieldPropertiesExtension.GetMemberName((SubCaste c) => c.SubCasteName), selectedValue);
        }
    }
}