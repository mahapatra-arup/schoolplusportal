﻿using SP.Data;
using SP.Data.Models;
using SP.Data.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SP.Web.Components.Others
{
    public class NationalityComponent
    {
        NationalityTools nationalityTools = new NationalityTools();

        public async Task<SelectList> CmbNationalitiesAsync(string selectedValue = "")
        {
            return new SelectList(await nationalityTools.GetNationalitiesAsync(),
                FieldPropertiesExtension.GetMemberName((Nationality c) => c.Id),
                FieldPropertiesExtension.GetMemberName((Nationality c) => c.NationalityName), selectedValue);
        }
    }
}