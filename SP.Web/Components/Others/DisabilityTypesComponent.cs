﻿using SP.Data;
using SP.Data.Models;
using SP.Data.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SP.Web.Components.Others
{
    public class DisabilityTypesComponent
    {
        DisabilityTypesTools disabilityTypesTools = new DisabilityTypesTools();

        public async Task<SelectList> CmbDisabilityTypesAsync(string selectedValue = "")
        {
            return new SelectList(await disabilityTypesTools.GetDisabilityTypesAsync(),
                FieldPropertiesExtension.GetMemberName((DisabilityType c) => c.Id),
                FieldPropertiesExtension.GetMemberName((DisabilityType c) => c.CatagoryName), selectedValue);
        }
    }
}