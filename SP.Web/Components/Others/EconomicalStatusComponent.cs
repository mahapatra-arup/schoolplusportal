﻿using SP.Data;
using SP.Data.Models;
using SP.Data.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SP.Web.Components.Others
{
    public class EconomicalStatusComponent
    {
        EconomicalStatusTools economicalStatusTools = new EconomicalStatusTools();

        public async Task<SelectList> CmbEconomicalStatusAsync(string selectedValue = "")
        {
            return new SelectList(await economicalStatusTools.GetEconomicalStatusAsync(),
                FieldPropertiesExtension.GetMemberName((EconomicalStatu c) => c.Id),
                FieldPropertiesExtension.GetMemberName((EconomicalStatu c) => c.EconomicalStatusName), selectedValue);
        }
    }
}