﻿using SP.Data;
using SP.Data.Models;
using SP.Data.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SP.Web.Components.Others
{
    public class ReligionComponent
    {
        ReligionTools religionTools = new ReligionTools();

        public async Task<SelectList> CmbReligionsAsync(string selectedValue = "")
        {
            return new SelectList(await religionTools.GetReligionsAsync(),
                FieldPropertiesExtension.GetMemberName((Religion c) => c.Id),
                FieldPropertiesExtension.GetMemberName((Religion c) => c.Religion1), selectedValue);
        }
    }
}