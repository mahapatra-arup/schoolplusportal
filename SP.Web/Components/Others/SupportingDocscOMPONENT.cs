﻿using SP.Data;
using SP.Data.Models;
using SP.Data.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SP.Web.Components.Others
{
    public class SupportingDocsComponent
    {
        SupportingDocsTools supportingDocsTools = new SupportingDocsTools();

        public async Task<SelectList> CmbSupportingDocsAsync(string selectedValue = "")
        {
            return new SelectList(await supportingDocsTools.GetSupportingDocsAsync(),
                FieldPropertiesExtension.GetMemberName((SupportingDoc c) => c.Id),
                FieldPropertiesExtension.GetMemberName((SupportingDoc c) => c.DocumentName), selectedValue);
        }
    }
}