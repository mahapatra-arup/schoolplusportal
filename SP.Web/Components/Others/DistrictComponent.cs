﻿using SP.Data;
using SP.Data.Models;
using SP.Data.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SP.Web.Components.Others
{
    public class DistrictComponent
    {
        DistrictTools districtTools = new DistrictTools();

        public async Task<SelectList> CmbDistrictsAsync(string selectedValue = "")
        {
            return new SelectList(await districtTools.GetDistrictAsync(),
                FieldPropertiesExtension.GetMemberName((District c) => c.Id),
                FieldPropertiesExtension.GetMemberName((District c) => c.DistName), selectedValue);
        }
    }
}