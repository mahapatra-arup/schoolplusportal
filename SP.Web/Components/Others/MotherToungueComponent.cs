﻿using SP.Data;
using SP.Data.Models;
using SP.Data.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SP.Web.Components.Others
{
    public class MotherToungueComponent
    {
        CasteTools casteTools = new CasteTools();

        public async Task<SelectList> CmbCastesAsync(string selectedValue = "")
        {
            return new SelectList(await casteTools.GetCasteAsync(),
                FieldPropertiesExtension.GetMemberName((Caste c) => c.Id),
                FieldPropertiesExtension.GetMemberName((Class c) => c.ClassName), selectedValue);
        }
    }
}