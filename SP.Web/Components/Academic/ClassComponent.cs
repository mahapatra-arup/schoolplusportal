﻿using SP.Data;
using SP.Data.Academic;
using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SP.Web.Components.Academic
{
    public class ClassComponent
    {
        //Object
        private SP_DataEntities db = new SP_DataEntities();
        ClassTools classTools = new ClassTools();

        /// <summary>
        /// Get Class use by session
        /// </summary>
        /// <param name="StudentID"></param>
        /// <returns></returns>
        public async Task<SelectList> CmbClassAsync(int MpSessionId, int HsSessionId,string selectedValue="")
        {
            return new SelectList(await classTools.GetClassAsync(MpSessionId, HsSessionId),
                FieldPropertiesExtension.GetMemberName((Class c) => c.Id),
                FieldPropertiesExtension.GetMemberName((Class c) => c.ClassName), selectedValue);
        }
        public async Task<SelectList>  CmbMpClassAsync(int MpSessionId, string selectedValue = "")
        {
            return new SelectList(await classTools.GetMpClassAsync(MpSessionId),
                FieldPropertiesExtension.GetMemberName((Class c) => c.Id),
                FieldPropertiesExtension.GetMemberName((Class c) => c.ClassName), selectedValue);
        }

        public async Task<SelectList> CmbHsClassAsync(int HsSessionId, string selectedValue = "")
        {
            return new SelectList(await classTools.GetHsClassAsync(HsSessionId),
                FieldPropertiesExtension.GetMemberName((Class c) => c.Id),
                FieldPropertiesExtension.GetMemberName((Class c) => c.ClassName), selectedValue);
        }
    }
}