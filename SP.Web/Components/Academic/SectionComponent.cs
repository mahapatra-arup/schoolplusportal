﻿using SP.Data;
using SP.Data.Academic.Tools;
using SP.Data.Models;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SP.Web.Components.Academic
{
    public class SectionComponent
    {

        private SP_DataEntities db = new SP_DataEntities();
        SectionTools _sectionTools = new SectionTools();

        /// <summary>
        /// Get Section Use [Class]
        /// </summary>
        /// <param name="StudentID"></param>
        /// <returns></returns>
        public async Task<SelectList> CmbSectionAsync(long? ClassID)
        {
            return new SelectList(await _sectionTools.GetSectionsAsync(ClassID),
                FieldPropertiesExtension.GetMemberName((Section c) => c.ID),
                FieldPropertiesExtension.GetMemberName((Section c) => c.SectionName));
        }

    }
}