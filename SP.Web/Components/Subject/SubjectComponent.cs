﻿using SP.Data;
using SP.Data.Academic;
using SP.Data.Models;
using SP.Data.Subject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SP.Web.Components.Subject
{
    public class SubjectComponent
    {
        //Object
        private SP_DataEntities db = new SP_DataEntities();
        SubjectTools subjectTools = new SubjectTools();


        #region ==mp==
        public async Task<SelectList> CmbMpSubjectAsync(int MpSessionId, string selectedValue = "")
        {
            return new SelectList(await subjectTools.GetMPSubjectAsync(MpSessionId),
                FieldPropertiesExtension.GetMemberName((MPSubject c) => c.ID),
                FieldPropertiesExtension.GetMemberName((MPSubject c) => c.SubjectName), selectedValue);
        }

        public async Task<SelectList> CmbMpSubjectNotInCombinationSubjectAsync(int MpSessionId, string selectedValue = "")
        {
            return new SelectList(await subjectTools.GetMPSubNotInCombinationSubAsync(MpSessionId),
                FieldPropertiesExtension.GetMemberName((MPSubject c) => c.ID),
                FieldPropertiesExtension.GetMemberName((MPSubject c) => c.SubjectName), selectedValue);
        }
        #endregion

        #region ==HS==
        public async Task<SelectList> CmbHsLabSubjectAsync( string selectedValue = "")
        {
            return new SelectList(await subjectTools.GetHSLabSubjectAsync(),
                FieldPropertiesExtension.GetMemberName((HSSubject c) => c.Id),
                FieldPropertiesExtension.GetMemberName((HSSubject c) => c.SubjectName), selectedValue);
        }

        #endregion
    }
}