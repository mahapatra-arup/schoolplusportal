﻿using SP.Data;
using SP.Data.Models;
using SP.Data.Subject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SP.Web.Components.Subject
{
    public class SubjectGroupComponent
    {
        //Object
        private SP_DataEntities db = new SP_DataEntities();
        SubjectsGroupTools subjectGrpTools = new SubjectsGroupTools();


        public async Task<SelectList> CmbSubjectGroupAsync(string selectedValue = "")
        {
            return new SelectList(await subjectGrpTools.GetSubjectsGroupAsync(),
                FieldPropertiesExtension.GetMemberName((SubjectsGroup c) => c.Id),
                FieldPropertiesExtension.GetMemberName((SubjectsGroup c) => c.GroupName), selectedValue);
        }
    }
}