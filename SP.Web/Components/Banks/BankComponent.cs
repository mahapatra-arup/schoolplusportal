﻿using SP.Data;
using SP.Data.Banks.Tools;
using SP.Data.Models;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SP.Web.Components.Banks
{
    public class BankComponent
    {
        BankTools bankTools = null;
        public BankComponent()
        {
            bankTools = new BankTools();
        }

        public async Task<SelectList> CmbBanksAsync(string selectedValue = "")
        {
            return new SelectList(await bankTools.GetBanksAsync(),
                FieldPropertiesExtension.GetMemberName((Bank c) => c.ID),
                FieldPropertiesExtension.GetMemberName((Bank c) => c.BankName), selectedValue);
        }
    }
}