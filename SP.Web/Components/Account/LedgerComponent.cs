﻿using SP.Data;
using SP.Data.Account.Models;
using SP.Data.Account.Tools;
using SP.Data.Fees;
using SP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static SP.Data.Enums.StudentsEnum;
using static SP.Data.Enums.StudentsFeesEnum;

namespace SP.Web.Components.Account
{
    public class LedgerComponent
    {
        //Object
        private SP_DataEntities db = new SP_DataEntities();
        LedgerTools ledgerTools = new LedgerTools();
        //Fixed
        LedgerCategories ledgerCategories = new LedgerCategories();
        LedgerType ledgerType = new LedgerType();
        FeesStructureUtils feesStructureUtils = new FeesStructureUtils();

        /// <summary>
        ///  _Category Like : LedgerCategories.FUND,LedgerCategories.Fees
        /// </summary>
        /// <param name="_Category"></param>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        public async Task<SelectList> CmbLedgerByCategoryAsync(string _Category,string selectedValue = "")
        {
            IEnumerable<Ledger> data = await ledgerTools.GetLedgerByCategoryAsync(_Category);
           // data = data.IsValidList() ? data : new List<Ledger>();
            return new SelectList(data,
                FieldPropertiesExtension.GetMemberName((Ledger c) => c.LedgerId),
                FieldPropertiesExtension.GetMemberName((Ledger c) => c.LedgerName), selectedValue);
        }
        /// <summary>
        /// Source from Class
        /// </summary>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        public SelectList CmbLedgerCategory(string selectedValue = "")
        {
            List<string> lst = new List<string>();
            var fields = ledgerCategories.GetType().GetProperties();
            foreach (var item in fields)
            {
              var value=  item.GetValue(ledgerCategories,null).ConvertObjectToString();
                lst.Add(value);
            }

            // Build a List<SelectListItem>
            var selectListItems = lst.Select(x => new SelectListItem() { Value = x, Text = x }).ToList();

            return new SelectList(selectListItems, "Value", "Text");
        }

        public SelectList CmbLedgerType(string selectedValue = "")
        {
            List<string> lst = new List<string>();
            var fields = ledgerType.GetType().GetProperties();
            foreach (var item in fields)
            {
                var value = item.GetValue(ledgerType, null).ConvertObjectToString();
                lst.Add(value);
            }

            // Build a List<SelectListItem>
            var selectListItems = lst.Select(x => new SelectListItem() { Value = x, Text = x }).ToList();

            return new SelectList(selectListItems, "Value", "Text");
        }

       
        public async Task<SelectList> CmbLedgerByClassIdsync(long ClassId, _AdmissionType _AdmissionType, CustomeAndFineFees _CustomeAndFineFees, string selectedValue = "")
        {
            var data = (await feesStructureUtils.GetFeesStructureView(ClassId, _AdmissionType,  _CustomeAndFineFees));
            
            return new SelectList(data,
                FieldPropertiesExtension.GetMemberName((Ledger c) => c.LedgerId),
                FieldPropertiesExtension.GetMemberName((Ledger c) => c.LedgerName), selectedValue);
        }
    }
}