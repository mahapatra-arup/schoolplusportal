﻿
/*
* ---------------------
* FeesStructure <Edit> <Update> <Delete> Process
* ---------------------
*/
$(function () {

    //Save Data API-
    var FeesStructureurl = $("#tblFeesStructure").data('request-url');

    ////Remove the dummy row if data present.
    //if ($("#tblFeesStructure tr").length > 2) {
    //    // $("#tblFeesStructure tr:eq(0)").remove();
    //} else {
    //    var row = $("#tblFeesStructure tbody tr:last-child");
    //    row.find(".Edit").show();
    //}

    //Edit Click event .
    $("body").on("click", "#tblFeesStructure tbody .Edit", function () {
        //row
        var row = $(this).closest("tr");

         //Enable Input
        $("td", row).each(function () {
            if ($(this).find("input").length > 0) {
                //Enable
                $(this).find("input").prop("disabled", false);
            }
        });

        //Hide/show
        row.find(".Update").show();
        row.find(".Cancel").show();
        $(this).hide();
    });

    //Update Click event .
    $("body").on("click", "#tblFeesStructure tbody .Update", function () {
        //row
        var row = $(this).closest("tr");

         //Disable Input
        $("td", row).each(function () {
            if ($(this).find("input").length > 0) {

                var input = $(this).find("input");
                //Input Disable
                input.prop("disabled", true);
            }
        });

        //Hide Show
        row.find(".Edit").show();
        row.find(".Cancel").hide();
        $(this).hide();


        //Data Get And FIll
        var feesStructureVM = {};
        //Class
        feesStructureVM.ClassId = Number(row.find(".ClassId").find("span").html());
        feesStructureVM.ClassName = row.find(".ClassName").find("span").html();

        //FeesStructure
        feesStructureVM.FeesStructureID = row.find(".FeesStructureID").find("span").html();



        feesStructureVM.IsAdmission = row.find(".IsAdmission").find("input").is(':checked');
        feesStructureVM.IsReadmission = row.find(".IsReadmission").find("input").is(':checked');
        feesStructureVM.IsFine = row.find(".IsFine").find("input").is(':checked');

        feesStructureVM.IsCustom = row.find(".IsCustom").find("input").is(':checked');
        feesStructureVM.IsMonthly = row.find(".IsMonthly").find("input").is(':checked');/*OR:: row.find("input[name=IsMonthly]").is(':checked')*/


         

        //Ledger
        var eLedgerId = document.getElementById("LedgerId");
        feesStructureVM.LedgerId = eLedgerId.options[eLedgerId.selectedIndex].value;;

        //Subject
        var eSubId = document.getElementById("SubjectId");
        var subValue = eSubId.options[eSubId.selectedIndex].value;
        feesStructureVM.SubjectId = subValue;

        //FeesAmount
        feesStructureVM.FeesAmount = row.find(".FeesAmount").find("input").val();


        //Ajax Call
        $.ajax({
            type: "POST",
            url: FeesStructureurl,
            data: '{FeesStructureVM: ' + JSON.stringify(feesStructureVM) + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response != null) {
                    row.find(".FeesStructureID").find("span").html(response.FeesStructureID);
                } else {
                    alert("Something went wrong");
                }
            },
            failure: function (response) {
                alert(response.responseText);

            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    });

    //Cancel Click event 
    $("body").on("click", "#tblFeesStructure tbody .Cancel", function () {
        //row
        var row = $(this).closest("tr");

        //Disable Input
        $("td", row).each(function () {
            if ($(this).find("input").length > 0) {

                var input = $(this).find("input");
                //Input Disable
                input.prop("disabled", true);
            }
        });

        //Hide Show
        row.find(".Edit").show();
        row.find(".Update").hide();
        $(this).hide();
    });

});
//-----END-------


/*
* ---------------------
* lab subject Process
* ---------------------
*/
//Load time Hide /show Subject View
$().ready(function () {
    var radioValue = $("input[name='chkLab']:checked").val();
    if (radioValue == "Individual") {
        $("#labSubjectGroup").show();
    }
    else {
        $("#labSubjectGroup").hide();
    }
});

//After radio button Check Click
$(document).ready(function () {
    $('input[type="radio"]').click(function () {
        var radioValue = $("input[name='chkLab']:checked").val();
        if ("Individual" == radioValue) {
            $("#labSubjectGroup").show();
        }
        else {
            // sets selected index to first item using jQuery (can work on multiple elements)(For Lab Subject)
            $("select#SubjectId").prop('selectedIndex', 0);
            $("#labSubjectGroup").hide();
        }
    });
});
//-------END------





//Window open() Method
//function OpenMessegeWindow(data) {
//    var myWindow = window.open("", "MsgWindow", "width=800,height=800");
//    myWindow.document.write(data);
//}
