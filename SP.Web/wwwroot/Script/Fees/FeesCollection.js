﻿
/*
 * ----------------------
* CalCulate Fees Row Wise
 * ----------------------
*/
$(function () {
    //all row get
    var $tblrows = $("#tblCompulsoryFees tbody tr");

    //Create Event
    $tblrows.each(function (index) {
        var $tblrow = $(this);

        //.PaidFees, .Concession KeyUp
        $tblrow.find('.PaidFees,.Concession').on('keyup', function () {
           // var _keyupval = $(this).val();
          //  alert(_keyupval);
            CalculateFees($tblrow);
        });

        //PaidFees KeyPress Event
        $tblrow.find('.PaidFees').on('keypress', function (event) {

            //KeyCode to  ChareCode COnvert
            var _keypressval = String.fromCharCode(event.which);

            //Keypress Val
            var _Fees = parseFloat(_keypressval);
            _Fees = isNaN(_Fees) ? 0 : _Fees;

            //Get Row Field Data
            var PreviousDueFees = parseFloat($tblrow.find("[id=PreviousDueFees]").val());
            var PaidFees = parseFloat($tblrow.find("[id=PaidFees]").val());
            var Concession = parseFloat($tblrow.find("[id=Concession]").val());

            //IsNan Check
            PreviousDueFees = isNaN(PreviousDueFees) ? 0 : PreviousDueFees;
            PaidFees = isNaN(PaidFees) ? 0 : PaidFees;
            Concession = isNaN(PreviousDueFees) ? 0 : Concession;

            //Calculation
            var afterKeypress_PaidFees = (PaidFees * 10) + _Fees;
            var totalpaid_concession = afterKeypress_PaidFees + Concession;


            if (totalpaid_concession > PreviousDueFees) {
                event.preventDefault();//stop character from entireng input
                event.stopPropagation();
                return false;
            }

        });

        //PaidFees KeyPress Event
        $tblrow.find('.Concession').on('keypress', function (event) {

            //KeyCode to  ChareCode COnvert
            var _keypressval = String.fromCharCode(event.which);

            //Keypress Val
            var _Fees = parseFloat(_keypressval);
            _Fees = isNaN(_Fees) ? 0 : _Fees;

            //Get Row Field Data
            var PreviousDueFees = parseFloat($tblrow.find("[id=PreviousDueFees]").val());
            var PaidFees = parseFloat($tblrow.find("[id=PaidFees]").val());
            var Concession = parseFloat($tblrow.find("[id=Concession]").val());

            //IsNan Check
            PreviousDueFees = isNaN(PreviousDueFees) ? 0 : PreviousDueFees;
            PaidFees = isNaN(PaidFees) ? 0 : PaidFees;
            Concession = isNaN(PreviousDueFees) ? 0 : Concession;

            //Calculation
            var afterKeypress_Concession = (Concession * 10) + _Fees;
            var totalpaid_concession = PaidFees+ afterKeypress_Concession ;

            //check
            if (totalpaid_concession > PreviousDueFees) {
                event.preventDefault();//stop character from entireng input
                event.stopPropagation();
                return false;
            }

        });

    });

    //Method
    function CalculateFees ($tblrow) {
        //Get Input Field & Parse TO Float
        var PreviousDueFees = parseFloat($tblrow.find("[id=PreviousDueFees]").val());
        var PaidFees = parseFloat($tblrow.find("[id=PaidFees]").val());
        var Concession = parseFloat($tblrow.find("[id=Concession]").val());

        //IsNan Check
        PreviousDueFees = isNaN(PreviousDueFees) ? 0 : PreviousDueFees;
        PaidFees = isNaN(PaidFees) ? 0 : PaidFees;
        Concession = isNaN(PreviousDueFees) ? 0 : Concession;

        //Calculate
        var subTotalDueFees = PreviousDueFees - (PaidFees + Concession);

        //Due Fees Fill
        if (!isNaN(subTotalDueFees)) {

            $tblrow.find('.DueFees').val(subTotalDueFees.toFixed(2));//toFixed :for after point value

            //==Grand Total calculate==
            var grdtotDueFees = 0;
            var grdtotPaidFees = 0;
            var grdtotConcession = 0;


            //DueFees:loop wise get
            $(".DueFees").each(function () {
                var stval = parseFloat($(this).val());
                grdtotDueFees += isNaN(stval) ? 0 : stval;
            });

            //PaidFees:loop wise get
            $(".PaidFees").each(function () {
                var stval = parseFloat($(this).val());
                grdtotPaidFees += isNaN(stval) ? 0 : stval;
            });

            //Concession:loop wise get
            $(".Concession").each(function () {
                var stval = parseFloat($(this).val());
                grdtotConcession += isNaN(stval) ? 0 : stval;
            });

            //Grand Total Fill
            $('.grdtotDueFees').val(grdtotDueFees.toFixed(2));
            $('.grdtotPaidFees').val(grdtotPaidFees.toFixed(2));
            $('.grdtotConcession').val(grdtotConcession.toFixed(2));
        }
    }
});
//END


/*
 * ----------------------------------
 * TRANSECTION MODE CASH BANK Modal
 * ----------------------------------
 */
$(document).ready(function () {
    $("#Status").change(function () {
        var result = $("#Status option:selected").text();
        if (result.toUpperCase() === "BANK") {
            $('#dlgTransDetails').modal('show');

            //Required field create

        }
    });
});
//END


/*
 * ---------------------
 * Months Check/Uncheck
 * ---------------------
 */
$(function () {
    ////All Month Get
    var $monthcheckboxes = $("input.clsMonth[type='checkbox']");

    $('.clsMonth').change(function () {
        var currentCheckIndex = $monthcheckboxes.index(this);

        if (this.checked) {
            $monthcheckboxes.each(function () {
                var loopCheckBOxIndex = $monthcheckboxes.index(this);
                if (currentCheckIndex >= loopCheckBOxIndex) {
                    this.checked = true;
                }
                else {
                    this.checked = false;
                }
            });
        }
        else {
            $monthcheckboxes.each(function () {
                var loopCheckBOxIndex = $monthcheckboxes.index(this);
                if (currentCheckIndex <= loopCheckBOxIndex) {
                    this.checked = false;
                }
                else {
                    this.checked = true;
                }
            });
        }

    });
});
//END