﻿//use : ImagePreviewFrominputFile(this,'image_Controll_Name')
function ImagePreviewFrominputFile(input, imageName) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#' + imageName).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

